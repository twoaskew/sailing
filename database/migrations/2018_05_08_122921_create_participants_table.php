<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('locale');
            $table->text('description');
            $table->string('facebook_link')->nullable();
            $table->integer('regatta_id')->unsigned();
            $table->integer('yacht_id')->unsigned()->nullable();
            $table->unique(['email', 'regatta_id']);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('yacht_id')
                    ->references('id')
                    ->on('yachts')
                    ->onDelete('set null');
            $table->foreign('regatta_id')
                ->references('id')
                ->on('regattas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('participants', function(Blueprint $table)
        {
            $table->dropUnique(['email', 'regatta_id']);
            $table->dropForeign(['yacht_id']);
            $table->dropForeign(['regatta_id']);            
        });
        Schema::dropIfExists('participants');
    }
}
