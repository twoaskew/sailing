<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('path');
            $table->string('extension');
            $table->integer('regatta_id')->unsigned();
            $table->integer('order');
            $table->timestamps();

            $table->foreign('regatta_id')
                    ->references('id')
                    ->on('regattas')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function(Blueprint $table) {
            $table->dropForeign(['regatta_id']);
        });

        Schema::dropIfExists('documents');
    }
}
