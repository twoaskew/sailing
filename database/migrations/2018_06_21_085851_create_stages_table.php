<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regatta_id')->unsigned();
            $table->string('name');
            $table->date('date');
            $table->time('starts_at')->nullable();
            $table->integer('distance')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('regatta_id')
                    ->references('id')
                    ->on('regattas')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stages');
    }
}
