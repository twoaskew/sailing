<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_logins', function (Blueprint $table) {
            $table->string('token')->index();
            $table->string('loginable_type')->index();
            $table->integer('loginable_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_logins', function(Blueprint $table) {
            $table->dropIndex(['token']);
            $table->dropIndex(['loginable_type']);
            $table->dropIndex(['loginable_id']);
        });
        Schema::dropIfExists('email_logins');
    }
}
