<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regatta_id')->unsigned();
            $table->string('author')->nullable();
            $table->text('body');
            $table->timestamps();

            $table->foreign('regatta_id')
                    ->references('id')
                    ->on('regattas')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotes', function(Blueprint $table) {
            $table->dropForeign(['regatta_id']);
        });
        Schema::dropIfExists('quotes');
    }
}
