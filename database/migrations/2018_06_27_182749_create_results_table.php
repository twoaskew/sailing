<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('race_id')->unsigned();
            $table->integer('yacht_id')->unsigned();
            $table->timestamp('finished_at')->nullable();
            $table->string('comment')->nullable();

            $table->foreign('race_id')
                    ->references('id')
                    ->on('races')
                    ->onDelete('cascade');

            $table->foreign('yacht_id')
                    ->references('id')
                    ->on('yachts')
                    ->onDelete('cascade');        

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function(Blueprint $table) {
            $table->dropForeign(['race_id']);
            $table->dropForeign(['yacht_id']);
        });
        Schema::dropIfExists('results');
    }
}
