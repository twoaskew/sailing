<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkippersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skippers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regatta_id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('locale');
            $table->rememberToken();
            $table->timestamps();

            $table->unique(['email', 'regatta_id']);
            $table->foreign('regatta_id')
                    ->references('id')
                    ->on('regattas')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('skippers', function(Blueprint $table)
        {
            $table->dropUnique(['email', 'regatta_id']);
            $table->dropForeign(['regatta_id']);
        });
        Schema::dropIfExists('skippers');
    }
}
