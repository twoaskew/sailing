<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('races', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stage_id')->unsigned();
            $table->integer('fleet_id')->unsigned();
            $table->timestamp('starts_at')->nullable();
            $table->integer('distance')->unsigned()->nullable();
            $table->string('type');

            $table->foreign('fleet_id')
                    ->references('id')
                    ->on('fleets')
                    ->onDelete('cascade');

            $table->foreign('stage_id')
                    ->references('id')
                    ->on('stages')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('races', function(Blueprint $table) {
            $table->dropForeign(['stage_id']);
        });
        Schema::table('races', function(Blueprint $table) {
            $table->dropForeign(['fleet_id']);
        });
        Schema::dropIfExists('races');
    }
}
