<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYachtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yachts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('skipper_id')->unsigned();
            $table->string('name');
            $table->string('make_model')->nullable();
            $table->smallInteger('year')->unsigned()->nullable();
            $table->string('sail_number');
            $table->string('yacht_club')->nullable();
            $table->string('group');
            $table->string('loa');
            $table->tinyInteger('team_size')->unsigned()->nullable();
            $table->tinyInteger('can_accept_people')->unsigned()->nullable();

            $table->timestamps();
            
            $table->foreign('skipper_id')
                    ->references('id')
                    ->on('skippers')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yachts', function(Blueprint $table) {
            $table->dropForeign(['skipper_id']);
        });
        Schema::dropIfExists('yachts');
    }
}
