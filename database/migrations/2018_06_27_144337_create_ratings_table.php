<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->decimal('rating', 8, 4)->nullable();
            $table->integer('yacht_id')->unsigned();

            $table->foreign('yacht_id')
                    ->references('id')
                    ->on('yachts')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function(Blueprint $table) {
            $table->dropForeign(['yacht_id']);
        });
        Schema::dropIfExists('ratings');
    }
}
