<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Regatta::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\Skipper::class, function (Faker $faker) {
	return [
		'name'	=> 	$faker->name,
		'email' => $faker->unique()->safeEmail,
		'phone' => $faker->e164PhoneNumber,
		'regatta_id' => function() {
			return factory(App\Regatta::class)->create()->id;
		},
		'locale' => $faker->randomElement(['en','lv']),
	];
});

$factory->define(App\Yacht::class, function (Faker $faker) {
	return [
		'skipper_id'	=> 	function() {
			return factory(App\Skipper::class)->create()->id;
		},
		'name' => $faker->name,
		'make_model' => $faker->company,
        'year' => $faker->year,
        'sail_number' => $faker->countryCode . $faker->numberBetween(100, 999),
        'yacht_club' => $faker->name,
        'group' => $faker->randomElement(['lys', 'orc', 'texel']),
        'loa' => $faker->randomFloat($nbMaxDecimals = 1, $min = 6, $max = 40),
        'team_size' => $faker->numberBetween(3, 12),
        'can_accept_people' => $faker->boolean,
	];
});

$factory->define(App\Participant::class, function (Faker $faker) {
	$skipper = factory(App\Skipper::class)->create();
	return [
		'name'	=> 	$faker->name,
		'email' => $faker->unique()->safeEmail,
		'phone' => $faker->e164PhoneNumber,
		'description' => $faker->paragraph(),
		'regatta_id' => function() use ($skipper) {
			return $skipper->regatta_id;
		},
		'locale' => $faker->randomElement(['en','lv']),
		'yacht_id' => function() use ($skipper) {
			return factory(App\Yacht::class)->create([
				'skipper_id' => $skipper->id,
			])->id;
		},
	];
});

$factory->define(App\Stage::class, function (Faker $faker) {
	return [
		'name'	=> 	$faker->name,
		'date' => $faker->date(),
		'starts_at' => $faker->time(),
		'distance' => $faker->numberBetween(10, 99),
		'regatta_id' => function() {
			return factory(App\Regatta::class)->create()->id;
		},
	];
});




