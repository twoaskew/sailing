<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => ['localize', 'localeSessionRedirect', 'localizationRedirect',],
], function()
	{
	Route::get('/', 'LandingPageController@index')->name('home');
	Route::get(LaravelLocalization::transRoute('routes.info'), 'InfoController@index')->name('info');
	Route::get(LaravelLocalization::transRoute('routes.contacts'), 'ContactsController@index')->name('contacts');
	Route::get(LaravelLocalization::transRoute('routes.news'), 'PostsController@index')->name('news.index');
	Route::get(LaravelLocalization::transRoute('routes.news_show'), 'PostsController@show')->name('news.show');
	Route::get(LaravelLocalization::transRoute('routes.notices'), 'NoticesController@index')->name('notices.index');
	Route::get(LaravelLocalization::transRoute('routes.notices_show'), 'NoticesController@show')->name('notices.show');
	// Route::get('news/{id}', 'PostsController@show')->name('news.show');
	Route::get(LaravelLocalization::transRoute('routes.skippers'), 'SkippersController@index')->name('skippers.index');
	Route::get(LaravelLocalization::transRoute('routes.archive'), 'ArchiveController@index')->name('archive.index');
	// Route::get('/archive/{year}/galleries/{gallery}', 'ArchivedGalleriesController@show')->name('archive.gallery');
	Route::get(LaravelLocalization::transRoute('routes.archivedGalleryShow'), 'ArchivedGalleriesController@show')->name('archive.gallery');
	// Route::get(LaravelLocalization::transRoute('routes.archive') . "/{year}/" . LaravelLocalization::transRoute('routes.galleryShow'), 'ArchivedGalleriesController@show')->name('archive.gallery');
	Route::get(LaravelLocalization::transRoute('routes.gallery'), 'GalleriesController@index')->name('galleries.index');
	Route::get(LaravelLocalization::transRoute('routes.galleryShow'), 'GalleriesController@show')->name('galleries.show');
	// Route::get('/gallery-single-day', 'GallerySingleDayController@index');
	# old registration route backwards compatibility
	
	# new registration route
	Route::get(LaravelLocalization::transRoute('routes.registration'), 'SkipperRegistrationController@index')->name('skippers.registration');
	Route::get(LaravelLocalization::transRoute('routes.registration-participants'), 'ParticipantRegistrationController@index')->name('participants.registration');

	Route::get(LaravelLocalization::transRoute('routes.results'), 'ResultsController@index')->name('results');



	Route::get(LaravelLocalization::transRoute('routes.login'), 'Auth\LoginController@showLoginForm')->name('login');

	Route::prefix('administracija')->middleware(['auth'])->namespace('Admin')->group(function() {
		Route::get('dalibnieki', 'ParticipantsController@index');
		Route::get('kapteini', 'SkippersController@index');
	});

	Route::get(LaravelLocalization::transRoute('routes.users.participant') . '/' . LaravelLocalization::transRoute('routes.users.home'), 'Participants\HomeController@index')->name('participants.dashboard');

	Route::get(LaravelLocalization::transRoute('routes.skipper.dashboard'), 'Skippers\HomeController@index')->name('skippers.dashboard');
	Route::get(LaravelLocalization::transRoute('routes.users.skipper') . '/' . LaravelLocalization::transRoute('routes.skipper.participants'), 'Skippers\ParticipantsController@index')->name('participants.index');
});

Route::group([
		'prefix' => 'admin',
		'middleware' => 'auth:admin',
], function() {
	Route::get('/', function() {
		return view('admin.index');
	});
	Route::get('galleries', 'Admin\GalleriesController@index')->name('admin.galleries.index');
	Route::get('galleries/new', 'Admin\GalleriesController@create')->name('galleries.new');
	Route::post('galleries', 'Admin\GalleriesController@store')->name('admin.galleries.store');
	Route::get('galleries/{gallery}', 'Admin\GalleriesController@edit')->name('admin.galleries.edit');
	Route::post('galleries/{gallery}/images', 'Admin\GalleryImagesController@store')->name('admin.=gallery.images.store');

	Route::delete('images/{media}', 'Admin\ImagesController@destroy')->name('admin.images.destroy');
});

// Auth::routes();
Route::get('auth/email-authenticate/{token}', 'Auth\LoginController@authenticateEmail')->name('auth.email-authenticate');
Route::get('/regatta/{regatta}/documents/{document}', 'RegattaDocumentsController@show')->name('document');
// Route::get('/regatta/{regatta}/documents/nor', 'RegattaNORController@index')->name('nor');
// Route::get('/regatta/{regatta}/documents/entry-form', 'RegattaEntryFormController@index')->name('entry-form');
// Route::get('/regatta/{regatta}/documents/inspection-list', 'RegattaInspectionListController@index')->name('inspection-list');
// Route::get('/regatta/{regatta}/documents/orc-sail-list', 'RegattaORCSailListController@index')->name('orc-sail-list');
// Route::get('/regatta/{regatta}/documents/lys-sail-list', 'RegattaLYSSailListController@index')->name('lys-sail-list');
Route::post('/participants', 'ParticipantsController@store')->name('participants.store');
Route::patch('/participants/{paticipant}', 'ParticipantsController@update')->name('participants.update')->middleware(['auth:participant']);
Route::delete('/participants/{participant}', 'ParticipantsController@destroy')->name('participants.destroy')->middleware(['auth:participant']);
Route::post('/participants/{participant}/invite', 'ParticipantInvitationsController@store')->name('participant.invitations')->middleware(['auth:skipper']);
Route::delete('/invitations/{invitation}', 'ParticipantInvitationsController@destroy')->name('invitations.destroy')->middleware(['auth:skipper']);
Route::post('/invitations/{invitation}/accept', 'Participant\InvitationAcceptController@store')->name('invitations.accept')->middleware(['auth']);
Route::post('/invitations/{invitation}/decline', 'Participant\InvitationDeclineController@store')->name('invitations.decline')->middleware(['auth']);
Route::post('/skippers', 'SkippersController@store')->name('skippers.store');
Route::patch('/skippers/{skipper}', 'SkippersController@update')->name('skippers.update')->middleware(['auth:skipper']);
Route::post('/login', 'Auth\LoginController@login')->name('login.store');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/movie', function() {
	return redirect('https://forms.gle/TysmsV7Rn2nv2M1B9');
});

// Route::get('/test-thanks', function() {
// 	return view('test-thanks');
// });