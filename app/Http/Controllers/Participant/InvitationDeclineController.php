<?php

namespace App\Http\Controllers\Participant;

use App\Invitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InvitationDeclineController extends Controller
{
    public function store(Request $request, Invitation $invitation)
    {
    	if ($invitation->participant_id === Auth::user()->id) {
    		$invitation->decline();
    	}
    }
}
