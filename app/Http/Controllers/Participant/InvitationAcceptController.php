<?php

namespace App\Http\Controllers\Participant;

use App\Invitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class InvitationAcceptController extends Controller
{
    public function store(Request $request, Invitation $invitation)
    {

   		if ($invitation->participant_id === Auth::user()->id) {
    		$invitation->accept();

    		$request->session()->flash('message', __('crew.accept_success'));

	        return redirect(route('participants.dashboard'));
    	}
    }
}
