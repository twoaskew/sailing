<?php

namespace App\Http\Controllers\Admin;

use App\Skipper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SkippersController extends Controller
{
    public function index(Request $request)
    {
    	return view('admin.skippers.index',[
    		'skippers' => Skipper::all(),
    	]);
    }
}
