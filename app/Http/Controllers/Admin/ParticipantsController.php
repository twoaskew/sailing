<?php

namespace App\Http\Controllers\Admin;

use App\Participant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParticipantsController extends Controller
{
    public function index(Request $request)
    {
    	return view('admin.participants.index', [
    		'participants' => Participant::all(),
    	]);	
    }
}
