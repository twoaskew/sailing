<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryImagesController extends Controller
{
    public function store(Request $request, Gallery $gallery)
    {
    	$request->validate([
    		'file' => 'required',
    	]);

    	if ($request->hasFile('file')) {
    		$image = $gallery->addImage($request->file);
    		return [
    			'id' => $image->id,
    			'thumb' => $image->getUrl(),
    		];
    	}
    }
}
