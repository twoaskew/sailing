<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\MediaLibrary\Models\Media;

class ImagesController extends Controller
{
    public function destroy(Media $media)
    {
    	$media->delete();
    }
}
