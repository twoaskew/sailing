<?php

namespace App\Http\Controllers\Skippers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:skipper');
    }

    /**
     * Show the application dashboard for participants.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skipper = Auth::user();

        LaravelLocalization::setLocale($skipper->locale);

        return view('skippers.dashboard', [
            'skipper' => $skipper->load('yacht'),
        ]);
    }
}
