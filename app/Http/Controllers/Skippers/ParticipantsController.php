<?php

namespace App\Http\Controllers\Skippers;

use App\Regatta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ParticipantsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:skipper');
	}

    public function index(Request $request)
    {
        $skipper = Auth::user();

        LaravelLocalization::setLocale($skipper->locale);

    	return view('skippers.participants.index', [
    		'participants' => Regatta::current()->participants()->free()->with(['invitations' => function($query) use ($skipper) {
                $query->where('skipper_id', $skipper->id);
            }])->get(),
    	]);
    }

    
}
