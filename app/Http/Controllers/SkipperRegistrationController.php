<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SkipperRegistrationController extends Controller
{
    public function index()
    {
    	// return view('registration.skipper');
    	// return redirect('https://www.manage2sail.com/en-US/event/GORR2021?#!/');
    	// return redirect('https://www.manage2sail.com/et-EE/event/f5adf233-e249-4c70-88a1-63f650f16576#!/');
    	return redirect('https://manage2sail.com/lv-LV/event/GoRR2023#!/');
    }
}
