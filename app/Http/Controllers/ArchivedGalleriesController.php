<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;

class ArchivedGalleriesController extends Controller
{
    public function show($year, Gallery $gallery) {
    	return view('archivedGallerySingleDay', [
    		'year' => $year,
    		'gallery' => $gallery->load('media'),
    		'images' => array_chunk($gallery->images()
                ->map(function($image, $index) {
                    return [
                        'thumb' => $image->getUrl('thumb'),
                        'preview' => $image->getUrl('thumb'),
                        'full' => $image->getUrl('large'),
                        'index' => $index,
                    ];
                })
                ->all(), 3),
    	]);
    }
}
