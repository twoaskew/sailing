<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Skipper;
use App\EmailLogin;
use App\Mail\Login;
use App\Participant;
use App\Rules\EmailExists;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo()
    {
        return auth()->user()->getRedirect();  
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => ['required', 'email', new EmailExists],
        ]);
    }

    public function authenticateEmail($token)
    {
        try {
            $emailLogin = EmailLogin::validFromToken($token, $this);
        } catch (ModelNotFoundException $e) {
            return $this->invalidTokenResponse();
        }

        if ($emailLogin) {
            $user = $emailLogin->loginable;
            Auth::guard($user->getGuard())->login($user);
            $emailLogin->delete();
            LaravelLocalization::setLocale($user->locale);
            return redirect($user->getRedirect());
        }

        return $this->invalidTokenResponse();
    }

    public function invalidTokenResponse()
    {
        return view('auth.invalidToken');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        
        $user = $this->findUserByEmail($request->email);
        
        $emailLogin = $user->createEmailLogin();

        LaravelLocalization::setLocale($user->locale);

        $url = route('auth.email-authenticate', [
            'token' => $emailLogin->token
        ]);
        
        Mail::to($emailLogin->loginable)->send(new Login($url, $user->locale));

        return view('auth.checkEmail');
    }

    protected function findUserByEmail($email)
    {
        if ($participant = Participant::where('email', $email)->first()) {
            return $participant;
        }
        if ($skipper = Skipper::where('email', $email)->first()) {
            return $skipper;
        }
        if ($admin = User::where('email', $email)->first()) {
            return $admin;
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $locale = $request->session()->get('locale', 'lv');
        
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect("/" . $locale);
    }

}
