<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;

class RegattaDocumentsController extends Controller
{
    public function show($regatta, Document $document)
    {
    	return response()->file(storage_path("app/documents/{$regatta}/{$document->path}"));
    }
}
