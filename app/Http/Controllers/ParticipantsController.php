<?php

namespace App\Http\Controllers;

use App\Regatta;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class ParticipantsController extends Controller
{
    public function store(Request $request)
    {
    	$fields = $request->validate([
    		'name' => 'required',
    		'email' => 'email|required',
    		'phone' => 'required',
            'locale' => [
                'required',
                Rule::in(['lv', 'en'])
            ],
    		'description' => 'required',
    		'facebook_link' => 'url|nullable',
            'regatta_id' => 'nullable|exists:regattas,id',
            'image' => 'nullable|image',
    	]);
        $regatta = Regatta::find($request->regatta_id ?? 1);

        $participant = $regatta->addParticipant(
            $request->only(['name', 'email', 'phone', 'description', 'facebook_link', 'locale'])
        );

        if ($request->hasFile('image')) {
            $participant->addImage($request->image);
        }

        return view('registration.participant-thanks');
    }

    public function update(Request $request, Participant $participant)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'description' => 'required',
            'image' => 'nullable|image',
        ]);

        $participant->update(
            $request->only(['name', 'phone', 'description'])
        );

        if ($request->hasFile('image')) {
            $participant->updateImage($request->image);
        }

        $request->session()->flash('message', 'Esam saglabājuši izmaiņas pieteikuma formā.');

        return redirect(route('participants.dashboard'));
    }

    public function destroy(Request $request, Participant $participant)
    {
        if ($participant->id === Auth::user()->id) {
            $participant->delete();
            Auth::logout();
            return redirect(route('home'));
        }   

        $request->session()->flash('error', 'Nav iespējams dzēst pieteikumu.');

        return redirect(route('participants.dashboard'));
    }
}
