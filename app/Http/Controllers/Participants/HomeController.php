<?php

namespace App\Http\Controllers\Participants;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:participant');
    }

    /**
     * Show the application dashboard for participants.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participant = Auth::user();

        LaravelLocalization::setLocale($participant->locale);
        
        return view('participants.dashboard', [
            'participant' => $participant,
        ]);
    }
}
