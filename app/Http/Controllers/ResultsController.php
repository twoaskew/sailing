<?php

namespace App\Http\Controllers;

use App\Race;
use App\Result;
use App\Regatta;
use App\Results;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    public function index()
    {
    	return redirect('https://www.manage2sail.com/et-EE/event/f5adf233-e249-4c70-88a1-63f650f16576#!/results?classId=76dc8b70-655c-419b-bea8-39f9018babc3');

    	// return redirect('https://lv.regattas.eu/gorr');
    	// $stages = Regatta::find(1)->stages()->with('races')->get();
    	// $stages = Regatta::current()->stages()->with('races')->get();
    	// $races = Race::whereIn('stage_id', $stages->pluck('id')->all())->with('results.yacht', 'fleet')->get();
    // 	$fleets = Regatta::current()->fleets()
		  //   	->with(['yachts', 'yachts.skipper', 'yachts.results' => function($query) use ($races) {
	   //  			$query->whereIn('results.race_id', $races->pluck('id')->all());
	   //  		}])
	   //  		->get()
	   //  		->map(function($fleet) use ($races, $stages) {
	   //  			$yachts = $fleet->yachts->map(function($yacht) use ($races, $stages) {
		  //   			$results = $races->filter(function($race) use ($yacht) {
		  //   						return $race->fleet_id === $yacht->fleet_id;
		  //   					})->map(function($race) use ($yacht) {
		  //   					$resultsLocal = $this->sortResults($race->results);
		  //   					$result = $resultsLocal->firstWhere('yacht_id', $yacht->id);
		  //   					return is_null($result) ? '-' : [
		  //   						'time' => $result->getPresetableTime(),
		  //   						'et' => $result->et,
		  //   						'ct' => $result->ct,
		  //   						'points' => $result->position,
		  //   					]; 
		  //   				});
		  //   			$missingRaces = $stages->count() - $results->count();
		  //   			while ($stages->count() - $results->count() > 0) {
		  //   				$results->push('-');
		  //   			}
		  //   			$total = $results->sum(function($result) {
		  //   					if (is_string($result) ) {
		  //   						return 0;
		  //   					}
		  //   					return array_key_exists('points', $result) ? $result['points'] : 0;
		  //   				});
		  //   			return [
		  //   				'name' => $yacht->name,
		  //   				'sail_number' => $yacht->sail_number,
		  //   				'rating' => $yacht->rating ?? '-',
		  //   				'skipper' => $yacht->skipper->name,
		  //   				'results' => $results->values()->all(),
		  //   				'total' => $total > 0 ? $total : '-',
		  //   			];
	   //  			});
	   //  			return [
	   //  				'id' => $fleet->id,
	   //  				'name' => $fleet->name,
	   //  				'type' => $fleet->type,
	   //  				'yachts' => $yachts,
	   //  			];
	   //  		});

	   //  $updated_at = Result::latest('updated_at')->first()->updated_at;
	   //  $updated_at->timezone = 'Europe/Riga';
    // 	return view('results',[
    // 		'stages' => $stages,
    // 		'fleets' => $fleets,
    // 		'updated_at' => $updated_at->toDateTimeString(),
    // 	]);
    // }

 //    protected static function sortResults($results)
	// {
	// 	$count = $results->count();
	// 	return $results->sort(function($a, $b) {
	// 				if (!is_null($b->finished_at) && !is_null($a->comment) ) {
	// 					return 1;
	// 				}
	// 				if (!is_null($a->finished_at) && !is_null($b->comment)) {
	// 					return -1;
	// 				}
	// 				if (is_null($a->finished_at) && is_null($b->finished_at)) {
	// 					return $a->yacht->name < $b->yacht->name ? -1 : 1;
	// 				}
	// 				return $a->ct - $b->ct;
	// 			})
	// 	       ->values()
	// 		   ->map(function($result, $key) use ($count) {
	// 		   	   $points = is_null($result->comment) ? $key + 1 : $count + 1;
	// 		       $result->position = $points;
	// 		       return $result;
	// 		   });
	}
}
