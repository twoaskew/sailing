<?php

namespace App\Http\Controllers;

use App\Quote;
use Illuminate\Http\Request;

class ParticipantRegistrationController extends Controller
{
    public function index()
    {
    	return view('registration.participant', [
    		'quotes' => Quote::where('regatta_id', 1)->get()->random(3),
    	]);
    }
}
