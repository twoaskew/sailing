<?php

namespace App\Http\Controllers;

use App\Invitation;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ParticipantInvitationsController extends Controller
{
    public function store(Request $request, Participant $participant)
    {
        $user = Auth::user();

        LaravelLocalization::setLocale($user->locale);

    	// if participant is not taken
    	if (is_null($participant->yacht_id)) {
    		$participant->invitations()->create([
    			'skipper_id' => Auth::user()->id,
    		]);

    		$request->session()->flash('message', __('skipper.invite_success'));

	        return redirect(route('participants.index'));
    	}

    	$request->session()->flash('error', 'Dalībnieks ir jau pieteikts citā komandā.');

    	return redirect(route('participants.index'));
    }

    public function destroy(Request $request, Invitation $invitation)
    {
        $user = Auth::user();

        LaravelLocalization::setLocale($user->locale);

        if ($invitation->skipper_id === Auth::user()->id) {
            $invitation->delete();
            
            $request->session()->flash('message', __('skipper.invite_revoked') );

            return redirect(route('participants.index'));
        }

        $request->session()->flash('error', 'Šo uzaicinājumu Tu nevari atsaukt.');

        return redirect(route('participants.index'));
    }
}
