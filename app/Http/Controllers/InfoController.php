<?php

namespace App\Http\Controllers;

use App\Regatta;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    public function index()
    {
    	return view('info', [
    		'documents' => Regatta::current()->documents()->ordered()->get(),
    	]);
    }
}
