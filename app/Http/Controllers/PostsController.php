<?php

namespace App\Http\Controllers;

use App\Post;
use App\Regatta;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class PostsController extends Controller
{
    public function index()
    {
    	$regatta = Regatta::current();
    	return view('posts.index', [
            'posts' => Post::where('regatta_id', $regatta->id)->latest()->get(),
        ]);
    }

    public function show($slug)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $post = Post::where("slug->{$locale}", $slug)->first();
    	return view('posts.show', [
    		'post' => $post
    	]);
    }
}
