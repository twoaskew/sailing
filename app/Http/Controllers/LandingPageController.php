<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function index()
    {
    	return view('index', [
    		'posts' => Post::where('regatta_id', 1)->latest()->limit(3)->get(),
    	]);
    }
}
