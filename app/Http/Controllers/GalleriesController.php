<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Regatta;
use Illuminate\Http\Request;

class GalleriesController extends Controller
{
    public function index()
    {
        return view('galleries.index', [
            // 'galleries' => Regatta::current()->galleries()->with('media')->get(),
            'galleries' => Regatta::current()->galleries()->with('media')->orderBy('created_at', 'DESC')->get(),
            // 'galleries' => Regatta::find(1)->galleries()->with('media')->get(),
        ]);
    }

    public function show(Gallery $gallery)
    {
        $gallery->load('media');
        return view('galleries.show', [
            'gallery' => $gallery,
            'images' => array_chunk($gallery->images()
                ->map(function($image, $index) {
                    return [
                        'thumb' => $image->getUrl('thumb'),
                        'preview' => $image->getUrl('thumb'),
                        'full' => $image->getUrl('large'),
                        'index' => $index,
                    ];
                })
                ->all(), 3),
        ]);
    }

}
