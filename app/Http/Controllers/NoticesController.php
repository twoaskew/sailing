<?php

namespace App\Http\Controllers;

use App\Notice;
use App\Regatta;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class NoticesController extends Controller
{
    public function index() {
        return redirect('https://www.manage2sail.com/et-EE/event/f5adf233-e249-4c70-88a1-63f650f16576#!/onb?tab=documents&classId=76dc8b70-655c-419b-bea8-39f9018babc3');
    }
    // public function index() {
    // 	$regatta = Regatta::current();
    // 	return view('notices.index', [
    // 		'notices' => Notice::where('regatta_id', $regatta->id)->latest()->get(),
    // 	]);
    // }

    // public function show($slug)
    // {
    // 	$locale = LaravelLocalization::getCurrentLocale();
    // 	$notice = Notice::where("slug->{$locale}", $slug)->first();
    // 	return view('notices.show', [
    // 		'notice' => $notice
    // 	]);
    // }
}
