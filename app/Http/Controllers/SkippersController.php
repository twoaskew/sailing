<?php

namespace App\Http\Controllers;

use App\User;
use App\Regatta;
use App\Skipper;
use App\Rules\EmailUnique;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationConfirmation;
use Illuminate\Database\QueryException;
use App\Notifications\JaunaRegistracija;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SkippersController extends Controller
{
    public function index()
    {
    // 		$regatta = Regatta::current();
    //     return view('skippers', [
    //     	'skippers' => Skipper::where('regatta_id', $regatta->id)
    // //     	->whereHas('yacht', function ($query)
    // //     	{
			 // //    $query->registered();
				// // })
    //     	->with('yacht')
    //     	->get(),
    //     ]);
    	return redirect('https://www.manage2sail.com/et-EE/event/f5adf233-e249-4c70-88a1-63f650f16576#!/entries?classId=76dc8b70-655c-419b-bea8-39f9018babc3');
    }

	public function store(Request $request)
	{
		$fields = $request->validate([
			'name' => 'required',
			'email' => ['required', 'email', 'max:255', new EmailUnique],
			'phone' => 'required',
            'locale' => [
                'required',
                Rule::in(['lv', 'en'])
            ],
			'yacht_name' => 'required',
			'sail_number' => 'required',
			'yacht_club' => 'nullable',
			'group' => [
				'required',
				Rule::in(['lys', 'orc', 'texel']),
			],
			'loa' => 'required',
			'make_model' => 'nullable',
			'year' => 'integer|nullable',
			'team_size' => 'required|numeric|max:12|min:1',
			'can_accept_people' => 'nullable|boolean',
			'nor_read' => 'required|accepted',
			'regatta_id' => 'nullable|exists:regattas,id',
			'image' => 'nullable|image|max:10000',
		]);
		LaravelLocalization::setLocale($request->locale);

		$regatta = Regatta::current();

		$skipper = $regatta->addSkipper($request->only(['name', 'email', 'phone', 'locale']));

		$yacht = $skipper->addYacht(
			$this->remapYachtKeys(
				$request->only(
					['yacht_name', 'sail_number', 'yacht_club', 'group', 'loa', 'make_model', 'year', 'team_size', 'can_accept_people']
				)
			)
		);

		if ($request->hasFile('image')) {
			$yacht->addImage($request->image);
		}

		// send an email
		// Mail::to($skipper->email)->send(new RegistrationConfirmation);
		// User::getMainAdmin()->notify(new JaunaRegistracija($skipper, $yacht));

		return view('registration.skipper-thanks');

	}

	public function update(Request $request, Skipper $skipper)
	{
		$request->validate([
			'name' => 'required',
			'phone' => 'required',
			'yacht_name' => 'required',
			'sail_number' => 'required',
			'yacht_club' => 'nullable',
			'group' => [
				'required',
				Rule::in(['lys', 'orc', 'texel']),
			],
			'loa' => 'required',
			'make_model' => 'nullable',
			'year' => 'integer|nullable',
			'team_size' => 'required|numeric|max:12|min:1',
			'can_accept_people' => 'nullable|boolean',
			'image' => 'nullable|image',
		]);
		LaravelLocalization::setLocale($skipper->locale);
		$skipper->update($request->only(['name', 'phone']));

		$skipper->yacht->update(
			$this->remapYachtKeys(
				$request->only(['yacht_name', 'sail_number', 'yacht_club', 'group', 'loa', 'make_model', 'year', 'team_size', 'can_accept_people'])
			)
		);

		if ($request->hasFile('image')) {
			$skipper->yacht->updateImage($request->image);
		}
		$request->session()->flash('message', trans('skipper.update_success'));

		return redirect(route('skippers.dashboard'));
	}

	protected function remapYachtKeys($fields)
	{
		return [
            'name' => $fields['yacht_name'],
            'make_model' => $fields['make_model'],
            'year' => $fields['year'],
            'sail_number' => $fields['sail_number'],
            'yacht_club' => $fields['yacht_club'],
            'group' => $fields['group'],
            'loa' => $fields['loa'],
            'team_size' => $fields['team_size'],
            'can_accept_people' => true,
		];
	}
}
