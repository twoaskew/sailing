<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regatta extends Model
{
    protected $fillable = ['name'];

    public static function current() {
      return self::orderBy('created_at', 'desc')->first();
    }

    public function stages()
    {
      return $this->hasMany(Stage::class);
    }

    public function fleets()
    {
      return $this->hasMany(Fleet::class);
    }

    public function skippers()
    {
    	return $this->hasMany(Skipper::class);
    }

    public function addSkipper($fields)
    {
      return $this->skippers()->create($fields);
    }

    public function yachts()
    {
      return $this->hasManyThrough(Yacht::class, Skipper::class);
    }

    public function yachtsInGroup($group)
    {
      return $this->yachts()->whereGroup($group)->get();
    }

    public function posts() {
      return $this->hasMany(Post::class);
    }

    public function quotes()
    {
      return $this->hasMany(Quote::class);
    }

   	public function participants()
   	{
   		return $this->hasMany(Participant::class);
   	}

   	public function addParticipant($fields)
   	{
   		return $this->participants()->create($fields);
   	}

    public function documents()
    {
      return $this->hasMany(Document::class);
    }

    public function galleries()
    {
      return $this->hasMany(Gallery::class);
    }
}
