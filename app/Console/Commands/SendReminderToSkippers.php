<?php

namespace App\Console\Commands;

use App\Regatta;
use Illuminate\Console\Command;
use App\Notifications\SkipperReminder;

class SendReminderToSkippers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'skippers:reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminders to skippers about crew without boats.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Regatta::current()
            ->skippers
            ->each
            ->notify(new SkipperReminder());
    }
}
