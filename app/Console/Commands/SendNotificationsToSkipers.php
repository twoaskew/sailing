<?php

namespace App\Console\Commands;

use App\Regatta;
use App\Participant;
use Illuminate\Console\Command;
use App\Notifications\NewParticipants;

class SendNotificationsToSkipers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'skippers:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify skippers about crew without boats.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $regatta = Regatta::current();
        if ($regatta->participants()->new()->count() > 0) {
            $skippers = $regatta->skippers;
            $skippers->each->notify(new NewParticipants());
            $regatta->participants()->where('notified', 0)->update(['notified' => 1]);
        }
    }
}
