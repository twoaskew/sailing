<?php

namespace App\Console\Commands;

use App\Post;
use App\Notice;
use Illuminate\Console\Command;

class AddEeTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:estonian';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Estonian translations to Posts and Notices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notices = Notice::all();
        $notices->each(function($notice) {
            $notice->setTranslation('slug', 'ee', $notice->getTranslation('slug', 'en'));
            $notice->setTranslation('title', 'ee', $notice->getTranslation('title', 'en'));
            $notice->setTranslation('body', 'ee', $notice->getTranslation('body', 'en'));
            $notice->save();
        });

        $posts = Post::all();
        $posts->each(function($post) {
            $post->setTranslation('slug', 'ee', $post->getTranslation('slug', 'en'));
            $post->setTranslation('title', 'ee', $post->getTranslation('title', 'en'));
            $post->setTranslation('body', 'ee', $post->getTranslation('body', 'en'));
            $post->save();
        });
    }
}
