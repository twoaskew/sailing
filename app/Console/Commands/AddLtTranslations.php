<?php

namespace App\Console\Commands;

use App\Post;
use App\Notice;
use Illuminate\Console\Command;

class AddLtTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:lithuanian';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Lithuanian translations to Posts and Notices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notices = Notice::all();
        $notices->each(function($notice) {
            $notice->setTranslation('slug', 'lt', $notice->getTranslation('slug', 'en'));
            $notice->setTranslation('title', 'lt', $notice->getTranslation('title', 'en'));
            $notice->setTranslation('body', 'lt', $notice->getTranslation('body', 'en'));
            $notice->save();
        });

        $posts = Post::all();
        $posts->each(function($post) {
            $post->setTranslation('slug', 'lt', $post->getTranslation('slug', 'en'));
            $post->setTranslation('title', 'lt', $post->getTranslation('title', 'en'));
            $post->setTranslation('body', 'lt', $post->getTranslation('body', 'en'));
            $post->save();
        });
    }
}
