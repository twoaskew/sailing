<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Post extends Model
{
	use Hastranslations;

  protected $guarded = [];

	public $translatable = ['title', 'body', 'slug'];

	public function excerpt()
	{
		if (strlen($this->body) > 300) {
			return substr($this->body, 0, strpos($this->body, ' ', 300)) . ' ...';
		} else {
			return $this->body;
		}
	}
}
