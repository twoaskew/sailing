<?php

namespace App;

use App\Calculators\LYS;
use App\Calculators\ORC;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	protected $with = ['race', 'yacht.ratings'];

	protected $guarded = [];

	protected $casts = [
		'finished_at' => 'timestamp',
	];

	public function race()
	{
		return $this->belongsTo(Race::class);
	}

	public function yacht()
	{
		return $this->belongsTo(Yacht::class);
	}

	public function getPresetableTime()
	{
		if ($this->hasException()) {
			return $this->comment;
		}
		return Carbon::createFromTimestamp($this->finished_at)->toTimeString();
	}

	public function getEtAttribute()
	{
		if ($this->hasException()) {
			return $this->comment;
		}
		$started_at = Carbon::createFromTimestamp($this->race->starts_at);
		return (int) Carbon::createFromTimestamp($this->finished_at)->diffInSeconds($started_at);
	}

	public function getCtAttribute()
	{
		if ($this->hasException()) {
			return $this->comment;
		}
		$calculator = $this->getCalculator($this->yacht->group);
		return $calculator->getCorrectedTime($this);
	}

	protected function getCalculator($type)
	{
		if ($type === 'orc') {
			return new ORC;
		}
		if ($type === 'lys') {
			return new LYS;
		}
	}

	protected function hasException()
	{
		return !is_null($this->comment);
	}

	protected function getLYSResult()
	{
		
	}
}
