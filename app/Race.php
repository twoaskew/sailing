<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    public $timestamps = false;

    public $appends = ['startTime'];

    public $casts = [
    	'starts_at' => 'timestamp',
    ];

    public function getStartTimeAttribute()
    {
        return Carbon::createFromTimestamp($this->starts_at)->toTimeString();
    }

    public function stage()
    {
    	return $this->belongsTo(Stage::class);
    }

    public function fleet()
    {
    	return $this->belongsTo(Fleet::class);
    }

    public function results()
    {
    	return $this->hasMany(Result::class);
    }
}
