<?php

namespace App;

use Spatie\Image\Manipulations;
use Spatie\EloquentSortable\Sortable;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Gallery extends Model implements HasMedia, Sortable
{
	use HasMediaTrait,
        SortableTrait,
        HasTranslations;

    protected $guarded = [];

    protected $translatable = ['title'];

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public function regatta()
    {
        return $this->belongsTo(Regatta::class);
    }

    public function addImage($image)
    {
    	return $this->addMedia($image)
    		->toMediaCollection('images');
    }

    public function images()
    {
    	return $this->getMedia('images');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 400, 400);

        $this->addMediaConversion('small')
        	->fit(Manipulations::FIT_CONTAIN, 200, 133);

        $this->addMediaConversion('large')
        	->fit(Manipulations::FIT_CONTAIN, 1200, 800);
    }
}
