<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Skipper extends Authenticatable implements AuthenticatableContract
{
    use Notifiable;
    
	protected $guarded = [];

     /**
     * Get all of the skipper's EmailLogins.
     */
    public function emailLogins()
    {
        return $this->morphMany(EmailLogin::class, 'loginable');
    }

    public function createEmailLogin()
    {
        return $this->emailLogins()->create([
            'token' => str_random(30),
        ]);
    }

    public function getGuard()
    {
        return 'skipper';
    }

    public function type()
    {
        return 'skipper';
    }

    public function getRedirect()
    {
        $skipper = __('routes.users.skipper');
        $dashboard = __('routes.users.home');
        return "/{$this->locale}/{$skipper}/{$dashboard}";
    }

    public function yacht()
    {
    	return $this->hasOne(Yacht::class);
    }

    public function addYacht($fields)
    {
    	return $this->yacht()->create($fields);
    }

    public function regatta()
    {
    	return $this->belongsTo(Regatta::model);
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }

}
