<?php

namespace App;

use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Participant extends Authenticatable implements AuthenticatableContract, HasMedia
{
    use HasMediaTrait;

	protected $guarded = [];

    protected $casts = [
        'notified' => 'boolean',
    ];

    public static function markNewAsNotified() {
        return self::where('notified', 0)->update(['notified' => 1]);
    }

    public function scopeFree($builder)
    {
        return $builder->whereNull('yacht_id');
    }

    public function scopeNew($builder)
    {
        return $builder->where('notified', 0);
    }

    public function type()
    {
        return 'participant';
    }

	 /**
	 * Get all of the participant's EmailLogins.
	 */
    public function emailLogins()
    {
        return $this->morphMany(EmailLogin::class, 'loginable');
    }

    public function createEmailLogin()
    {
        return $this->emailLogins()->create([
            'token' => str_random(30),
        ]);
    }

    public function getGuard()
    {
        return 'participant';
    }

    public function getRedirect()
    {
        $participant = __('routes.users.participant');
        $dashboard = __('routes.users.home');
        return "/{$this->locale}/{$participant}/{$dashboard}";
    }

    public function yacht()
    {
    	return $this->belongsTo(Yacht::class);
    }

    public function addImage($image)
    {
        $this->addMedia($image)
            ->toMediaCollection();
    }

    public function updateImage($image)
    {
        $this->getFirstMedia()->delete();
        $this->addImage($image);
    }

    public function thumbnail()
    {
        if (! is_null($this->getFirstMedia())) {
            return $this->getFirstMedia()->getUrl('thumb');            
        }
        return '/images/user-image-empty.png';
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
             ->fit(Manipulations::FIT_CROP, 200, 200);
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }

    public function openInvitations()
    {
        return $this->invitations()->open();
    }

    public function isOnBoard()
    {
        return !is_null($this->yacht_id);
    }
}
