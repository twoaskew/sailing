<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Login extends Mailable
{
    use Queueable, SerializesModels;

    public $locale;

    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($url, $locale)
    {
        $this->url = $url;
        $this->locale = $locale;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->getView(), ['url' => $this->url])
                    ->subject($this->getSubject());
    }

    protected function getView()
    {
        if ($this->locale == 'lv') {
            return 'auth.loginEmail_plain';
        }
        return 'auth.loginEmail_plain_en';
    }

    protected function getSubject()
    {
        if ($this->locale == 'lv') {
            return 'Ienākt Rīgas Līča Regates lapā';
        }
        return 'Sign in to Gulf of Riga Regatta';
    }
}
