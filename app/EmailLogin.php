<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EmailLogin extends Model
{
    public $incrementing = false;
    
    public $fillable = ['token'];

    protected $primaryKey = 'token';

    public function loginable()
    {
        return $this->morphTo();
    }

    public static function validFromToken($token)
    {
        return self::where('token', $token)
                ->where('created_at', '>', Carbon::parse('-15 minutes'))
                ->firstOrFail();
    }
}
