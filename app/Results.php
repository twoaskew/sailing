<?php

namespace App;

class Results
{
	public static function stages() {
		return Regatta::current()->stages;
	}

	public static function forGroup($group) {
		return Regatta::current()->yachtsInGroup($group);
	}

	public static function forTable()
	{
		$participants = Regatta::current()->yachts->load('results');
		return static::sortResults($participants);
	}

	protected static function sortResults($results)
	{
		return $results->sort(function($a, $b) {
			if (!is_null($b->time) && $a->dnf || $a->dns || $a->dsq ) {
				return 1;
			}
			if (!is_null($a->time) && $b->dnf || $b->dns || $b->dsq) {
				return -1;
			}
			if (is_null($a->time) && is_null($b->time)) {
				return $a->yacht->name < $b->yacht->name ? -1 : 1;
			}
			return $a->ct - $b->ct;
		})->values();
		// $count = $results->count();
		// return $results->sortBy('ct')
		// 			   ->values()
		// 			   ->map(function($result, $key) use ($count) {
		// 			   	   $points = $result->dnf || $result->dns ? $count + 1 : $key + 1;
		// 			       $result->position = $points;
		// 			       return $result;
		// 			   });
	}
}