<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
	protected $guarded = [];

    public function regatta()
    {
    	return $this->belongsTo(Regatta::class);
    }
}
