<?php

namespace App\Rules;

use App\User;
use App\Skipper;
use App\Regatta;
use App\Participant;
use Illuminate\Contracts\Validation\Rule;

class EmailExists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $regatta = Regatta::current();
        if (Skipper::where('regatta_id', $regatta->id)->where('email', $value)->count() > 0) {
            return true;
        }

        if (Participant::where('regatta_id', $regatta->id)->where('email', $value)->count() > 0) {
            return true;
        }

        if (User::where('email', $value)->count() > 0)
        {
            return true;
        }
        
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('errors.email_exists');
    }
}
