<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $guarded = [];

    public function scopeOpen($builder)
    {
        return $builder->whereNull('accepted_at')
                       ->whereNull('declined_at');
    }

    public function participant()
    {
    	return $this->belongsTo(Participant::class);
    }

    public function skipper()
    {
    	return $this->belongsTo(Skipper::class);
    }

    public function decline()
    {
    	return $this->update(['declined_at' => Carbon::now()]);
    }

    public function accept()
    {
        $this->participant->update(['yacht_id' => $this->skipper->yacht->id]);
        $this->update(['accepted_at' => Carbon::now()]);
    }
}
