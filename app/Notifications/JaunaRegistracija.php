<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class JaunaRegistracija extends Notification
{
    use Queueable;

    public $skipper;

    public $yacht;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($skipper, $yacht)
    {
        $this->skipper = $skipper;
        $this->yacht = $yacht;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Jauns regates dalībnieks')
                    ->line('Vārds, uzvārds: ' . $skipper->name)
                    ->line('E-pasts: ' . $skipper->)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
