<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fleet extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function regatta()
    {
    	return $this->belongsTo(Regatta::class);
    }

    public function races()
    {
        return $this->hasMany(Race::class);
    }

    public function yachts()
    {
    	return $this->hasMany(Yacht::class);
    }
}
