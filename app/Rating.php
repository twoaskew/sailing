<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public $timestamps = false;

    public function yacht()
    {
    	return $this->belongsTo(Yacht::class);
    }
}
