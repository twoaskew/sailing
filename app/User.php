<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use HasRoles,
        Notifiable;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function type()
    {
        return 'admin';
    }

    public function getGuard()
    {
        return 'admin';
    }

    public function getRedirect()
    {
        return '/admin';
    }

    public static function getMainAdmin() {
        return static::where('email', config('sailing.main_admin'))->first();
    }

    /**
    * Get all of the skipper's EmailLogins.
    */
    public function emailLogins()
    {
        return $this->morphMany(EmailLogin::class, 'loginable');
    }

    public function createEmailLogin()
    {
        return $this->emailLogins()->create([
            'token' => str_random(30),
        ]);
}
}
