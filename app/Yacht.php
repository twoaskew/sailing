<?php

namespace App;

use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Yacht extends Model implements HasMedia
{
	use HasMediaTrait;

    protected $with = ['ratings'];

	protected $guarded = [];

    protected $appends = ['thumbnail'];

    protected $casts = [
        'registered' => 'boolean',
    ];

    public function scopeRegistered($query)
    {
        return $query->where('registered', 1);
    }

    public function skipper()
    {
    	return $this->belongsTo(Skipper::class);
    }

    public function participants()
    {
    	return $this->hasMany(Participant::class);
    }

    public function fleet()
    {
        return $this->belongsTo(Fleet::class);
    }

    public function results()
    {
        return $this->hasMany(Result::class);
    }

    public function addImage($image)
    {
    	$this->addMedia($image)
    		->toMediaCollection();
    }

    public function updateImage($image)
    {
        if ($this->hasThumbnail()) 
        {
            $this->getFirstMedia()->delete();
        }
        $this->addImage($image);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
              ->fit(Manipulations::FIT_CROP, 300, 200);
    }

    public function thumbnail()
    {
        return $this->hasThumbnail() ? $this->getFirstMedia()->getUrl('thumb') : '/images/yacht-no-image-gray.png';
    }

    protected function hasThumbnail()
    {
        return !is_null($this->getFirstMedia());
    }

    public function getThumbnailAttribute()
    {
        return $this->thumbnail();
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function getRating($type)
    {
        if ($this->group == 'lys') {
            return $this->ratings->first()->rating;
        }
        return $this->ratings->where('type', $type)->first()->rating;
    }
}
