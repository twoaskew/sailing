<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    public function regatta()
    {
    	return $this->belongsTo(Regatta::class);
    }

    public function races()
    {
        return $this->hasMany(Race::class);
    }

    public function yachts()
    {
    	return $this->regatta->yachts();
    }

    public function addResult($yacht, $time, $dnf = 0, $dns = 0, $dsq = 0)
    {
        if ($dnf || $dns || $dsq) {
            $time = null;
        }
    	if ($yacht instanceof Yacht) {
    		$yacht = $yacht->id;
    	}
    	return $this->results()->create([
            'time' => $time,
            'yacht_id' => $yacht,
            'dnf' => $dnf,
            'dns' => $dns,
            'dsq' => $dsq
        ]);
    }

    public function results()
    {
    	return $this->hasMany(Result::class);
    }
}
