<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Notice extends Model
{
    use Hastranslations;

    public $translatable = ['title', 'body', 'slug'];
}
