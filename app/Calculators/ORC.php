<?php

namespace App\Calculators;

class ORC {

	public function getCorrectedTime($result)
	{
		$raceType = $this->getRaceType($result);
		$scoringOption = $this->getScoringOption($result);
		if ($scoringOption === 'ToD') {
			$ToD = $this->getRatingFor($raceType, $result->yacht, 'ToD');
			return intval(round($result->et - $ToD * $result->race->distance ));
		}
		if ($scoringOption === 'ToT') {
			$ToT = $this->getRatingFor($raceType, $result->yacht, 'ToT');
			return intval(round($result->et * $ToT));
		}
	}

	protected function getRaceType($result)
	{
		return $result->race->type;
	}

	protected function getScoringOption($result)
	{
		return $result->race->scoring_option;
	}

	protected function getRatingFor($type, $yacht, $scoring_option)
	{
		return $yacht->ratings->where('type', $type)->where('scoring_option', $scoring_option)->first()->rating;
	}
}