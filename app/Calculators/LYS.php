<?php

namespace App\Calculators;

class LYS {
	
	public function getCorrectedTime($result)
	{
		$rating = $result->yacht->ratings->first()->rating;
		return intval(round($result->et * $rating));
	}

}