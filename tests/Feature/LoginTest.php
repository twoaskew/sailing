<?php

namespace Tests\Feature;

use App\Skipper;
use Tests\TestCase;
use App\Participant;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function en_skippers_locale_is_used_in_login()
    {
    	// given we have an EN skipper and a valid EmailLogin
    	$skipper = factory(Skipper::class)->create([
    		'locale' => 'en',
    	]);
    	$emailLogin = $skipper->emailLogins()->create([
            'token' => str_random(30),
        ]);

    	// when he logs in
    	$response = $this->get("/auth/email-authenticate/{$emailLogin->token}");

    	// he should be redirected to "/en/skipper/my-application" page
    	$response->assertRedirect("/en/skipper/my-application");
    }

    /** @test */
    public function lv_skippers_locale_is_used_in_login()
    {
    	// given we have an EN skipper and a valid EmailLogin
    	$skipper = factory(Skipper::class)->create([
    		'locale' => 'lv',
    	]);
    	$emailLogin = $skipper->emailLogins()->create([
    		'token' => str_random(30),
    	]);

    	// when skipper logs in
    	$response = $this->get("/auth/email-authenticate/{$emailLogin->token}");

    	// he should be redirected to "/lv/kapteinis/mans-pieteikums" page
    	$response->assertRedirect("/lv/kapteinis/mans-pieteikums");
    }

    /** @test */
    public function en_participants_locale_is_used_in_login()
    {
    	// given we have an EN participant and a valid EmailLogin
    	$participant = factory(Participant::class)->create([
    		'locale' => 'en',
    		'yacht_id' => null,
    	]);
    	$emailLogin = $participant->emailLogins()->create([
            'token' => str_random(30),
        ]);

    	// when he logs in
    	$response = $this->get("/auth/email-authenticate/{$emailLogin->token}");

    	// he should be redirected to "/en/participant/my-application" page
    	$response->assertRedirect("/en/participant/my-application");
    }

    /** @test */
    public function lv_participant_locale_is_used_in_login()
    {
    	// given we have a Latvian Participant and an EmailLogin
    	$participant = factory(Participant::class)->create([
    		'locale' => 'lv',
    		'yacht_id' => null,
    	]);
    	$emailLogin = $participant->emailLogins()->create([
    		'token' => str_random(30),
    	]);

    	// when participant logs in
    	$response = $this->get("/auth/email-authenticate/{$emailLogin->token}");

    	// he should be redirected to "/lv/dalibnieks/mans-pieteikums" page
    	$response->assertRedirect("/lv/dalibnieks/mans-pieteikums");
    }
}
