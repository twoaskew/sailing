<?php

namespace Tests\Feature;

use App\User;
use App\Yacht;
use App\Regatta;
use App\Skipper;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkipperRegistrationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function when_a_skipper_registers_they_are_included_in_the_skippers_table()
    {
        $regatta = Regatta::create(['name' => 'foobar']);
        
        $response = $this->post('/skippers', [
            'name' => 'John Doe',
            'email' => 'johndoe@example.org',
            'phone' => '29000000',
            'locale' => 'en',
            'yacht_name' => 'ABC',
            'sail_number' => 'LV123',
            'yacht_club' => 'Foobar Yachting',
            'group' => 'orc',
            'loa' => 14,
            'make_model' => 'Benetti X-41',
            'year' => '2004',
            'team_size' => 3,
            'can_accept_people' => true,
            'nor_read' => true,
            'regatta_id' => $regatta->id,
        ]);

        $this->assertCount(1, $regatta->fresh()->skippers, 'skipper was NOT created');

    }

    /** @test */
    public function skippers_can_update_their_info()
    {
        // if we have a skipper with english locale
        $skipper = factory(Skipper::class)->create(['locale' => 'en']);
        $yacht = factory(Yacht::class)->create(['skipper_id' => $skipper->id]);
        // wheh the skipper logs in
        // and updates his/her info
        $reponse = $this->actingAs($skipper, 'skipper')->patch("/skippers/{$skipper->id}", [
            'name' => 'Foo bar',
            'phone' => $skipper->phone,
            'yacht_name' => $yacht->name,
            'sail_number' => $yacht->sail_number,
            'yacht_club' => $yacht->yacht_club,
            'group' => $yacht->group,
            'loa' => $yacht->loa,
            'make_model' => $yacht->make_model,
            'year' => $yacht->year,
            'team_size' => $yacht->team_size,
            'can_accept_people' => $yacht->can_accept_people,
        ]);

        $reponse->assertStatus(302);
        // the changes should be reflected in the database
        $this->assertDatabaseHas('skippers', [
            'name' => 'Foo bar',
            'phone' => $skipper->phone,
            'email' => $skipper->email,
        ]);
        
    }
}
