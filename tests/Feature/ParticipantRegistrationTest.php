<?php

namespace Tests\Feature;

use App\User;
use App\Regatta;
use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParticipantRegistrationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function participants_can_open_registration_page()
    {
        $response = $this->get('/registracija-dalibniekiem');

        $response->assertStatus(200);
    }

    /** @test */
    public function when_a_participant_registers_they_are_included_in_the_participants_table()
    {
        $regatta = Regatta::create(['name' => 'foobar']);
        $response = $this->post('/participants', [
            'name' => 'John Doe',
            'email' => 'johndoe@example.org',
            'phone' => '29000000',
            'description' => 'Foo bar, biz baz',
            'facebook_link' => '',
            'regatta_id' => $regatta->id,
        ]);

        $response->assertSuccessful();

        $this->assertCount(1, $regatta->fresh()->participants, 'the participant was NOT created');

        // when admin visits the participants index page
        Role::create(['name' => 'admin']);
        $this->actingAs(factory(User::class)->create()->assignRole('admin'));
        $adminPage = $this->get('/administracija/dalibnieki');
        // the user should be visible there
        $adminPage->assertSee('John Doe');


    }

    /** @test */
    public function name_email_phone_and_description_is_required()
    {
        $response = $this->withExceptionHandling()->post('/participants', [
            'email' => 'foobar',
            'phone' => '',
            'description' => '',
            'facebook_link' => '',
            'regatta_id' => '',
        ]);

        $response->assertSessionHasErrors(['email', 'description', 'phone', 'name', 'regatta_id']);
    }
}
