<?php

namespace Tests\Unit;

use App\Stage;
use App\Yacht;
use App\Regatta;
use App\Skipper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StageTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function it_belongs_to_a_regatta()
    {
    	$stage = factory(Stage::class)->create();

    	$this->assertNotNull($stage->regatta);
    }

    /** @test */
    public function it_has_yachts()
    {
    	$regatta = factory(Regatta::class)->create();
    	$skippers = factory(Skipper::class, 3)->create(['regatta_id' => $regatta->id]);
    	$skippers->each(function($s) {
    		factory(Yacht::class)->create(['skipper_id' => $s->id]);
    	});

    	$stage = factory(Stage::class)->create(['regatta_id' => $regatta->id]);

    	$this->assertEquals($stage->yachts->count(), 3);
    }

    /** @test */
    public function it_has_groups()
    {
    	$regatta = factory(Regatta::class)->create();
    	$stage = factory(Stage::class)->create(['regatta_id' => $regatta->id]);
    	// add 3 skippers with LYS yachts
    	$skippers = factory(Skipper::class, 3)->create(['regatta_id' => $regatta->id]);
    	$skippers->each(function($s) {
    		factory(Yacht::class)->create(['skipper_id' => $s->id, 'group' => 'lys']);
    	});

    	// add 2 skippers with ORC yachts
    	$skippersOrc = factory(Skipper::class, 2)->create(['regatta_id' => $regatta->id]);
    	$skippersOrc->each(function($s) {
    		factory(Yacht::class)->create(['skipper_id' => $s->id, 'group' => 'orc']);
    	});

    	$this->assertEquals(3, $regatta->yachtsInGroup('lys')->count());
    }
}
