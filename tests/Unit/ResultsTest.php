<?php

namespace Tests\Unit;

use App\Stage;
use App\Yacht;
use App\Result;
use App\Regatta;
use App\Results;
use App\Skipper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function result_belongs_to_a_yacht_and_stage()
    {
    	$yacht = factory(Yacht::class)->create();
    	$stage = factory(Stage::class)->create([
    		'regatta_id' => $yacht->skipper->regatta_id,
    		'starts_at' => '12:00:00',
    	]);

    	$stage->addResult($yacht, '13:24:05');
    	$result = Result::find(1);

    	$this->assertNotNull($result);
    	$this->assertNotNull($result->yacht);
    	$this->assertNotNull($result->stage);
    }

    /** @test */
    public function result_calculates_deltas_and_adjusted_time()
    {
    	$yacht = factory(Yacht::class)->create(['group' => 'lys', 'rating' => 1.234]);
    	$stage = factory(Stage::class)->create([
    		'regatta_id' => $yacht->skipper->regatta_id,
    		'starts_at' => '12:00:00',
    	]);

    	$stage->addResult($yacht, '13:24:05');
    	$result = Result::find(1);

    	$this->assertEquals(5045, $result->et);
    	$this->assertEquals(6226, $result->ct);
    }

    /** @test */
    public function repo_orders_teams_by_fastest_adjusted_times()
    {
        $regatta = factory(Regatta::class)->create();

        $yacht1 = $this->create_yacht_for_regatta($regatta, ['group' => 'lys', 'rating' => 1.234]);
        $yacht2 = $this->create_yacht_for_regatta($regatta, ['group' => 'lys', 'rating' => 1.234]);
        $yacht3 = $this->create_yacht_for_regatta($regatta, ['group' => 'lys', 'rating' => 1.234, 'name' => 'AA']);
        $yacht4 = $this->create_yacht_for_regatta($regatta, ['group' => 'lys', 'rating' => 1.234, 'name' => 'BB']);

        $stage = factory(Stage::class)->create([
            'name' => 'Rīga - Rīga',
            'regatta_id' => $regatta->id,
            'starts_at' => '12:00:00',
        ]);

        // yachts finish in the following order #2, #1, #3, #4
        $stage->addResult($yacht1, '12:24:20');
        $stage->addResult($yacht2, '12:00:40');
        $stage->addResult($yacht3, null, $dnf = true); 
        $stage->addResult($yacht4, null, false, $dns = true);

        $this->assertEquals(Results::forTable(), [
            'lys' => [
                [
                    'stages' => [
                        $stage 
                    ],
                    'results' => [
                        [
                            'yacht' => $yacht2,
                            'result' => [
                                ''
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }

    protected function create_yacht_for_regatta($regatta, $params)
    {
        $skipper = factory(Skipper::class)->create(['regatta_id' => $regatta->id]);
        $params['skipper_id'] = $skipper->id;
        return factory(Yacht::class)->create($params);
    }

}
