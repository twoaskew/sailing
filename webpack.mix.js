let mix = require('laravel-mix')
var tailwindcss = require('tailwindcss')
require('laravel-mix-purgecss')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
	.js('resources/js/turbolinks.js', 'public/js')
    .js('resources/js/gallery.js', 'public/js')
    .stylus('resources/stylus/index.styl', 'public/css')
    .options({
        postCss: [
            tailwindcss('./tailwind.js')
        ]
    })
    .copy('resources/images', 'public/images', false)
    .purgeCss()
    .version()