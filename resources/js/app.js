/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap')

window.Vue = require('vue')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Modal from './components/Modal'
import Timer from './components/Timer'
import ImageSelect from './components/ImageSelect'
import LabelWithModal from './components/LabelWithModal'
import Toast from './components/Toast'
import SkippersWrapper from './components/SkippersWrapper'
import Results from './components/Results'
import ImageGallery from './components/ImageGallery'
import AdminGallery from './components/AdminGallery'
// import InfoModal from './components/InfoModal'

import ProgressiveImage from 'progressive-image/dist/vue'

Vue.component('modal', Modal)
Vue.component('timer', Timer)
Vue.component('image-select', ImageSelect)
Vue.component('label-with-modal', LabelWithModal)
Vue.component('toast', Toast)
Vue.component('skippers-wrapper', SkippersWrapper)
Vue.component('results', Results)
Vue.component('image-gallery', ImageGallery)
Vue.component('admin-gallery', AdminGallery)
// Vue.component('info-modal', InfoModal)

Vue.use(ProgressiveImage, {
	removePreview: true
})

const app = new Vue({
    el: '#app'
})
