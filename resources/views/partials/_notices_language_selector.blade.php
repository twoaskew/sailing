<ul class="list-reset flex ml-auto">
    @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
        <li>
            <a rel="alternate" class="mr-2 no-underline uppercase font-bold text-sm text-30 tracking-wide @if(LaravelLocalization::getCurrentLocale() == $localeCode) text-color-primary @endif" hreflang="{{ $localeCode }}" href="/{{ $localeCode }}/{{__('routes.notices', [], $localeCode) }}/{{ $notice->getTranslation('slug', $localeCode ) }}">
                {{ $localeCode }}
            </a>
        </li>
    @endforeach
</ul>