<footer class="bg-white w-9/10 max-w-2xl mx-auto md:flex text-center md:text-left py-12 justify-between items-center flex-no-shrink">
	<div class="flex-1">
		<p class="font-semibold text-70 mb-4">{{ __('footer.title') }}</p>
		<p class="text-70 text-sm mb-1">{{ __('footer.subtitle_open') }}</p>
		<p class="text-70 text-sm">{{ __('footer.subtitle_livonia') }}</p>
	</div>
	<div class="mx-auto my-6 md:my-0 flex-1 text-center">
		<img src="{{ asset('/images/gorr_logo_2021_small_green.png') }}" alt="Gulf of Riga Regatta Logo">
	</div>
	<div class="flex-1 md:text-right">
		<p class="flex items-center mb-2 justify-center md:justify-end">
			<a href="https://www.youtube.com/channel/UCL_uQSw0K9kegAmZt3AtAfQ/featured" class="mr-4">
				<img src="{{ asset('/images/yt_logo.png') }}" alt="Youtube Logo">
			</a>
			<a href="https://www.facebook.com/gulfregatta" class="mr-4">
				<img src="{{ asset('/images/fb_logo.png') }}" alt="Facebook Logo">
			</a>
			<a href="https://www.instagram.com/gorr_lv/">
				<img src="{{ asset('/images/ig_logo.png') }}" alt="Instagram Logo">
			</a>
		</p>
		<p class="mb-2"><a href="tel:+37129247411" class="no-underline text-70 text-sm">+371 29247411</a></p>
		<p><a href="mailto:gulfregatta@gmail.com" class="no-underline text-70 text-sm">gulfregatta@gmail.com</a></p>
	</div>
</footer>