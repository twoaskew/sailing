<header class="hidden xl:block bg-blue-nav">
	<div class="w-9/10 mx-auto py-8 flex justify-between items-center">
		<ul class="list-reset flex items-center">
			<li><a class="mr-12" href="{{ route('home') }}"><img src="{{ asset('/images/gorr_logo_2022_small.png') }}" alt="Gulf of Riga Regatta logo"></a></li>
			{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('notices.index') }}">{{ __('navigation.notices') }}</a></li> --}}
			<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('news.index') }}">{{ __('navigation.news') }}</a></li>
			<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('galleries.index') }}">{{ __('navigation.gallery') }}</a></li>
			<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('info') }}">{{ __('navigation.info') }}</a></li>
			<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('skippers.registration') }}">{{ __('navigation.registration') }}</a></li>
			{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="https://ej.uz/u6bp" target="_blank">{{ __('navigation.join') }}</a></li> --}}
			{{-- <li>
				<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('skippers.index') }}">
					{{ __('navigation.participants') }}
					<span class="bg-color-primary rounded-full p-1 text-xs">'22</span>
				</a>
			</li> --}}
			{{-- <li>
				<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" target="_blank" href="https://gulfofrigaregatta.eu/regatta/2/documents/GoRR-2019-results.pdf">
					{{ __('navigation.results') }}
					<span class="bg-color-primary rounded-full p-1 text-xs">'19</span>
				</a>
			</li> --}}
			<li>
				<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('results') }}">
					{{ __('navigation.results') }}
					<span class="bg-color-primary rounded-full p-1 text-xs">'22</span>
				</a>
			</li>

			{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="http://shorestore.mozello.lv/" target="_blank">{{ __('navigation.merchandise') }}</a></li> --}}
			<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('contacts')}}">{{ __('navigation.contacts') }}</a></li>
			{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('archive.index', ['year' => 2018])}}">{{ __('navigation.archive') }}</a></li> --}}
		</ul>
		<div class="flex items-center">
			{{-- <a href="/ienakt" class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6">{{ __('navigation.login') }}</a> --}}
			@include('partials._notices_language_selector')
		</div>
	</div>
</header>

<header class="xl:hidden w-full py-8 bg-blue-nav">
	<div class="w-9/10 mx-auto">
		<input type="checkbox" id="menu">
		<div class="flex items-center">
			<label for="menu" class="mr-6 select-none">
				<svg width="27px" height="19px" viewBox="0 0 27 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			        <g transform="translate(-258.000000, -355.000000)" fill="#f1f4f1" fill-rule="nonzero">
			            <g id="menu-dark" transform="translate(258.000000, 355.000000)">
			                <path d="M0,0 L27,0 L27,2.7 L0,2.7 L0,0 Z M0,8.1 L27,8.1 L27,10.8 L0,10.8 L0,8.1 Z M0,16.2 L27,16.2 L27,18.9 L0,18.9 L0,16.2 Z" id="Shape"></path>
			            </g>
			        </g>
			    </g>
				</svg>
			</label>
			{{-- <a href="/ienakt" class="mr-6 ml-auto no-underline uppercase font-bold text-sm text-30 tracking-wide">{{ __('navigation.login') }}</a> --}}
			@include('partials._notices_language_selector')
		</div>
		<ul class="menu-content list-reset text-center mt-12">
			<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('home') }}">{{ __('navigation.home') }}</a></li>
			{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('notices.index') }}">{{ __('navigation.notices') }}</a></li> --}}
			<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('news.index') }}">{{ __('navigation.news') }}</a></li>
			<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('galleries.index') }}">{{ __('navigation.gallery') }}</a></li>
			<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('info') }}">{{ __('navigation.info') }}</a></li>
			<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('skippers.registration') }}">{{ __('navigation.registration') }}</a></li>
			{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="https://ej.uz/u6bp" target="_blank">{{ __('navigation.join') }}</a></li> --}}
			{{-- <li class="py-4">
				<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('skippers.index') }}">
					{{ __('navigation.participants') }}
					<span class="bg-color-primary rounded-full p-1 text-xs">'22</span>
				</a>
			</li> --}}
			{{-- <li class="py-4">
				<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('results') }}">
					{{ __('navigation.results') }}
					<span class="bg-color-primary rounded-full p-1 text-xs">'21</span>
				</a>
			</li> --}}
			
			{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" target="_blank" href="https://gulfofrigaregatta.eu/regatta/2/documents/GoRR-2019-results.pdf">{{ __('navigation.results') }}</a></li> --}}
			{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="http://shorestore.mozello.lv/" target="_blank">{{ __('navigation.merchandise') }}</a></li> --}}
			<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('contacts')}}">{{ __('navigation.contacts') }}</a></li>
			{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('archive.index', ['year' => 2018])}}">{{ __('navigation.archive') }}</a></li> --}}
		</ul>
	</div>
</header>