<div class="bg-50 pt-16 pb-24">
	<h2 class="text-center uppercase text-lg text-70 font-normal mb-12">{{ __('landingPage.organizing_team') }}</h2>
	<div class="w-9/10 max-w-lg mx-auto team-grid mb-12">
		{{-- Valters Romans --}}
		<div class="flex items-center">
			<img src="/images/valters-romans-img.png" alt="Valters Romans" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Valters Romans</h3>
				<p><a href="tel:+37129235659" class="text-sm text-70 no-underline mb-1">+371 29235659</a></p>
				<p><a href="mailto:valters@laivulietas.lv" class="text-sm text-70 no-underline">valters@laivulietas.lv</a></p>
			</div>
		</div>
		{{-- Kristine Kanska --}}
		<div class="flex items-center">
			<img src="/images/kristine-kanska-new-img.png" alt="Kristīne Kanska" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Kristīne Kanska</h3>
				<p><a href="tel:+37129247411" class="text-sm text-70 no-underline mb-1">+371 29247411</a></p>
				<p><a href="mailto:kanska1409@gmail.com" class="text-sm text-70 no-underline">kanska1409@gmail.com</a></p>
			</div>
		</div>
		{{-- Silards Kamergrauzis --}}
		<div class="flex items-center">
			<img src="/images/silards-kamergrauzis-img.png" alt="Silards Kamergrauzis" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Silards Kamergrauzis</h3>
				<p><a href="+37126434440" class="text-sm text-70 no-underline mb-1">+371 26434440</a></p>
				<p><a href="mailto:sk.firmamarisa@gmail.com" class="text-sm text-70 no-underline">sk.firmamarisa@gmail.com</a></p>
			</div>
		</div>
		{{-- Ilona Skorobogatova --}}
		<div class="flex items-center">
			<img src="/images/ilona.png" alt="Ilona Skorobogatova" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Ilona Skorobogatova</h3>
				<p><a href="+37125922782" class="text-sm text-70 no-underline mb-1">+371 25922782</a></p>
				<p><a href="mailto:ilona.skorobogatova@gmail.com" class="text-sm text-70 no-underline">ilona.skorobogatova@gmail.com</a></p>
			</div>
		</div>
		{{-- Piret Tank --}}
		<div class="flex items-center">
			<img src="/images/piret-tank-img.png" alt="Piret Tank" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Piret Ausman</h3>
				<p><a href="+37256500295" class="text-sm text-70 no-underline mb-1">+372 56500295</a></p>
				<p><a href="mailto:piret.tank@gmail.com" class="text-sm text-70 no-underline">piret.tank@gmail.com</a></p>
			</div>
		</div>
		{{-- Ilze Romane --}}
		<div class="flex items-center">
			<img src="/images/ilze-romane-img.png" alt="Ilze Romane" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Ilze Romane</h3>
				<p><a href="mailto:ilze.romane@gmail.com" class="text-sm text-70 no-underline">ilze.romane@gmail.com</a></p>
			</div>
		</div>
		{{-- Jānis Norde --}}
		<div class="flex items-center">
			<img src="/images/janis-norde-img.png" alt="Jānis Norde" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Jānis Norde</h3>
				<p><a href="mailto:jn@sarmanorde.lv" class="text-sm text-70 no-underline">jn@sarmanorde.lv</a></p>
			</div>
		</div>
		{{-- Kārlis Straubergs --}}
		<div class="flex items-center">
			<img src="/images/karlis-straubergs-img.png" alt="Kārlis Straubergs" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Kārlis Straubergs</h3>
				<p><a href="mailto:karlis@laivulietas.lv" class="text-sm text-70 no-underline">karlis@laivulietas.lv</a></p>
			</div>
		</div>
		{{-- Inese Salina --}}
		<div class="flex items-center">
			<img src="/images/inese-salina-img.png" alt="Inese Saliņa" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Inese Saliņa</h3>
				<p><a href="mailto:inese.dizains@gmail.com" class="text-sm text-70 no-underline">inese.dizains@gmail.com</a></p>
			</div>
		</div>
		{{-- Davis Norde --}}
		<div class="flex items-center">
			<img src="/images/davis-norde-img.png" alt="Davis Norde" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Dāvis Norde</h3>
				<p><a href="mailto:davis@twoaskew.com" class="text-sm text-70 no-underline">davis@twoaskew.com</a></p>
			</div>
		</div>
		{{-- Aiva Zumente --}}
		<div class="flex items-center">
			<img src="/images/aiva-zumente-img.png" alt="Aiva Zumente" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Aiva Zumente</h3>
				<p><a href="+37129480825" class="text-sm text-70 no-underline mb-1">+371 29480825</a></p>
				<p><a href="mailto:aivazumente@gmail.com" class="text-sm text-70 no-underline">aivazumente@gmail.com</a></p>
			</div>
		</div>
		{{-- Liva Krigere --}}
		<div class="flex items-center">
			<img src="/images/liva-krigere-img.png" alt="Liva Krigere" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Līva Krīgere</h3>
				<p><a href="+37128814002" class="text-sm text-70 no-underline mb-1">+371 28814002</a></p>
				<p><a href="mailto:liva@sailinglatvia.lv" class="text-sm text-70 no-underline">liva@sailinglatvia.lv</a></p>
			</div>
		</div>
	</div>

	<h2 class="text-center uppercase text-lg text-70 font-normal mb-12">{{ __('landingPage.friends_promoters') }}</h2>
	<div class="w-9/10 max-w-lg mx-auto team-grid">
		{{-- Rain Riim --}}
		<div class="flex items-center">
			<img src="/images/rain-riim-img.png" alt="Rain Riim" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Rain Riim</h3>
				<p><a href="mailto:rain.riim@tridens.ee" class="text-sm text-70 no-underline">rain.riim@tridens.ee</a></p>
			</div>
		</div>
		{{-- Madis Ausman --}}
		<div class="flex items-center">
			<img src="/images/madis-ausman-img.png" alt="Madis Ausman" class="w-16 md:w-24">
			<div class="ml-6">
				<h3 class="text-color-primary text-2xl font-light mb-2">Madis Ausman</h3>
				<p class="text-sm text-70 mb-1">{{ __('landingPage.ausmanInfo') }}</p>
				<p><a href="mailto:madis@neverland.ee" class="text-sm text-70 no-underline">madis@neverland.ee</a></p>
			</div>
		</div>
	</div>
</div>