<section class="pt-20 pb-12 bg-30">
	<div class="w-9/10 max-w-md mx-auto">
		<h2 class="uppercase text-lg text-60 font-normal mb-6">{{ __('landingPage.calendar') }}</h2>

		{{-- day 1 --}}
		<div class="flex border-t border-50 border-solid pt-6 mb-6">
			<div class="border-color-primary border-2 flex justify-center flex-col items-center px-3 py-2 mr-8 self-start">
				<span class="text-color-primary text-2xl">{{ __('landingPage.date1') }}</span>
				<span class="text-color-primary text-base">{{ __('landingPage.june') }}</span>
			</div>
			<div class="max-w-sm">
				<h4 class="text-xl text-100 mb-2">{{ __('landingPage.day1-title') }}</h4>
				<div class="text-90 leading-normal">{!! __('landingPage.day1-body') !!}</div>
			</div>
		</div>
		
		{{-- day 2 --}}
		<div class="flex border-t border-50 border-solid pt-6 mb-6">
			<div class="border-color-primary border-2 flex justify-center flex-col items-center px-3 py-2 mr-8 self-start">
				<span class="text-color-primary text-2xl">{{ __('landingPage.date2') }}</span>
				<span class="text-color-primary text-base">{{ __('landingPage.june') }}</span>
			</div>
			<div class="max-w-sm">
				<h4 class="text-xl text-100 mb-2">{{ __('landingPage.day2-title') }}</h4>
				<div class="text-90 leading-normal">{!! __('landingPage.day2-body') !!}</div>
			</div>
		</div>

		{{-- day 3 --}}
		<div class="flex border-t border-50 border-solid pt-6 mb-6">
			<div class="border-color-primary border-2 flex justify-center flex-col items-center px-3 py-2 mr-8 self-start">
				<span class="text-color-primary text-2xl">{{ __('landingPage.date3') }}</span>
				<span class="text-color-primary text-base">{{ __('landingPage.june') }}</span>
			</div>
			<div class="max-w-sm">
				<h4 class="text-xl text-100 mb-2">{{ __('landingPage.day3-title') }}</h4>
				<div class="text-90 leading-normal">{!! __('landingPage.day3-body') !!}</div>
			</div>
		</div>

		{{-- day 4 --}}
		<div class="flex border-t border-50 border-solid pt-6 mb-6">
			<div class="border-color-primary border-2 flex justify-center flex-col items-center px-3 py-2 mr-8 self-start">
				<span class="text-color-primary text-2xl">{{ __('landingPage.date4') }}</span>
				<span class="text-color-primary text-base">{{ __('landingPage.june') }}</span>
			</div>
			<div class="max-w-sm">
				<h4 class="text-xl text-100 mb-2">{{ __('landingPage.day4-title') }}</h4>
				<div class="text-90 leading-normal">{!! __('landingPage.day4-body') !!}</div>
			</div>
		</div>

		{{-- day 5 --}}
		<div class="flex border-t border-50 border-solid pt-6 mb-6">
			<div class="border-color-primary border-2 flex justify-center flex-col items-center px-3 py-2 mr-8 self-start">
				<span class="text-color-primary text-2xl">{{ __('landingPage.date5') }}</span>
				<span class="text-color-primary text-base">{{ __('landingPage.june') }}</span>
			</div>
			<div class="max-w-sm">
				<h4 class="text-xl text-100 mb-2">{{ __('landingPage.day5-title') }}</h4>
				<div class="text-90 leading-normal">{!! __('landingPage.day5-body') !!}</div>
			</div>
		</div>

		{{-- day 6 --}}
		<div class="flex border-t border-50 border-solid pt-6 mb-6">
			<div class="border-color-primary border-2 flex justify-center flex-col items-center px-3 py-2 mr-8 self-start">
				<span class="text-color-primary text-2xl">{{ __('landingPage.date6') }}</span>
				<span class="text-color-primary text-base">{{ __('landingPage.june') }}</span>
			</div>
			<div class="max-w-sm">
				<h4 class="text-xl text-100 mb-2">{{ __('landingPage.day6-title') }}</h4>
				<div class="text-90 leading-normal">{!! __('landingPage.day6-body') !!}</div>
			</div>
		</div>

		{{-- day 7 --}}
		<div class="flex border-t border-50 border-solid pt-6 mb-6">
			<div class="border-color-primary border-2 flex justify-center flex-col items-center px-3 py-2 mr-8 self-start">
				<span class="text-color-primary text-2xl">{{ __('landingPage.date7') }}</span>
				<span class="text-color-primary text-base">{{ __('landingPage.june') }}</span>
			</div>
			<div class="max-w-sm">
				<h4 class="text-xl text-100 mb-2">{{ __('landingPage.day7-title') }}</h4>
				<div class="text-90 leading-normal">{!! __('landingPage.day7-body') !!}</div>
			</div>
		</div>

		{{-- <div class="text-90 text-sm mt-16">{{ __('landingPage.calendarFooter') }}</div> --}}

	</div>
</section>