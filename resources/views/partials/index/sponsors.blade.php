<div class="w-9/10 max-w-2xl mx-auto mb-24 pt-16">
	<h2 class="text-center uppercase text-lg text-60 font-normal mb-16">{{ __('landingPage.partners') }}</h2>
	{{-- <div class="mb-16 sponsors-grid">
		<a href="https://www.upb.lv/" target="_blank" class="sponsors-grid-item-offset"><img src="{{ asset('/images/logos/upb.png') }}" alt="UPB Logo"></a>
		<a href="http://rop.lv/en/" target="_blank"><img src="{{ asset('/images/logos/rbo-end.png') }}" alt="Freeport of Riga Logo"></a>
	</div> --}}
	<div class="mb-16 flex justify-center">
		<a href="http://rop.lv/en/" target="_blank"><img src="{{ asset('/images/logos/rbo-end.png') }}" alt="Freeport of Riga Logo"></a>
	</div>
	<div class="mb-24 sponsors-grid">
		<a href="http://boatpark.lv/index.php/contacts/" target="_blank"><img src="{{ asset('/images/logos/boatpark-logo.png') }}" alt="Boatpark Pavilosta Logo"></a>
		{{-- <a href="https://www.anitra.lv/lv/par-anitra" target="_blank"><img src="{{ asset('/images/logos/anitra-2022.png') }}" alt="Anitra Logo"></a> --}}
		{{-- <a href="http://eastbaltic.eu/" target="_blank"><img src="{{ asset('/images/logos/ebc.png') }}" alt="East Coast Baltic Logo"></a> --}}
		<a href="https://www.instagram.com/janis_spurdzins/" target="_blank"><img src="{{ asset('/images/logos/janis-spurdzins.png') }}" alt="Janis Spurdzins Logo"></a>
		<a href="http://laivulietas.lv/" target="_blank"><img src="{{ asset('/images/logos/laivulietas.png') }}" alt="Laivu lietas Logo"></a>
		{{-- <a href="https://www.stevespencer.tv/" target="_blank"><img src="{{ asset('/images/logos/steve-spencer.png') }}" alt="Steve Spencer Logo"></a> --}}
		{{-- <a href="https://www.prestolboats.com/" target="_blank"><img src="{{ asset('/images/logos/prestol.png') }}" alt="Prestol Logo"></a> --}}
		{{-- <a href="https://coopsaaremaa.ee/" target="_blank"><img src="{{ asset('/images/logos/coop-saaremaa.png') }}" alt="Coop Saaremaa Logo"></a> --}}
		{{-- <a href="https://www.instagram.com/hugogrills/" target="_blank"><img src="{{ asset('/images/logos/hugo-2021.png') }}" alt="Hugo Grills Logo"></a> --}}
		{{-- <a href="http://www.linstow.lv/" target="_blank"><img src="{{ asset('/images/logos/linstow.png') }}" alt="Linstow Logo"></a> --}}
		{{-- <a href="https://www.ventspilsmarina.lv/" target="_blank"><img src="{{ asset('/images/logos/ventspils-jahtu-osta.png') }}" alt="Ventspils jahtu osta Logo"></a> --}}
		{{-- <a href="https://www.kapteinuosta.lv/" target="_blank"><img src="{{ asset('/images/logos/kapteinu-osta.png') }}" alt="Kapteinu osta Logo"></a> --}}
		<a href="https://www.facebook.com/inese.salina" target="_blank"><img src="{{ asset('/images/logos/inese-salina-2021.png') }}" alt="Designed by Inese Salina Logo"></a>
		{{-- <a href="https://salacgrivaport.lv/" target="_blank"><img src="{{ asset('/images/logos/salacgrivas-ostas-parvalde.png') }}" alt="Salacgrivas ostas parvalde Logo"></a> --}}
		{{-- <a href="http://www.apsara.lv/" target="_blank"><img src="{{ asset('/images/logos/apsara.png') }}" alt="Apsara Logo"></a> --}}
		{{-- <a href="https://scarpawine.com/" target="_blank"><img src="{{ asset('/images/logos/scarpa.png') }}" alt="Scarpa Logo"></a> --}}
		<a href="https://www.delfi.lv/" target="_blank"><img src="{{ asset('/images/logos/delfi-2022.png') }}" alt="Delfi Logo"></a>
		{{-- <a href="https://kompozits.lv/" target="_blank"><img src="{{ asset('/images/logos/kompozits.png') }}" alt="Kompozits Logo"></a> --}}
		<a href="http://www.sailinglatvia.lv/" target="_blank"><img src="{{ asset('/images/logos/sailing-latvia.png') }}" alt="Sailing Latvia Logo"></a>
		{{-- <a href="https://www.redbull.com/lv-lv/" target="_blank"><img src="{{ asset('/images/logos/red-bull-new.png') }}" alt="Red Bull Logo"></a> --}}
		<a href="http://sarmanorde.lv/" target="_blank"><img src="{{ asset('/images/logos/sarmanorde.png') }}" alt="Sarma & Norde Architects Logo"></a>
		{{-- <a href="http://www.lhei.lv/" target="_blank"><img src="{{ asset('/images/logos/lhi.png') }}" alt="Latvijas Hidreoekoloģijas Institūts Logo"></a> --}}
		{{-- <a href="https://www.marine.ee/en" target="_blank"><img src="{{ asset('/images/logos/alter-marine.png') }}" alt="Alter Marine Logo"></a> --}}
		<a href="http://roja.lv/" target="_blank"><img src="{{ asset('/images/logos/roja.png') }}" alt="Roja Logo"></a>
		<a href="http://www.saaremaamerispordiselts.ee/" target="_blank"><img src="{{ asset('/images/logos/saaremaa.png') }}" alt="Saaremaa Logo"></a>
		{{-- <a href="https://jahtklubi.ee/jahtklubi/uudised" target="_blank"><img src="{{ asset('/images/logos/parnu-yaht-klubi.png') }}" alt="Parnu Yacht Klubi Logo"></a> --}}
		<span><img src="{{ asset('/images/logos/ljba-2021.png') }}" alt="LJBA Logo"></span>
		{{-- <a href="https://www.egl.lv/" target="_blank"><img src="{{ asset('/images/logos/egl.png') }}" alt="EGL Logo"></a> --}}
		{{-- <a href="https://www.valmiermuiza.lv/" target="_blank"><img src="{{ asset('/images/logos/valmiermuiza.png') }}" alt="Valmiermuiza Logo"></a> --}}
		{{-- <a href="https://www.tele2.lv/" target="_blank"><img src="{{ asset('/images/logos/tele2.png') }}" alt="Tele2 Logo"></a> --}}
		{{-- <a href="https://kokmuiza.lv/" target="_blank"><img src="{{ asset('/images/logos/kokmuiza.png') }}" alt="Kokmuiza Logo"></a> --}}
		{{-- <a href="https://www.jagermeister.com/lv-LV/taste-remastered" target="_blank"><img src="{{ asset('/images/logos/jagermeister.png') }}" alt="Jagermeister Logo"></a> --}}
		{{-- <a href="https://www.facebook.com/AlusBursh/" target="_blank"><img src="{{ asset('/images/logos/bursh.png') }}" alt="Bursh Logo"></a> --}}
		<a href="https://www.facebook.com/FotoMan.lv/" target="_blank"><img src="{{ asset('/images/logos/fotomanlv.png') }}" alt="FotoManLV Logo"></a>
		<a href="https://www.facebook.com/liona.tagat/photos" target="_blank"><img src="{{ asset('/images/logos/tagat-2023.png') }}" alt="Tagat Logo"></a>
		<a href="https://www.knauf.lv/" target="_blank"><img src="{{ asset('/images/logos/knauf.png') }}" alt="Knauf Logo"></a>
		{{-- <a href="http://www.zalais.lv/" target="_blank"><img src="{{ asset('/images/logos/latvijas-zalais-punkts.png') }}" alt="Latvijas Zalais Punkts Logo"></a> --}}
		<a href="https://kate.lv/" target="_blank"><img src="{{ asset('/images/logos/kate.png') }}" alt="Kate Logo"></a>
		{{-- <a href="https://www.brivaisvilnis.lv/" target="_blank"><img src="{{ asset('/images/logos/brivais-vilnis-2022-2.png') }}" alt="Brivais Vilnis Logo"></a> --}}
		{{-- <a href="https://rop.lv/" target="_blank"><img src="{{ asset('/images/logos/rigas-brivosta-2021.png') }}" alt="Rigas Brivosta Logo"></a> --}}
		{{-- <a href="https://www.dabasmaize.lv/" target="_blank"><img src="{{ asset('/images/logos/dabas-maize-2021.png') }}" alt="Dabas Maize Logo"></a> --}}
		{{-- <a href="https://kompozits.lv/" target="_blank"><img src="{{ asset('/images/logos/kompozits-2021.png') }}" alt="Kompozits Logo"></a> --}}
		{{-- <a href="https://www.lmt.lv/" target="_blank"><img src="{{ asset('/images/logos/lmt-2021.png') }}" alt="LMT Logo"></a> --}}
		{{-- <a href="https://www.pilsetasjahtklubs.lv/" target="_blank"><img src="{{ asset('/images/logos/pilsetas-jahtklubs-2021.png') }}" alt="Pilsetas Jahtklubs Logo"></a> --}}
		{{-- <a href="https://pupumaiss.lv/" target="_blank"><img src="{{ asset('/images/logos/pupumaiss-2021.png') }}" alt="Pupu Maiss Logo"></a> --}}
		{{-- <a href="http://www.sabilessidrs.lv/" target="_blank"><img src="{{ asset('/images/logos/sabiles-sidrs-2021.png') }}" alt="Sabiles Sidrs Logo"></a> --}}
		<a href="https://www.skyroutefilms.com/" target="_blank"><img src="{{ asset('/images/logos/sky-route-films-2021.png') }}" alt="Sky Route Films Logo"></a>
		{{-- <a href="https://www.facebook.com/tastemakerlv/" target="_blank"><img src="{{ asset('/images/logos/taste-maker-2021.png') }}" alt="Taste Maker Logo"></a> --}}
		{{-- <a href="https://www.yachting.lv/" target="_blank"><img src="{{ asset('/images/logos/yachting-lv-2021.png') }}" alt="Yachting LV Logo"></a> --}}
		{{-- <a href="https://toitoi.lv/" target="_blank"><img src="{{ asset('/images/logos/toi-toi.png') }}" alt="Toi Toi Latvija Logo"></a> --}}
		{{-- <a href="https://www.doka.com/" target="_blank"><img src="{{ asset('/images/logos/doka.png') }}" alt="Doka Logo"></a> --}}
		{{-- <a href="https://ramkalni.lv/" target="_blank"><img src="{{ asset('/images/logos/ramkalni.png') }}" alt="Ramkalni Logo"></a> --}}
		<a href="https://www.trodo.com/" target="_blank"><img src="{{ asset('/images/logos/trodo.png') }}" alt="Trodo Logo"></a>
		{{-- <a href="https://www.izm.gov.lv/lv" target="_blank"><img src="{{ asset('/images/logos/izm-2022.png') }}" alt="Izglitibas ministrija Logo"></a> --}}
		{{-- <a href="https://elvstromsails.com/" target="_blank"><img src="{{ asset('/images/logos/elvstrom-2022.png') }}" alt="Elvstrom Sails Logo"></a> --}}
		{{-- <a href="https://www.koigustemarina.com/" target="_blank"><img src="{{ asset('/images/logos/koiguste-2022.png') }}" alt="Koiguste Marina Logo"></a> --}}
		{{-- <a href="https://lsfp.lv/" target="_blank"><img src="{{ asset('/images/logos/lsfp-2022.png') }}" alt="LSFP Logo"></a> --}}
		{{-- <a href="https://www.roblineropes.com/en/" target="_blank"><img src="{{ asset('/images/logos/robline-2022.png') }}" alt="Robline Logo"></a> --}}
		{{-- <a href="https://www.tgsbaltic.com/" target="_blank"><img src="{{ asset('/images/logos/tgs-baltic-2022.png') }}" alt="TGS Baltic Logo"></a> --}}
		{{-- <a href="http://printpack.lv/" target="_blank"><img src="{{ asset('/images/logos/printpack-2022.png') }}" alt="Printpack Service Logo"></a> --}}
		{{-- <a href="https://www.venden.lv/" target="_blank"><img src="{{ asset('/images/logos/venden-2022.png') }}" alt="Venden Logo"></a> --}}
		<a href="https://sadam.kuressaare.ee/en/" target="_blank"><img src="{{ asset('/images/logos/kuressaare-2023.png') }}" alt="Kuressaare Logo"></a>
		{{-- <a href="https://www.hafen.ee/" target="_blank"><img src="{{ asset('/images/logos/hafen-2022.png') }}" alt="Hafen Logo"></a> --}}
		<a href="https://bao.lv/" target="_blank"><img src="{{ asset('/images/logos/bao.png') }}" alt="Bao Logo"></a>
		<a href="https://talsunovads.lv/" target="_blank"><img src="{{ asset('/images/logos/talsu-novads.png') }}" alt="Talsu novads Logo"></a>
		<a href="https://lja.lv/" target="_blank"><img src="{{ asset('/images/logos/lja-logo.png') }}" alt="Latvijas Jūras Administrācija Logo"></a>

	</div>
	{{-- <h2 class="text-center uppercase text-lg text-60 font-normal mb-6">{{ __('landingPage.organizers') }}</h2>
	<div class="mb-24 sponsors-grid"></div> --}}

</div>