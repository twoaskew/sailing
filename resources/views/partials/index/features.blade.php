<div>
	<div class="md:flex py-8 justify-center mb-24 bg-blue-light">
		<div class="border-t-4 border-solid border-yellow-main py-8 pl-8 pr-16 md:mr-16 bg-white w-9/10 max-w-sm mx-auto md:mx-0">
			<div class="flex items-center mb-4">
				<img src="{{ asset('/images/icons/ship-icon.png') }}" alt="Ship icon" class="mr-4">
				<p class="text-sm w-56">{{ __('landingPage.feature1') }}</p>
			</div>
			<div class="flex items-center mb-4">
				<img src="{{ asset('/images/icons/trophy-icon.png') }}" alt="Trophy icon" class="mr-4">
				<p class="text-sm w-56">{{ __('landingPage.feature2') }}</p>
			</div>
			<div class="flex items-center">
				<img src="{{ asset('/images/icons/globe-icon.png') }}" alt="Globe icon" class="mr-4">
				<p class="text-sm w-56">{{ __('landingPage.feature3') }}</p>
			</div>
		</div>
		<div class="text-center self-center">
			{{-- <a href="{{ route('skippers.registration') }}" class="bg-yellow-main uppercase px-12 py-4 no-underline text-black font-bold tracking-wide shadow inline-block mt-6 md:mt-0">{{ __('landingPage.results') }}</a> --}}
			<a href="https://gulfofrigaregatta.eu/regatta/2/documents/GoRR-2019-results.pdf" target="_blank" class="bg-yellow-main uppercase px-12 py-4 no-underline text-black font-bold tracking-wide shadow inline-block mt-6 md:mt-0">{{ __('landingPage.results') }}</a>
		</div>
	</div>
</div>