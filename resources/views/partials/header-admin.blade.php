<header class="hidden xl:block bg-white shadow-md relative zi-10">
	<div class="w-9/10 max-w-2xl mx-auto py-8 flex justify-between items-baseline">
		<ul class="list-reset flex">
			{{-- <li><a class="mr-6 no-underline text-60 hover:text-100 text-sm uppercase font-bold tracking-wide" href="/posts">Jaunumi</a></li> --}}
			{{-- <li><a class="mr-6 no-underline text-60 hover:text-100 text-sm uppercase font-bold tracking-wide" href="/results">Rezultāti</a></li> --}}
			<li><a class="mr-6 no-underline text-60 hover:text-100 text-sm uppercase font-bold tracking-wide" href="{{ route('admin.galleries.index') }}">Galerijas</a></li>
		</ul>
		
		<div class="flex items-baseline">
			@if(Auth::check())
				<form action="{{ route('logout') }}" method="POST" class="inline-block">
					@csrf
					<button class="mr-6 no-underline text-green hover:text-white hover:bg-green  uppercase text-xs font-bold border rounded-sm border-solid border-green px-6 py-3 tracking-wide">{{ __('navigation.logout') }}</button>
				</form>
			@else
				<a href="{{ route('login') }}" class="mr-6 no-underline text-green hover:text-white hover:bg-green  uppercase text-xs font-bold border rounded-sm border-solid border-green px-6 py-3 tracking-wide">{{ __('navigation.login') }}</a>
			@endif
		</div>
	</div>
</header>