@if (Auth::check() && Auth::user()->type() == 'participant')
	@include('partials.header-participant')
@elseif (Auth::check() && Auth::user()->type() == 'skipper')
	@include('partials.header-skipper')
@elseif (Auth::check() && Auth::user()->type() == 'admin')
	@include('partials.header-admin')
@else
	@include('partials.header-default')
@endif