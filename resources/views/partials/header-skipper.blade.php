<header class="hidden xl:block bg-blue-nav">
	<div class="w-9/10 mx-auto py-8 flex justify-between items-center">
		<ul class="list-reset flex items-center">
			<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('skippers.dashboard') }}">{{ __('navigation.my_team') }}</a></li>
			{{-- <li><a class="mr-6 no-underline text-60 hover:text-100 text-sm uppercase font-bold tracking-wide" href="{{ route('participants.index') }}">{{ __('navigation.new_participants') }}</a></li> --}}
		</ul>
		
		<div class="flex items-baseline">
			<form action="{{ route('logout') }}" method="POST" class="inline-block">
				@csrf
				<button class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6">{{ __('navigation.logout') }}</button>
			</form>
			@include('partials._language_selector')
		</div>
	</div>
</header>

<header class="xl:hidden w-full py-8 bg-blue-nav">
	<div class="w-9/10 mx-auto">
		<input type="checkbox" id="menu">
		<div class="flex items-center">
			<label for="menu" class="mr-6 select-none">
				<svg width="27px" height="19px" viewBox="0 0 27 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			        <g transform="translate(-258.000000, -355.000000)" fill="#f1f4f1" fill-rule="nonzero">
			            <g id="menu-dark" transform="translate(258.000000, 355.000000)">
			                <path d="M0,0 L27,0 L27,2.7 L0,2.7 L0,0 Z M0,8.1 L27,8.1 L27,10.8 L0,10.8 L0,8.1 Z M0,16.2 L27,16.2 L27,18.9 L0,18.9 L0,16.2 Z" id="Shape"></path>
			            </g>
			        </g>
			    </g>
				</svg>
			</label>
			
			<form action="{{ route('logout') }}" method="POST" class="inline-block ml-auto">
				@csrf
				<button class="mr-6 no-underline uppercase font-bold text-sm text-30 tracking-wide">{{ __('navigation.logout') }}</button>
			</form>
			
			@include('partials._language_selector')
		</div>
		<ul class="menu-content list-reset text-center mt-12">
			<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('skippers.dashboard') }}">{{ __('navigation.my_team') }}</a></li>
			{{-- <li class="py-4"><a class="no-underline text-60 hover:text-100 text-lg uppercase font-bold tracking-wide" href="{{ route('participants.index') }}">{{ __('navigation.new_participants')  }}</a></li> --}}
		</ul>
	</div>
</header>