@extends('layouts.master')

@section('content')
    <div class="bg-blue-lightest flex-expand">
        <div class="bg-white w-9/10 xl:w-4/5 mt-16 mx-auto mb-16 px-12 py-16 rounded shadow-lg flex">
            
            <form class="flex-2" method="POST" action="{{ route('participants.store') }}">
                {{ csrf_field() }}
                <h1 class="font-bold text-2xl text-black tracking-wide">Reģistrācija regatei</h1>

                <p class="font-medium text-xs text-60 tracking-semi leading-loose w-3/4 mt-6">Ja vēlies piedalīties regatē, bet Tev nav savas komandas, atstāj savu kontaktinformāciju un Tev būs iespēja kļūt par kādas komandas dalībnieku, kas meklē sev papildinājumu.</p>

                <div class="mt-8">
                    <div class="flex items-baseline">
                        <div class="border-2 border-green rounded-full leading-none p-2 inline-block h-8 w-8 flex justify-center items-center mr-2">
                            <span class="text-lg text-green font-bold">1</span>
                        </div>
                        <h3 class="font-semibold text-lg text-black tracking-wide ">Kontaktinformācija</h3>
                    </div>

                    <div class="w-full md:w-96 mt-6">
                        <label class="form-label block mb-2" for="name">Vārds, uzvārds<span class="text-red text-lg ml-1">*</span></label>
                        <input class="form-input" id="name" name="name" placeholder="Valdis Burātājs" type="text">
                        @if ($errors->has('name'))
                            <p class="text-xs italic text-red font-regular mt-1">Norādi vārdu, uzvārdu</p>
                        @endif
                    </div>

                    <div class="w-full md:w-96 mt-6">
                        <label class="form-label block mb-2" for="email">E-pasts<span class="text-red text-lg ml-1">*</span></label>
                        <input class="form-input" id="email" name="email" placeholder="valdis@example.org" type="text">
                        @if ($errors->has('email'))
                            <p class="text-xs italic text-red font-regular mt-1">Norādi derīgu e-pasta adresi</p>
                        @endif
                    </div>

                    <div class="w-full md:w-64 mt-6">
                        <label class="form-label block mb-2" for="phone">Telefons<span class="text-red text-lg ml-1">*</span></label>
                        <input class="form-input" id="phone" name="phone" placeholder="+371 29123456" type="text">
                        @if ($errors->has('phone'))
                            <p class="text-xs italic text-red font-regular mt-1">Norādi savu telefona numuru</p>
                        @endif
                    </div>

                </div>

                <div class="border-b-2 border-20 mt-12"></div>

                <div class="mt-12">
                    <div class="flex items-baseline">
                        <div class="border-2 border-green rounded-full leading-none p-2 inline-block h-8 w-8 flex justify-center items-center mr-2">
                            <span class="text-lg text-green font-bold">2</span>
                        </div>
                        <h3 class="font-semibold text-lg text-black tracking-wide ">Par Tevi</h3>
                    </div>

                    <div class="w-full md:w-96 mt-8">
                        <label class="form-label block mb-2" for="description">Pastāsti par sevi<span class="text-red text-lg ml-1">*</span></label>
                        <textarea class="form-input" id="description" type="text" placeholder="Vai Tev ir pieredze burāšanā? Kādēļ vēlies piedalīties regatē? Vai Tev garšo rums?" name="description" rows="6"></textarea>
                        @if($errors->has('description'))
                            <p class="text-xs italic text-red font-regular mt-1">Lūdzu pastāsti par sevi</p>
                        @endif
                    </div>

                    <div class="w-full md:w-96 mt-8">
                        <label class="form-label block mb-2" for="facebook_link">Links uz Tavu Facebook profilu</label>
                        <input class="form-input" id="facebook_link" name="facebook_link" placeholder="http://facebook.com/valdisburatajs" type="text">
                    </div>

                </div>

                <div class="mt-12">
                    <p class="text-sm italic text-60 font-regular"><span class="text-red text-lg mr-1">*</span> Ar sarkanu zvaigznīti apzīmētie lauki aizpildāmi obligāti</p>
                </div>

                <div class="mt-12">
                    <button class="btn btn-green">Reģistrēties</button>
                </div>
            </form>
            
            <div class="flex-1 ml-12 hidden lg:block" style="background-image:url('/images/dark-sea.png');background-size:cover;background-repeat:none;background-position:top center;">
                
                <div class="mt-32 mx-8">
                    <p class="font-semibold text-white tracking-xl text-lg italic leading-tight">"This is the main sailing event in the Baltic Sea!"</p>
                    <p class="mt-3 text-right font-regular text-white text-sm tracking-wide">- Bjorn Johansson</p>
                </div>

                <div class="mt-16 mx-8">
                    <p class="font-semibold text-white tracking-xl text-lg italic leading-tight">"Latvijas atklātais čempionāts burāšanā sniedz lieliskas emocijas."</p>
                    <p class="mt-3 text-right font-regular text-white text-sm tracking-wide">- Andrejs Drīksna</p>
                </div>

                <div class="mt-16 mx-8">
                    <p class="font-semibold text-white tracking-xl text-lg italic leading-tight">"Piecas azarta un piedzīvojumu pilnas dienas lieliskā kompānijā!"</p>
                    <p class="mt-3 text-right font-regular text-white text-sm tracking-wide">- Leons Bortturis</p>
                </div>
            </div>
            
        </div>
    </div>
@endsection