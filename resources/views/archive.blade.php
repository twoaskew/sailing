@extends('layouts.master')

@section('head-js')
	<script>
		window.stages = {!! json_encode($stages) !!}
		window.fleets = {!! json_encode($fleets) !!}
	</script>
@endsection

@section('content')
	<section class="bg-30 pb-24 flex-expand" id="app">
		<h2 class="text-center text-2xl text-100 mb-16 pt-12 md:pt-24">{{ __('archive.title') }}</h2>
		{{-- news --}}
		<div class="w-9/10 max-w-md mx-auto">
			<h3 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12">{{ __('archive.news') }}</h3>
			@foreach ($posts as $post)
				<div class="mb-8 border-t border-50 border-solid flex flex-col md:flex-row">
					<div class="border-yellow-main border-2 flex justify-center items-center px-3 py-2 mr-8 self-start text-color-primary mt-8">{{ $post->created_at->format('d.m') }}</div>
					<div>
						<h3 class="text-2xl text-100 mb-2 mt-4 md:mt-8">{{ $post->title }}</h3>
						<p class="text-90 leading-normal">{!! nl2br($post->body) !!}</p>
					</div>
				</div>
			@endforeach
		</div>
		{{-- participants --}}
		<div class="w-9/10 xl:w-4/5 max-w-xl mx-auto relative mb-16">
			<h3 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12">{{ __('skippers.entriesTitle') }}</h3>
			@if ($skippers->count() > 0)
				<skippers-wrapper
			      title="{{ __('skippers.entriesTitle') }}"
			      subtitle="{{ __('skippers.entriesSubtitleArchive', ['skippers_count' => $skippers->count()]) }}"
			      :skippers="{{ json_encode($skippers->all()) }}">
			  </skippers-wrapper>
		  @endif
		</div>
		{{-- results --}}
		<div class="w-9/10 max-w-xl mx-auto mb-16">
			<h3 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12">{{ __('archive.results') }}</h3>
			<div class="border-t border-50 border-solid mb-12"></div>
			<results></results>
		</div>
		{{-- gallery --}}
		<div class="w-9/10 max-w-xl mx-auto">
			<h3 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12">{{ __('archive.gallery') }}</h3>
			<div class="border-t border-50 border-solid"></div>
			@foreach($galleries as $gallery)
				<a href="{{ route('archive.gallery', ['year' => $year,'gallery' => $gallery ])}}" class="no-underline">
					<h2 class="text-base font-normal text-90 mb-4 ml-2 mt-8 text-center sm:text-left">{{ $gallery->title }}</h2>
					<div class="sm:flex sm:justify-between mb-6">
						@foreach($gallery->images()->take(3) as $image)
							<div class="flex-1 mx-auto sm:mx-2 mb-2 cursor-pointer shadow-md max-w-xs sm:max-w-full">
								<img src="{{ $image->getUrl('thumb') }}" alt="" class="block">
							</div>
						@endforeach
					</div>
					<div class="text-right">
						<a href="{{ route('archive.gallery', ['year' => $year,'gallery' => $gallery ])}} }}" class="text-color-primary no-underline">{{ __('gallery.viewFullGalleryLink') }}</a>
					</div>
				</a>
			@endforeach
		</div>
	</section>
@endsection