@extends('layouts.master')

@section('content')
<div class="bg-30 py-24">
  <div class="w-9/10 mx-auto">
    <h1 class="text-center uppercase text-lg text-60 font-normal mb-6">{{ __('results.title') }}</h1>
    <div>
      @if (App::isLocale('lv'))
        <div class="iframe-container">
          <iframe src="https://lv.regattas.eu/iframe/lv/regatta_results/1468"></iframe>
        </div>
      @else
        <div class="iframe-container">
          <iframe src="https://lv.regattas.eu/iframe/en/regatta_results/1468"></iframe>
        </div>
      @endif
    </div>
  </div>
</div>
@endsection

{{-- @section('head-js')
	<script>
		window.stages = {!! json_encode($stages) !!}
		window.fleets = {!! json_encode($fleets) !!}
	</script>
@endsection --}}

{{-- @section('content')
<div class="flex-expand bg-30" id="app">
    <div class="w-9/10 max-w-xl mx-auto mb-16">
        <h3 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12">{{ __('archive.results') }}</h3>
        <p class="text-60 mb-6">{{ __('results.updated_at') }}: {{ $updated_at }}</p>
        <div class="border-t border-50 border-solid mb-6"></div>
        <results></results>
    </div>
</div>
@endsection --}}