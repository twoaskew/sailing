@extends('layouts.master')

@section('content')
{{-- <div class="flex-expand bg-cover" style="background-image:url('/images/sea.png');">
    <div class="bg-white w-9/10 xl:w-4/5 mt-16 mx-auto mb-16 px-6 sm:px-12 py-16 rounded shadow-lg text-center">
        <img src="/images/results-empty-state-image-gray.png" style="transform: rotate(-20deg);" class="w-16">
        <div class="font-semibold text-30 mb-6 text-green opacity-50" style="font-size: 10rem">404</div>
        <p class="text-60 text-lg leading-normal">{!! __('errors.404Text') !!}</p>
    </div>
</div> --}}
<section class="bg-30 pt-12 pb-24 flex-expand">
	<div class="w-9/10 max-w-md mx-auto text-center">
		<img src="/images/gorr_logo_19.5.png" class="w-16">
		<div class="font-semibold text-30 mb-6 text-color-primary text-center" style="font-size: 10rem">404</div>
		<p class="text-90 mx-auto text-center max-w-sm text-lg leading-normal">{!! __('errors.404Text') !!}</p>
	</div>
</section>
@endsection