@extends('layouts.landing')

@section('content')
	{{-- <div class="bg-cover bg-bottom" style="background: linear-gradient(#02426B, #011F39);"> --}}
	<div class="sm:flex text-center justify-center items-center py-4 bg-color-primary text-white">
		<p class="uppercase font-semibold">{{ __('landingPage.standWithUkraine') }}</p>
		<p class="ml-2 mt-1 sm:mt-0">#StandWithUkraine<span class="ml-2">🇺🇦</span></p>
	</div>
	<div class="bg-cover bg-bottom"
	style="background-image: url('images/hero_bg_2020.jpg');">
	{{-- <div class="bg-gray"> --}}
		<header class="hidden xl:block">
			<div class="w-9/10 mx-auto py-8 flex justify-between items-center">
				<ul class="list-reset flex items-center">
					<li><a class="mr-12" href="{{ route('home') }}"><img src="{{ asset('/images/gorr_logo_2022_small.png') }}" alt="Gulf of Riga Regatta logo"></a></li>
					{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('notices.index') }}">{{ __('navigation.notices') }}</a></li> --}}
					<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('news.index') }}">{{ __('navigation.news') }}</a></li>
					<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('galleries.index') }}">{{ __('navigation.gallery') }}</a></li>
					<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('info') }}">{{ __('navigation.info') }}</a></li>
					<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('skippers.registration') }}">{{ __('navigation.registration') }}</a></li>
					{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="https://ej.uz/u6bp" target="_blank">{{ __('navigation.join') }}</a></li> --}}
					{{-- <li>
						<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('skippers.index') }}">
							{{ __('navigation.participants') }}
							<span class="bg-color-primary rounded-full p-1 text-xs">'22</span>
						</a>
					</li> --}}
					{{-- <li>
						<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" target="_blank" href="https://gulfofrigaregatta.eu/regatta/2/documents/GoRR-2019-results.pdf">
							{{ __('navigation.results') }}
							<span class="bg-color-primary rounded-full p-1 text-xs">'19</span>
						</a>
					</li> --}}
					<li>
						<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('results') }}">
							{{ __('navigation.results') }}
							<span class="bg-color-primary rounded-full p-1 text-xs">'22</span>
						</a>
					</li>
					
					{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="http://shorestore.mozello.lv/" target="_blank">{{ __('navigation.merchandise') }}</a></li> --}}
					<li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6" href="{{ route('contacts')}}">{{ __('navigation.contacts') }}</a></li>
					{{-- <li><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('archive.index', ['year' => 2018])}}">{{ __('navigation.archive') }}</a></li> --}}
				</ul>
				<div class="flex items-center">
					{{-- <a href="/ienakt" class="no-underline uppercase font-bold text-sm text-30 tracking-wide mr-6">{{ __('navigation.login') }}</a> --}}
					@include('partials._language_selector')
				</div>
			</div>
		</header>
		<header class="xl:hidden w-9/10 mx-auto py-8">
			<input type="checkbox" id="menu">
			<div class="flex items-center">
				<label for="menu" class="mr-6 select-none">
					<svg width="27px" height="19px" viewBox="0 0 27 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <g transform="translate(-258.000000, -355.000000)" fill="#FFFFFF" fill-rule="nonzero">
				            <g id="menu-dark" transform="translate(258.000000, 355.000000)">
				                <path d="M0,0 L27,0 L27,2.7 L0,2.7 L0,0 Z M0,8.1 L27,8.1 L27,10.8 L0,10.8 L0,8.1 Z M0,16.2 L27,16.2 L27,18.9 L0,18.9 L0,16.2 Z" id="Shape"></path>
				            </g>
				        </g>
				    </g>
					</svg>
				</label>
				{{-- <a href="/ienakt" class="mr-6 ml-auto no-underline uppercase font-bold text-sm text-30 tracking-wide">{{ __('navigation.login') }}</a> --}}
				@include('partials._language_selector')
			</div>
			<ul class="menu-content list-reset text-center mt-12">
				<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('home') }}">{{ __('navigation.home') }}</a></li>
				{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('notices.index') }}">{{ __('navigation.notices') }}</a></li> --}}
				<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('news.index') }}">{{ __('navigation.news') }}</a></li>
				<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('galleries.index') }}">{{ __('navigation.gallery') }}</a></li>
				<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('info') }}">{{ __('navigation.info') }}</a></li>
				<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('skippers.registration') }}">{{ __('navigation.registration') }}</a></li>
				{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="https://ej.uz/u6bp" target="_blank">{{ __('navigation.join') }}</a></li> --}}
				{{-- <li class="py-4">
					<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('skippers.index') }}">
						{{ __('navigation.participants') }}
						<span class="bg-color-primary rounded-full p-1 text-xs">'22</span>
					</a>
				</li> --}}
				{{-- <li class="py-4">
					<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" target="_blank" href="https://gulfofrigaregatta.eu/regatta/2/documents/GoRR-2019-results.pdf">
						{{ __('navigation.results') }}
						<span class="bg-color-primary rounded-full p-1 text-xs">'19</span>
					</a>
				</li> --}}
				<li class="py-4">
					<a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('results') }}">
						{{ __('navigation.results') }}
						<span class="bg-color-primary rounded-full p-1 text-xs">'22</span>
					</a>
				</li>

				{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="http://shorestore.mozello.lv/" target="_blank">{{ __('navigation.merchandise') }}</a></li> --}}
				<li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('contacts')}}">{{ __('navigation.contacts') }}</a></li>
				{{-- <li class="py-4"><a class="no-underline uppercase font-bold text-sm text-30 tracking-wide" href="{{ route('archive.index', ['year' => 2018])}}">{{ __('navigation.archive') }}</a></li> --}}
			</ul>
		</header>
		<div class="w-9/10 mx-auto text-center">
			<div class="my-12 md:flex md:items-center md:justify-center">
				<img class="md:mr-8 mb-8 md:mb-0" src="{{ asset('/images/GoRR_logo_2023.png') }}" alt="Gulf of Riga Regatta logo">
				<img src="{{ asset('/images/map_2023.png') }}" alt="Gulf of Riga Regatta">
			</div>
			<p class="uppercase text-30 mb-2 tracking-wide leading-normal">{{ __('regatta.subtitle1') }}</p>
			<p class="uppercase text-30 mb-8 tracking-wide leading-normal">{{ __('regatta.subtitle2') }}</p>
			<p class="uppercase text-center text-white inline-block py-2 px-8 mb-8 rounded-sm" style="background-color: #4B71F6;">Ventspils - Roja - Kuressaare</p>
			{{-- <p class="text-color-primary mb-10 text-2xl">{{ __('landingPage.date') }}</p> --}}
			{{-- <a href="{{ route('skippers.registration') }}" class="no-underline bg-color-primary text-black font-semibold text-2xl leading-none rounded-sm py-4 px-8 mb-12 inline-block">
				{!! __('landingPage.signUpHere') !!}
			</a> --}}
		</div>
		<div class="bg-no-repeat bg-cover" style="background-image:url('/images/wave.png'); height: 126px;"></div>
	</div>
	{{-- timer --}}
	<div class="bg-30 text-center pb-20">
		<div class="inline-block max-w-sm md:max-w-md px-8 py-12 -mt-6 md:-mt-28 shadow-lg rounded-sm uppercase text-lg" style="background-color: #f2f9fa">
				{{-- <div class="w-32 mx-auto mb-4 md:mb-0">
					<img src="{{ asset('/images/gorr_new_normal_logo.png') }}" alt="Gulf of Riga Regatta 2020">
				</div> --}}
				{{-- <div class="md:text-left md:ml-16">
					<div class="text-color-primary text-3xl md:text-5xl font-bold mb-4 leading-none">{{ __('landingPage.thankYouForParticipating') }}</div>
					<div class="text-color-primary text-2xl md:text-4xl font-light mb-4">{{ __('landingPage.seeYouNextYear') }}</div>
					<div class="text-2xl md:text-4xl font-light" style="color: #0099ff;">#GoRR2021</div>
				</div> --}}
				{{-- <div class="md:px-16 mb-8 flex"> --}}
					<timer date="06/24/2023 09:00:00" v-cloak>{{ __('regatta.countdown') }}</timer>

					{{-- <timer date="06/27/2021 09:00:00" v-cloak></timer> --}}

					{{-- <p class="my-6 md:mt-0">{{ __('landingPage.regattaHasEnded') }}</p>
					<a href="{{ route('results') }}" target="_blank" class="block bg-color-primary uppercase mx-12 md:mx-0 px-12 py-4 no-underline text-black font-bold tracking-wide shadow">{{ __('landingPage.results') }}</a> --}}

					<a href="{{ route('skippers.registration') }}" class="block uppercase md:mx-0 px-12 py-4 no-underline text-white font-bold tracking-wide shadow" style="background-color: #4B71F6;">{{ __('navigation.registration') }}</a>

					{{-- <a href="https://kwindoo.com/tracking/28081-gulf-of-riga-regatta-gorr" target="_blank" class="block bg-color-primary uppercase mx-12 md:mx-0 px-6 py-4 no-underline font-bold tracking-wide shadow flex items-center justify-center" style="color: #021A34;">{{ __('landingPage.followLive') }} <div class="pulse ml-2"></div></a> --}}

				{{-- </div> --}}
		</div>
	</div>
	{{-- end of timer --}}

	{{-- calendar --}}
	@include('partials.index.calendar')
	{{-- end of calendar --}}

	{{-- features --}}
	{{-- @include('partials.index.features') --}}
	{{-- end of features --}}

	{{-- sponsors and organizers --}}
	@include('partials.index.sponsors')
	{{-- end of sponsors and organizers --}}

	{{-- team of organizers --}}
	@include('partials.index.organisers_team')
	{{-- end of team of organizers --}}

	{{-- 2019 info modal --}}
	{{-- <div>
		<info-modal></info-modal>
	</div> --}}
@endsection