@extends('layouts.master')

@section('content')
	<div class="bg-30 py-24">
		<div class="w-9/10 mx-auto">
			<h1 class="text-center uppercase text-lg text-60 font-normal mb-8">{{ __('skipper.registration') }}</h1>
			<div>
				@if (App::isLocale('lv'))
					<div class="iframe-container">
						<iframe src="https://lv.regattas.eu/iframe/lv/regatta_registration/1468"></iframe>
					</div>
				@elseif (App::isLocale('en'))
					<div class="iframe-container">
						<iframe src="https://lv.regattas.eu/iframe/en/regatta_registration/1468"></iframe>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection
