@extends('layouts.master')

@section('content')
    <div class="bg-blue-lightest flex-expand" id="app">
        <div class="bg-white w-9/10 xl:w-4/5 mt-16 mx-auto mb-16 px-6 sm:px-12 py-16 rounded shadow-lg flex">
            
            <form class="flex-2" method="POST" action="{{ route('participants.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <input type="hidden" name="locale" value="{{ LaravelLocalization::getCurrentLocale() }}">
                <h1 class="font-bold text-2xl text-90 tracking-wide">{{ __('crew.new_submission') }}</h1>

                <p class="font-regular text-sm text-60 tracking-semi leading-loose w-full mt-6">{{ __('crew.new_submission_copy') }}</p>

                <div class="mt-4">
                    <p class="text-xs italic text-60 tracking-semi font-medium flex items-start"><span class="text-red text-lg mr-1">*</span><span> - {{ __('form.required_fields') }}</span></p>
                </div>

                <div class="mt-8">
                    <div class="flex items-baseline">
                        <div class="border-2 border-green rounded-full leading-none p-2 inline-block h-8 w-8 flex justify-center items-center mr-2">
                            <span class="text-lg text-green font-bold">1.</span>
                        </div>
                        <h3 class="font-semibold text-lg text-90 tracking-wide ">{{ __('crew.contact_info') }}</h3>
                    </div>

                    <div class="w-full md:w-96 mt-6">
                        <label class="form-label block mb-2" for="name">{{ __('form.name') }}<span class="text-red text-lg ml-1">*</span></label>
                        <input class="form-input" id="name" name="name" placeholder="{{ __('form.placeholders.name') }}" value="{{ old('name') }}" type="text" required autofocus>
                        @if ($errors->has('name'))
                            <p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.name') }}</p>
                        @endif
                    </div>

                    <div class="w-full md:w-96 mt-6">
                        <label class="form-label block mb-2" for="email">{{ __('form.email') }}<span class="text-red text-lg ml-1">*</span></label>
                        <input class="form-input" id="email" name="email" placeholder="{{ __('form.placeholders.email') }}" type="text" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.email') }}</p>
                        @endif
                    </div>

                    <div class="w-full md:w-64 mt-6">
                        <label class="form-label block mb-2" for="phone">{{ __('form.phone') }}<span class="text-red text-lg ml-1">*</span></label>
                        <input class="form-input" id="phone" name="phone" placeholder="{{ __('form.placeholders.phone') }}" type="text" value="{{ old('phone') }}" required>
                        @if ($errors->has('phone'))
                            <p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.phone') }}</p>
                        @endif
                    </div>

                </div>

                <div class="border-b-2 border-20 mt-12"></div>

                <div class="mt-12">
                    <div class="flex items-baseline">
                        <div class="border-2 border-green rounded-full leading-none p-2 inline-block h-8 w-8 flex justify-center items-center mr-2">
                            <span class="text-lg text-green font-bold">2.</span>
                        </div>
                        <h3 class="font-semibold text-lg text-90 tracking-wide ">{{ __('crew.about_you') }}</h3>
                    </div>

                    <div class="w-full md:w-96 mt-8">
                        <label class="form-label block mb-2" for="description">{{ __('form.description') }}<span class="text-red text-lg ml-1">*</span></label>
                        <textarea class="form-input leading-tight" id="description" type="text" placeholder="{{ __('form.placeholders.description') }}" name="description" rows="6">{{ old('description') }}</textarea>
                        @if($errors->has('description'))
                            <p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.description') }}</p>
                        @endif
                    </div>

                    <div class="w-full md:w-96 mt-8">
                        <label class="form-label block mb-2" for="facebook_link">{{ __('form.profile_image') }}</label>
                        <image-select value="" name="image"></image-select>
                    </div>

                </div>

                <div class="mt-12">
                    <button class="btn btn-green">{{ __('form.register') }}</button>
                </div>
            </form>
            
            <div class="flex-1 ml-12 hidden lg:block" style="background-image:url('/images/dark-sea.png');background-size:cover;background-repeat:none;background-position:top center;">
                
                <div class="mt-32">
                    @if (count($quotes) > 0)
                        @foreach($quotes as $quote)
                            <div class="mb-16 mx-8">
                                <p class="font-semibold text-white tracking-xl text-lg italic leading-tight">"{{ $quote->body }}"</p>
                                <p class="mt-3 text-right font-regular text-white text-sm tracking-wide">- {{ $quote->author }}</p>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
            
        </div>
    </div>
@endsection