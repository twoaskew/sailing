@extends('layouts.master')

@section('content')
	<section class="bg-30 pb-24 flex-expand">
		<div class="w-9/10 max-w-md mx-auto">
			<h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('skipper.thanks.title') }}</h2>
			<div class="mb-8 border-t border-50 border-solid flex">
				<p class="text-90 leading-normal mt-10">{{ __('skipper.thanks.copy') }}</p>
			</div>
		</div>
	</section>
@endsection