@extends('layouts.master')

@section('content')
<div class="flex-expand bg-cover" style="background-image:url('/images/sea.png');">
	<div class="bg-white w-9/10 xl:w-4/5 max-w-xl mt-16 py-16 px-12 mx-auto mb-16 rounded shadow-lg md:flex">
		<div class="bg-white flex-1 mr-6">
			<h3 class="font-bold text-lg text-black">{{ __('crew.thanks.title') }}</h3>
			<p class="leading-normal mt-8 mb-6 text-60 text-sm">{{ __('crew.thanks.copy1') }}</p>
			<p class="leading-normal mt-8 mb-6 text-60 text-sm">{{ __('crew.thanks.copy2') }}</p>
		</div>
		<div class="flex-1 bg-cover bg-center min-h-56" style="background-image:url('/images/yacht-1.jpg');"></div>
	</div>
</div>
@endsection