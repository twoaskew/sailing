@extends('layouts.master')

@section('content')
    <div class="bg-blue-lightest flex-expand" id="app">
        <div class="bg-white w-9/10 xl:w-4/5 mt-16 mx-auto mb-16 px-12 py-16 rounded shadow-lg flex">

            @if (session()->has('message') )
                <toast type="success">{{ session('message') }}</toast>
            @endif
            
            <div class="flex-2">
                
                @if ($participant->openInvitations()->count() > 0)
                    <div class="mb-6">
                        @foreach($participant->openInvitations as $invitation)
                        <div class="p-4 px-6 bg-green-lightest rounded mb-4 leading-normal">
                            Jahtas <span class="font-bold text-black capitalize">
                            {!! __('crew.invitation', ['yacht' => $invitation->skipper->yacht->name]) !!}
                            <form class="ml-2 inline-block" action="{{ route('invitations.accept', ['invitation' => $invitation]) }}" method="POST">
                                @csrf
                                <button class=" cursor-pointer text-sm text-green underline capitalize">{{ __('crew.accept') }}</button>
                            </form>
                            <form class="ml-3 inline-block" action="{{ route('invitations.decline', ['invitation' => $invitation]) }}" method="POST">
                                @csrf
                                <button class=" cursor-pointer text-sm text-red underline capitalize">{{ __('crew.decline') }}</button>
                            </form>
                        </div>
                        @endforeach
                    </div>
                @endif

                @if ($participant->isOnBoard())
                    <div class="mb-6">
                        <div class="p-4 px-6 bg-green-light rounded mb-4 leading-normal">
                            {!! __('crew.onBoard', ['yacht' => $participant->yacht->name]) !!}
                        </div>
                    </div>
                @endif

                
                <form method="POST" action="{{ route('participants.update', ['participant' => $participant]) }}">

                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <h1 class="font-bold text-2xl text-black tracking-wide">{{ __('crew.submission') }}</h1>

                    <p class="font-medium text-xs text-60 tracking-semi leading-loose w-full mt-6">{{ __('crew.submission_copy') }}</p>

                    <div class="mt-4">
                        <p class="text-xs italic text-60 tracking-semi font-medium flex items-start"><span class="text-red text-lg mr-1">*</span><span> - {{ __('form.required_fields') }}</span></p>
                    </div>

                    <div class="mt-8">
                        <div class="flex items-baseline">
                            <div class="border-2 border-green rounded-full leading-none p-2 inline-block h-8 w-8 flex justify-center items-center mr-2">
                                <span class="text-lg text-green font-bold">1.</span>
                            </div>
                            <h3 class="font-semibold text-lg text-black tracking-wide ">{{ __('crew.contact_info') }}</h3>
                        </div>

                        <div class="w-full md:w-96 mt-6">
                            <label class="form-label block mb-2" for="name">{{ __('form.name') }}<span class="text-red text-lg ml-1">*</span></label>
                            <input class="form-input" id="name" name="name" placeholder="{{ __('form.placeholders.name') }}" type="text" value="{{ $participant->name }}">
                            @if ($errors->has('name'))
                                <p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.name') }}</p>
                            @endif
                        </div>

                        <div class="w-full md:w-96 mt-6">
                            <label class="form-label block mb-2" for="email">{{ __('form.email') }}<span class="text-red text-lg ml-1">*</span></label>
                            <input class="hidden" name="email" placeholder="{{ __('form.placeholders.email') }}" type="hidden" value="{{ $participant->email }}">
                            <span class="block text-50 font-medium text-sm tracking-wide py-3 px-4">{{ $participant->email }}</span>
                        </div>

                        <div class="w-full md:w-64 mt-6">
                            <label class="form-label block mb-2" for="phone">{{ __('form.phone') }}<span class="text-red text-lg ml-1">*</span></label>
                            <input class="form-input" id="phone" name="phone" placeholder="{{ __('form.placeholders.phone') }}" type="text" value="{{ $participant->phone }}">
                            @if ($errors->has('phone'))
                                <p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.phone') }}</p>
                            @endif
                        </div>

                    </div>

                    <div class="border-b-2 border-20 mt-12"></div>

                    <div class="mt-12">
                        <div class="flex items-baseline">
                            <div class="border-2 border-green rounded-full leading-none p-2 inline-block h-8 w-8 flex justify-center items-center mr-2">
                                <span class="text-lg text-green font-bold">2.</span>
                            </div>
                            <h3 class="font-semibold text-lg text-black tracking-wide ">{{ __('crew.about') }}</h3>
                        </div>

                        <div class="w-full md:w-96 mt-8">
                            <label class="form-label block mb-2" for="description">{{ __('form.description') }}<span class="text-red text-lg ml-1">*</span></label>
                            <textarea class="form-input" id="description" type="text" placeholder="{{ __('form.placeholders.description') }}" name="description" rows="6">{{ $participant->description }}</textarea>
                            @if($errors->has('description'))
                                <p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.description') }}</p>
                            @endif
                        </div>

                        <div class="w-full md:w-96 mt-8">
                            <label class="form-label block mb-2" for="facebook_link">{{ __('form.profile_image') }}</label>
                            <image-select value="{{ $participant->thumbnail() }}" name="image"></image-select>
                        </div>

                    </div>
                    <input type="submit" class="hidden" id="update-form">
                </form>

                <div class="mt-12">
                    <label for="update-form" class="btn btn-green mr-3">{{ __('form.save') }}</label>
                    @if (!$participant->isOnBoard())
                    <form class="inline-block" action="{{ route('participants.destroy', ['participant' => $participant]) }}" onsubmit="return confirm('Vai tiešām vēlies dzēst savu pieteikumu?');" method="POST">
                        @csrf
                        {{ method_field('DELETE') }}
                        <button class="btn btn-red" type="submit">{{ __('crew.delete_submission') }}</button>
                    </form>
                    @endif
                </div>
            </div>
            
            <div class="flex-1 ml-12 hidden lg:block" style="background-image:url('/images/dark-sea.png');background-size:cover;background-repeat:none;background-position:top center;">
                
                <div class="mt-32 mx-8">
                    <p class="font-semibold text-white tracking-xl text-lg italic leading-tight">"This is the main sailing event in the Baltic Sea!"</p>
                    <p class="mt-3 text-right font-regular text-white text-sm tracking-wide">- Bjorn Johansson</p>
                </div>

                <div class="mt-16 mx-8">
                    <p class="font-semibold text-white tracking-xl text-lg italic leading-tight">"Latvijas atklātais čempionāts burāšanā sniedz lieliskas emocijas."</p>
                    <p class="mt-3 text-right font-regular text-white text-sm tracking-wide">- Andrejs Drīksna</p>
                </div>

                <div class="mt-16 mx-8">
                    <p class="font-semibold text-white tracking-xl text-lg italic leading-tight">"Piecas azarta un piedzīvojumu pilnas dienas lieliskā kompānijā!"</p>
                    <p class="mt-3 text-right font-regular text-white text-sm tracking-wide">- Leons Bortturis</p>
                </div>
            </div>
            
        </div>
    </div>
@endsection