@extends('layouts.master')

@section('head-js')
@endsection

@section('content')
  <section class="bg-30 pb-24">
    <h3 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('gallery.title') }}</h3>
    <div class="w-9/10 max-w-xl mx-auto">
      <div class="border-t border-50 border-solid"></div>
    </div>
    <div class="flex w-9/10 max-w-xl mx-auto mt-8 gallery-grid">
      @foreach($galleries as $gallery)
        <a href="{{ route('galleries.show', ['gallery' => $gallery]) }}" class="no-underline grid-item mx-auto">

          @if($gallery->images()->first->name)
            <img src="{{ $gallery->images()->first()->getUrl('thumb')}}" alt="" class="block shadow">
          @else
            <img src="{{ asset('/images/video-thumbnail.png') }}" alt="" class="block shadow">
            {{-- <img src="https://img.youtube.com/vi/CjadmjleQT8/maxresdefault.jpg" alt="" class="block shadow"> --}}
          @endif

          <h4 class="mt-4 text-90 font-normal">{{ $gallery->title }}</h4>
        </a>
      @endforeach
    </div>
  </section>
@endsection

{{-- @section('content') --}}
{{-- <section class="bg-30 pb-24">
  <h3 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('gallery.title') }}</h3>
  <div class="w-9/10 max-w-xl mx-auto">
    <div class="border-t border-50 border-solid"></div>
    @foreach($galleries as $gallery)
      <a href="{{ route('galleries.show', ['gallery' => $gallery]) }}" class="no-underline">
        <h2 class="text-base font-normal text-90 mb-4 ml-2 mt-8 text-center sm:text-left">{{ $gallery->title }}</h2>
        <div class="sm:flex sm:justify-between mb-6">
          @foreach($gallery->images()->take(3) as $image)
            <div class="flex-1 mx-auto sm:mx-2 mb-2 cursor-pointer shadow-md max-w-xs sm:max-w-full">
              <img src="{{ $image->getUrl('thumb') }}" alt="" class="block">
            </div>
          @endforeach
        </div>
        <div class="text-right">
          <a href="{{ route('galleries.show', ['gallery' => $gallery ])}} }}" class="text-color-primary no-underline">{{ __('gallery.viewFullGalleryLink') }}</a>
        </div>
      </a>
    @endforeach
  </div>
</section> --}}
{{-- @endsection --}}