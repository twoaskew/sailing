@extends('layouts.master')

@section('head-js')
	<script src="{{ mix('js/gallery.js') }}"></script>
@endsection

@section('content') 
	<h1>Pievienot bildes galerijai {{ $gallery->title }}</h1>
	<div>
		@foreach($images as $image)
			<img src="{{ $image->getUrl('thumb') }}" alt="" style="height:200px;width:auto;" class="mr-2 border">
		@endforeach
	</div>
	<form id="image-form" method="POST" action="{{ route('gallery.images.store', ['gallery' => $gallery]) }}"  class="dropzone">
		@csrf
	</form>
@endsection