@extends('layouts.master')

@section('head-js')
  <script>
    window.images = {!! json_encode($images) !!}
    window.videoId = "{{ $gallery->video_id }}"
  </script>
@endsection

@section('content')
<section class="bg-30 pb-24 flex-expand" id="app">
	<div class="w-9/10 max-w-xl mx-auto">
		<h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ $gallery->title }}</h2>
		<a href="{{ route('archive.index', $year) }}" class="font-normal text-90 no-underline inline-block">&#8592; {{ __('archive.archive') }}</a>
		<image-gallery></image-gallery>
	</div>
</section>
@endsection