@extends('layouts.master')

@section('content')
<div class="flex-expand bg-cover" style="background-image:url('/images/sea.png');" id="app">
    <div class="bg-white w-4/5 max-w-xl mt-16 mx-auto mb-16 px-6 sm:px-12 py-16 rounded shadow-lg">
        <h1 class="font-bold text-2xl text-90 mb-6">{{ __('gallery.day1') }}</h1>
        <a href="/" class="font-semibold text-green mb-6 inline-block">{{ __('gallery.allGalleriesLink') }}</a>
        <div class="sm:flex md:justify-between mb-6">
            <div class="flex-1 bg-20 rounded p-2 m-2 cursor-pointer">
                <img src="https://img.youtube.com/vi/MoTFKyd9pT0/hqdefault.jpg" alt="">
            </div>
            <div class="flex-1 bg-20 rounded p-2 m-2 cursor-pointer">
                <img src="https://img.youtube.com/vi/TEat9bfStbY/hqdefault.jpg" alt="">
            </div>
            <div class="flex-1 bg-20 rounded p-2 m-2 cursor-pointer">
                <img src="https://img.youtube.com/vi/MoTFKyd9pT0/hqdefault.jpg" alt="">
            </div>
        </div>
    </div>
</div>
@endsection