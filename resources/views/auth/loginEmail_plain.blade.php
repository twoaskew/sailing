Sveiki!
<br><br>

Lai piekļūtu savai informācijai Rīgas Līča regates mājaslapā izmanto saiti <a href="{{ $url }}">{{ $url }}</a>.
<br><br>

Šī saite būs aktīva tikai 15 minūtes un to var izmantot tikai vienu reizi.