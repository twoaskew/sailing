Tu saņem šo e-pastu, jo vēlējies piekļūt savas reģistrācijas informācijai

@component('mail::button', ['url' => $url])
Ienākt
@endcomponent