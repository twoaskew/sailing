Click the link below to sign in to your Gulf of Riga Regatta profile.<br><br>

<a href="{{ $url }}">{{ $url }}</a>
<br><br>

This link will expire in 15 minutes and it can be used only once.