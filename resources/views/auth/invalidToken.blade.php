@extends('layouts.master')

@section('content')
	<section class="bg-30 pb-24 flex-expand">
		<div class="w-9/10 max-w-md mx-auto">
			<h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('login.title') }}</h2>
			<div class="mb-8 border-t border-50 border-solid">
				<p class="text-90 leading-normal mt-10">{{ __('login.linkHasExpired') }}</p>
				<div class="mt-6">
          <a href="{{ route('login') }}" class="text-color-primary no-underline font-regular text-sm">{{ __('login.loginAgain') }}</a>
        </div>
			</div>
		</div>
	</section>
@endsection