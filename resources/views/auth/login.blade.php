{{-- @extends('layouts.master')

@section('content')
<div class="flex-expand bg-cover" style="background-image:url('/images/sea.png');">
      <div class="bg-white w-9/10 xl:w-4/5 max-w-xl mt-16 py-16 px-12 mx-auto mb-16 rounded shadow-lg md:flex">
        <form class="bg-white flex-1 mr-6" method="POST" action="/login">
          @csrf    
          <h1 class="font-bold text-2xl text-90">{{ __('login.title') }}</h1>
          <p class="leading-normal mt-8 mb-6 text-60 text-sm">{{ __('login.copy') }}</p>
          <div class="w-full md:w-96 mt-6">
            <label class="form-label block mb-2" for="email">{{ __('login.label') }}</label>
            <input class="form-input" id="email" name="email" placeholder="{{ __('login.placeholder') }}" type="text" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
              <p class="text-xs italic text-red font-regular mt-1">{{ __('login.error') }}</p>
            @endif
          </div>
          <div class="mt-6">
            <button class="btn btn-green">{{ __('login.submit') }}</button>
          </div>
        </form>

        <div class="flex-1 bg-cover bg-center min-h-56 hidden md:block" style="background-image:url('/images/contacts-sail.jpg');">
        </div>
      </div>
  </div>
@endsection --}}

@extends('layouts.master')

@section('content')
<section class="bg-30 pb-24 flex-expand">
    <div class="w-9/10 max-w-sm mx-auto">
      <h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('login.title') }}</h2>
      <div class="mb-8 border-t border-50 border-solid">
        <p class="text-90 leading-normal mb-8 mt-10">{{ __('login.copy') }}</p>
        <form class="" method="POST" action="/login">
          @csrf
          <label class="form-label block mb-2" for="email">{{ __('login.label') }}</label>
          <input class="form-input" id="email" name="email" placeholder="{{ __('login.placeholder') }}" type="text" value="{{ old('email') }}" required autofocus>
          @if ($errors->has('email'))
            <p class="text-xs italic text-red font-regular mt-1">{{ __('login.error') }}</p>
          @endif
          <div class="mt-6 text-center">
            <button class="bg-yellow-main inline-block uppercase px-12 py-4 no-underline text-black font-bold tracking-wide shadow">{{ __('login.submit') }}</button>
          </div>
        </form>
      </div>
    </div>
</section>
@endsection

