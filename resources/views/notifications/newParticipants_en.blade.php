Hi!
<br><br>

New participants looking for crew have signed up for Gulf of Riga Regatta.
<br><br>

<a href="https://gulfofrigaregatta.eu/en/login">Sign in to your account</a> and visit Crew Looking for Boats section to view the applications and add members to your team.