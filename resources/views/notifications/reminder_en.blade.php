Hi!
<br><br>

Gulf of Riga Regatta starts next Tuesday. There are still participants looking to join a crew. If you would like to add someone to your team, <a href="https://gulfofrigaregatta.eu/en/login">login to your account</a> and visit "Crew Looking for Boats" section to view the applications and add members to the team.
