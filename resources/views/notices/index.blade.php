@extends('layouts.master')

@section('content')

<section class="bg-30 pb-24 flex-expand">
	<div class="w-9/10 max-w-md mx-auto">
		<h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('notices.title') }}</h2>
		{{-- {{ count($notices) }} --}}
		@if (count($notices))
			@foreach($notices as $notice)
			<div class="mb-8 border-t border-50 border-solid flex flex-col md:flex-row">
				<div class="border-primary border-2 flex justify-center items-center px-3 py-2 mr-8 self-start text-color-primary mt-8">{{ $notice->created_at->format('d.m') }}</div>
				<div>
					<a href="{{ route('notices.show', ['slug' => $notice->slug]) }}" class="no-underline">
						<h3 class="text-2xl text-100 mb-2 mt-4 md:mt-8">{{ $notice->title }}</h3>
					</a>
					<div class="text-90 leading-normal">{!! $notice->body !!}</div>
				</div>
			</div>
			@endforeach
		@else
			<p class="text-md text-70 text-center">{{ __('notices.empty') }}</p>
		@endif
	</div>
</section>

@endsection