@extends('layouts.master')

@section('content') 
	<div class="bg-30 py-24 flex-expand" id="app">
		<div class="w-9/10 mx-auto max-w-lg">
			@if (session()->has('message') )
				<toast type="success">{{ session('message') }}</toast>
			@endif

			<form class="flex-2" method="POST" action="{{ route('skippers.update', ['skipper' => $skipper ]) }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<h1 class="text-center uppercase text-lg text-60 font-normal mb-6">{{ __('skipper.my_profile') }}</h1>

				<p class="text-sm text-70 flex items-start mb-2 leading-normal">{{ __('skipper.profile_copy') }}</p>
				<p class="text-sm text-70 flex items-start mb-2"><span class="text-color-primary text-lg mr-1">*</span><span> - {{ __('form.required_fields') }}</span></p>
				
				{{-- skipper main section --}}
				<div class="border-solid border-50 border-t md:flex justify-between">
						<div class="mt-4 mr-6 flex-1">
							<h3 class="font-semibold text-lg text-90 tracking-wide mb-2">{{ __('skipper.skipper') }}</h3>
							<p class="text-sm text-70 max-w-xs leading-normal">{{ __('skipper.description') }}</p>
						</div>

						<div class="mt-4 flex-1">
							<div class="max-w-xs md:max-w-full">
								<label class="form-label block mb-2" for="name">{{ __('form.name') }}<span class="text-color-primary text-lg ml-1">*</span></label>
								<input class="form-input" id="name" type="text" placeholder="Valdis Burātājs" name="name" value="{{ $skipper->name }}">
								@if($errors->has('name'))
									<p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.name') }}</p>
								@endif
							</div>

						<div class="mt-6 max-w-xs md:max-w-full">
							<label class="form-label block mb-2">{{ __('form.email') }}<span class="text-color-primary text-lg ml-1">*</span></label>
							<input class="hidden" name="email" placeholder="valdis@example.org" type="hidden" value="{{ $skipper->email }}">
							<span class="block text-70 font-medium text-sm tracking-wide py-3 px-4">{{ $skipper->email }}</span>
						</div>

						<div class="mt-6 max-w-xs md:max-w-full">
							<label class="form-label block mb-2" for="phone">{{ __('form.phone') }}<span class="text-color-primary text-lg ml-1">*</span></label>
							<input class="form-input" id="phone" name="phone" placeholder="+371 2000000" type="text" value="{{ $skipper->phone }}">
							@if($errors->has('phone'))
								<p class="text-xs italic text-red font-regular mt-1">{{ __('form.erros.phone') }}</p>
							@endif
						</div>
					</div>

				</div>
				{{-- skipper main section ends --}}

				{{-- yacht main section --}}
				<div class="border-solid border-50 border-t md:flex justify-between mt-12">
					<div class="mt-4 flex-1">
						<h3 class="font-semibold text-lg text-black tracking-wide mb-2">{{ __('skipper.yacht') }}</h3>
						<p class="text-sm text-70 max-w-xs leading-normal">{{ __('skipper.yacht-description') }}</p>
					</div>

					<div class="mt-4 flex-1">
						<div class="sm:flex">
							{{-- yacht name --}}
							<div class="sm:flex-2 sm:mr-6 max-w-xs md:max-w-full">
								<label class="form-label block mb-2" for="yacht_name">{{ __('form.yacht.name') }}<span class="text-color-primary text-lg ml-1">*</span></label>
								<input class="form-input" id="yacht_name" type="text" placeholder="Selga" name="yacht_name" value="{{ $skipper->yacht->name }}">
								@if($errors->has('yacht_name'))
									<p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.yacht.name') }}</p>
								@endif
							</div>
						
							{{-- sail number --}}
							<div class="sm:flex-1 min-w-64 mt-6 sm:mt-0 max-w-40 md:max-w-full">
								<label class="form-label block mb-2" for="sail_number">{{ __('form.yacht.sail_number') }}<span class="text-color-primary text-lg ml-1">*</span></label>
								<input class="form-input" id="sail_number" name="sail_number" placeholder="LAT352" type="text" value="{{ $skipper->yacht->sail_number }}">
								@if($errors->has('sail_number'))
									<p class="text-xs italic text-red font-regular mt-1">{{ __('form.erros.yacht.sail_number') }}</p>
								@endif
							</div>
						</div>
					
					{{-- club --}}
					<div class="max-w-xs md:max-w-full mt-6">
						<label class="form-label block mb-2" for="yacht_club">{{ __('form.yacht.yacht_club') }}</label>
						<input class="form-input" id="yacht_club" name="yacht_club" placeholder="Karamele" type="text" value="{{ $skipper->yacht->yacht_club }}">
					</div>
					
					{{-- group --}}
					<div class="mt-8">
						<p class="form-label mb-2 pb-0">{{ __('form.yacht.group') }}<span class="text-color-primary text-lg ml-1">*</span></p>

						<div class="sm:flex items-baseline">
							<div class="mb-3 sm:mr-8 sm:mb-0">
								<input type="radio" name="group" id="group-lys" value="lys" @if( $skipper->yacht->group === 'lys' )checked @endif>
								<label class="text-sm text-black font-medium" for="group-lys">LYS</label>					
							</div>
							<div class="mb-3 sm:mr-8 sm:mb-0">
								<input type="radio" name="group" id="group-orc" value="orc" @if( $skipper->yacht->group === 'orc' )checked @endif>
								<label class="text-sm text-black font-medium" for="group-orc">ORC</label>					
							</div>
							<div>
								<input type="radio" name="group" id="group-texel" value="texel" @if( $skipper->yacht->group === 'texel' )checked @endif>
								<label class="text-sm text-black font-medium" for="group-texel">Texel</label>					
							</div>
						</div>
						@if($errors->has('group'))
							<p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.yacht.group') }}</p>
						@endif
					</div>

					<div class="mt-8 max-w-xs md:max-w-full">
						<label class="form-label block mb-2" for="loa">LOA, <span class="lowercase">(m)</span><span class="text-color-primary text-lg ml-1">*</span></label>
						<input class="form-input" type="text" name="loa" id="loa" placeholder="12m" value="{{ $skipper->yacht->loa }}">
						@if($errors->has('loa'))
							<p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.loa') }}</p>
						@endif
					</div>

					<div class="sm:flex mt-6">
						<div class="sm:flex-2 sm:mr-6 max-w-xs md:max-w-full">
							<label class="form-label block mb-2" for="make_model">{{ __('form.yacht.make_model') }}</label>
							<input class="form-input" type="text" name="make_model" id="make_model" placeholder="Benetti Yachts, X-41" value="{{ $skipper->yacht->make_model }}">
							@if($errors->has('make_model'))
								<p class="text-xs italic text-red font-regular mt-1">{{ __('form.erros.make_model') }}</p>
							@endif
						</div>

						<div class="sm:flex-1 min-w-64 mt-6 sm:mt-0 max-w-40 md:max-w-full">
							<label for="year" class="form-label block mb-2">{{ __('form.yacht.year') }}</label>
							<input class="form-input" type="text" name="year" id="year" placeholder="2007" value="{{ $skipper->yacht->year }}">
							@if($errors->has('year'))
								<p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.yacht.year') }}</p>
							@endif
						</div>
					</div>

					<div class="mt-8 w-full">
						<label for="image" class="form-label block mb-2">{{ __('form.yacht.image') }}</label>
						{{-- <input class="text-sm text-50 font-medium" type="file" accept="image/*"> --}}
						<image-select value="{{ $skipper->yacht->thumbnail() }}" name="image"></image-select>
					</div>

					</div>
				</div>

				<div class="border-solid border-50 border-t md:flex mt-12">
					<div class="mt-4 flex-1">
						<h3 class="font-semibold text-lg text-black tracking-wide mb-2">{{ __('skipper.team') }}</h3>
						<p class="text-sm text-70 max-w-xs leading-normal">{{ __('skipper.team-description') }}</p>
					</div>

					<div class="mt-4 flex-1">
						<label class="form-label block mb-2" for="team_size">{{ __('form.team_size') }}<span class="text-color-primary text-lg ml-1">*</span></label>
						<div class="relative w-24">
							<select class="block appearance-none focusring-none w-full bg-white border border-gray text-black text-sm py-3 px-4 pr-8 rounded" id="team_size" name="team_size">
								<option @if( $skipper->yacht->team_size == 1) selected @endif>1</option>
								<option @if( $skipper->yacht->team_size == 2) selected @endif>2</option>
								<option @if( $skipper->yacht->team_size == 3) selected @endif>3</option>
								<option @if( $skipper->yacht->team_size == 4) selected @endif>4</option>
								<option @if( $skipper->yacht->team_size == 5) selected @endif>5</option>
								<option @if( $skipper->yacht->team_size == 6) selected @endif>6</option>
								<option @if( $skipper->yacht->team_size == 7) selected @endif>7</option>
								<option @if( $skipper->yacht->team_size == 8) selected @endif>8</option>
								<option @if( $skipper->yacht->team_size == 9) selected @endif>9</option>
								<option @if( $skipper->yacht->team_size == 10) selected @endif>10</option>
								<option @if( $skipper->yacht->team_size == 11) selected @endif>11</option>
								<option @if( $skipper->yacht->team_size == 12) selected @endif>12</option>
							</select>
							<div class="pointer-events-none absolute pin-y pin-r flex items-center px-2 text-100">
								<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
							</div>
						</div>
						@if($errors->has('team_size'))
							<p class="text-xs italic text-red font-regular mt-1">{{ __('form.errors.team_size') }}</p>
						@endif
					</div>
					
					@if ($skipper->yacht->participants()->count() > 0)
					<div class="mt-8">
						<label class="form-label block mb-3">{{ __('form.extra_crew') }}</label>
						@foreach($skipper->yacht->participants as $participant)
						<div class="flex items-center mb-3">
							<img src="{{ $participant->thumbnail() }}" alt="" class="rounded w-12 h-auto shadow-sm mr-3">
							<span>{{ $participant->name }}</span>
						</div>
						@endforeach
					</div>
					@endif

					{{-- <div class="mt-8">
						<input type="hidden" name="can_accept_people" value="0">
						<input id="can_accept_people" name="can_accept_people" type="checkbox" value="1" @if($skipper->yacht->can_accept_people) checked @endif>
						<label for="can_accept_people" class="tracking-wide">{{ __('form.can_accept_people') }}</label>
					</div> --}}

				</div>

				<div class="mt-24 text-center">
					<button class="bg-yellow-main self-center uppercase px-12 py-4 no-underline text-black font-bold tracking-wide shadow">{{ __('form.save') }}</button>
				</div>
			</form>

		</div>
	</div>
@endsection