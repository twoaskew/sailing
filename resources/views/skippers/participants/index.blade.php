@extends('layouts.master')

@section('content') 
<div class="bg-blue-lightest flex-expand" id="app">
	<p>TEST</p>
	<div class="bg-white w-9/10 xl:w-4/5 mt-16 mx-auto mb-16 px-12 py-16 rounded shadow-lg">

		@if (session()->has('message') )
			<toast type="success">{{ session('message') }}</toast>
		@endif

		@if (session()->has('error') )
			<toast type="error">{{ session('error') }}</toast>
		@endif

		<p class="mb-6 text-lg font-semibold tracking-wide">{{ __('skipper.extra_crew') }}</p>

		@if(count($participants) > 0)
		<p class="mb-4 text-md text-70 leading-tight">{{ __('skipper.extra_crew_copy') }}</p>
		<div class="mt-8">
			@foreach($participants as $participant)
				<div class="mb-12 flex items-start @if ($participant->invitations && $participant->invitations->count() > 0) p-3 bg-green-lightest rounded @endif">
					<img src="{{ $participant->thumbnail() }}" alt="" class="shadow mr-8 rounded h-auto max-w-20">
					<div>
						<h3 class="font-semibold text-black text-md tracking-semi">{{ $participant->name }}</h3>
						<a class="text-70 text-sm font-regular" href="mailto:{{ $participant->email }}">{{ $participant->email }}</a>
						<p class="mt-1 text-70 text-sm font-regular">{{ $participant->phone }}</p>
						<p class="mt-4 italic">{{ $participant->description }}</p>
						@if ($participant->invitations && $participant->invitations->count() > 0)
							<form method="POST" class="mt-4" action="{{ route('invitations.destroy', ['invitation' => $participant->invitations->first()]) }}">
								@csrf
								{{ method_field('DELETE') }}
								<span class="mt-6 text-sm text-green-dark font-semibold">{{ __('skipper.invited') }}</span>
								<button class="underline inline-block text-sm text-red font-regular ml-2 cursor-pointer">{{ __('skipper.revoke_invitation') }}</button>
							</form>
						@else
							<form method="POST" action="{{ route('participant.invitations', ['participant' => $participant]) }}">
								@csrf
								<button class="btn btn-green-st inline-block mt-4 no-underline">{{ __('skipper.invite') }}</button>
							</form>
						@endif
					</div>
				</div>
			@endforeach
		</div>

		@else
			{{ __('skipper.no_extra_crew') }}
		@endif

	</div>
</div>
@endsection