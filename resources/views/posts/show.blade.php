@extends('layouts.master_for_posts_show')

@section('content')
	<section class="bg-30 pb-24 flex-expand">
		<div class="w-9/10 max-w-md mx-auto pt-12">
			<a href="{{ route('news.index') }}" class="font-normal text-90 no-underline inline-block mb-4">&#8592; {{ __('news.back') }}</a>
			<div class="mb-8 pt-12 border-t border-50 border-solid flex flex-col md:flex-row">
				<div class="border-primary border-2 flex justify-center items-center px-3 py-2 mr-8 self-start text-color-primary mt-8">{{ $post->created_at->format('d.m') }}</div>
				<div>
					<h3 class="text-2xl text-100 mb-4 mt-4 md:mt-8">{{ $post->title }}</h3>
					<div class="text-90 leading-normal">{!! nl2br($post->body) !!}</div>
				</div>
		</div>
		</div>
	</section>
@endsection