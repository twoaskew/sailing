@extends('layouts.master')

@section('content')
<section class="bg-30 pb-24 flex-expand">
	<div class="w-9/10 max-w-md mx-auto">
			<h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('news.title') }}</h2>
		@foreach($posts as $post)
		<div class="mb-8 border-t border-50 border-solid flex flex-col md:flex-row">
			<div class="border-primary border-2 flex justify-center items-center px-3 py-2 mr-8 self-start text-color-primary mt-8">{{ $post->created_at->format('d.m') }}</div>
			<div>
				<a href="{{ route('news.show', ['slug' => $post->slug]) }}" class="no-underline">
					<h3 class="text-2xl text-100 mb-2 mt-4 md:mt-8">{{ $post->title }}</h3>
				</a>
				{{-- <p class="text-90 leading-normal">{!! str_limit(nl2br($post->body), 300) !!}</p> --}}
				<div class="text-90 leading-normal">{!! $post->excerpt() !!}</div>
				<span class="flex">
					<a href="{{ route('news.show', ['slug' => $post->slug]) }}" class="text-right text-color-primary mt-6 no-underline inline-block ml-auto">{{ __('news.read_more') }}</a>
				</span>
			</div>
		</div>
	@endforeach
	</div>
</section>
@endsection