@extends('layouts.master')

@section('content')
  <section class="bg-30 pb-24">
    <div class="w-9/10 max-w-md mx-auto">
      <h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('information.information') }}</h2>
      {{-- <div class="mb-8 border-t border-50 border-solid">
        <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('information.about') }}</h3>
        <p class="text-90 leading-normal">{{ __('information.description') }}</p>
      </div> --}}

      {{-- <div class="mb-8 border-t border-50 border-solid">
        <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('information.entryFee') }}</h3>
        <p class="text-90 leading-normal mb-6">{{ __('information.feeUnder30feet') }}</p>
        <p class="text-90 leading-normal mb-6">{{ __('information.feeOver30feet') }}</p>
        <p class="text-90 leading-normal mb-6">{{ __('information.feeForOneStage') }}</p>
        <p>
          <span class="text-100 font-bold leading-normal">{{ __('information.paymentInfoTitle') }}</span><br>
          <span class="text-90 leading-normal">{!! __('information.paymentInfo') !!}</span>
        </p>
      </div> --}}
      
      {{-- <div class="mb-8 border-t border-50 border-solid"> --}}
        {{-- <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('information.documents') }}</h3> --}}
        {{-- @foreach($documents as $document)
          <p class="text-90 leading-normal"><img src="{{ asset('/images/clip.png') }}" class="mr-2">{{ $document->name }}</p>
        @endforeach --}}
        @foreach($documents as $document)
          <p class="text-90 leading-normal mb-3">
            <img src="{{ asset('/images/clip.png') }}" class="mr-2">
            <a href="{{ route('document', ['regatta' => 2, 'document' => $document]) }}" target="_blank" class="no-underline text-90 hover:text-color-primary">{{ $document->name }}</a>{{ $document->extension }}
          </p>
        @endforeach
      {{-- </div> --}}
      <div class="mb-8 border-t border-50 border-solid">
        {{-- <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('information.harborsTitle') }}</h3>
        <ul class="list-reset">
          <li>
            <a href={{ Storage::disk('public')->url('pilsetas-jahtklubs.jpeg') }} target="_blank" rel="noopener" class="no-underline text-90 hover:text-color-primary">
              {{ __('information.rigaCityYachtClub') }}
            </a>
          </li>
          <li class="mt-3">
            <a href={{ Storage::disk('public')->url('ruhnu.pdf') }} target="_blank" rel="noopener" class="no-underline text-90 hover:text-color-primary">
              {{ __('information.ruhnu') }}
            </a>
          </li>
          <li class="mt-3">
            <a href={{ Storage::disk('public')->url('salacgriva-1.jpeg') }} target="_blank" rel="noopener" class="no-underline text-90 hover:text-color-primary">
              {{ __('information.salacgriva') }} - 1
            </a>
          </li>
          <li class="mt-3">
            <a href={{ Storage::disk('public')->url('salacgriva-2.jpeg') }} target="_blank" rel="noopener" class="no-underline text-90 hover:text-color-primary">
              {{ __('information.salacgriva') }} - 2
            </a>
          </li>
          <li class="mt-3">
            <a href={{ Storage::disk('public')->url('parnu.pdf') }} target="_blank" rel="noopener" class="no-underline text-90 hover:text-color-primary">
              {{ __('information.parnu') }}
            </a>
          </li>
        </ul> --}}
        <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('information.aboutOrcTitle') }}</h3>
        <p class="text-90 leading-normal mb-6">{!! __('information.aboutOrcText') !!}</p>
        <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('information.lysTitle') }}</h3>
        <p class="text-90 leading-normal mb-6">{!! __('information.lysWhy') !!}</p>
      </div>
    </div>
  </section>
@endsection