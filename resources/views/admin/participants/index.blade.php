<table>
	<thead>
		<tr>
			<td>Vārds, uzvārds</td>
			<td>E-pasts</td>
			<td>Telefons</td>
		</tr>
	</thead>
	@foreach($participants as $participant)
		<tr>
			<td>{{ $participant->name }}</td>
			<td>{{ $participant->email }}</td>
			<td>{{ $participant->phone }}</td>
		</tr>
	@endforeach
</table>