<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Yacht</th>
		</tr>
	</thead>
	@foreach($skippers as $skipper)
		<tr>
			<td>{{ $skipper->name }}</td>
			<td>{{ $skipper->email }}</td>
			<td>{{ $skipper->phone }}</td>
			<td>{{ $skipper->yacht->name }}</td>
		</tr>
	@endforeach
</table>