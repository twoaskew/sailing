@extends('layouts.master')

@section('content')
  <div class="bg-cover flex-expand" style="background-image:url('/images/sea.png');">
    <div class="bg-white w-9/10 xl:w-4/5 max-w-2xl mt-16 mx-auto mb-16 px-12 py-16 rounded shadow-lg">
      <h1 class="font-bold text-2xl text-90 mb-6">Galerijas</h1>
      <p class="leading-normal text-sm text-60 max-w-lg">Šajā sadaļā var izveidot jaunas galerijas vai rediģēt esošās</p>
      
      <a href="{{ route('galleries.new') }}" class="text-green font-semibold">Izveidot jaunu galeriju</a>

      <div class="mt-12">
        @foreach($galleries as $gallery)
         <div class="mb-12">
            <h2 class="text-lg text-90 font-semibold mb-6">{{ $gallery->title }}</h2>
            <div class="sm:flex md:justify-between mb-6">
                @foreach($gallery->images()->take(3) as $image)
                <div class="flex-1 bg-20 rounded p-2 m-2 cursor-pointer">
                    <img src="{{ $image->getUrl('thumb') }}" alt="">
                </div>
                @endforeach
            </div>
            <div>
            <a href="{{ route('admin.galleries.edit', ['gallery' => $gallery]) }}" class="text-green font-semibold">Rediģēt galeriju</a>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection