@extends('layouts.master')

@section('content')
<div class="bg-cover flex-expand" style="background-image:url('/images/sea.png');" id="app">
    <div class="bg-white w-9/10 xl:w-4/5 max-w-2xl mt-16 mx-auto mb-16 px-12 py-16 rounded shadow-lg">

		<div class="mb-6">
			<a href="{{ route('admin.galleries.index') }}" class="text-green font-semibold">Atgriezties uz galerijām</a>
		</div>

		<h1 class="font-bold text-2xl text-90 mb-6">Rediģēt galeriju</h1>

		<admin-gallery :initial-images="{{ json_encode($images) }}" :gallery="{{ $gallery->id }}"></admin-gallery>

		{{-- dropzone ar jaunu bilžu ielādi --}}

	</div>
</div>
@endsection