@extends('layouts.master')

@section('content')
<div class="bg-cover flex-expand" style="background-image:url('/images/sea.png');">
    <div class="bg-white w-9/10 xl:w-4/5 max-w-2xl mt-16 mx-auto mb-16 px-12 py-16 rounded shadow-lg">

	  <div class="mb-6">
	      <a href="{{ route('admin.galleries.index') }}" class="text-green font-semibold">Atgriezties uz galerijām</a>
	  </div>

      <h1 class="font-bold text-2xl text-90 mb-6">Jauna galerija</h1>
      
      <div class="mt-6">
      	<form action="{{ route('admin.galleries.store') }}" method="POST">
  			@csrf
			<div class="w-full md:w-96 mt-6">
				<label class="form-label block mb-2" for="title_lv">Nosaukums (LV)<span class="text-red text-lg ml-1">*</span></label>
				<input class="form-input" id="title_lv" type="text" placeholder="Jauna galerija..." name="title_lv" value="{{ old('title_lv') }}" required autofocus>
				@if($errors->has('title_lv'))
					<p class="text-xs italic text-red font-regular mt-1">Norādi galerijas LV nosaukumu</p>
				@endif
			</div>

			<div class="w-full md:w-96 mt-6">
				<label class="form-label block mb-2" for="title_en">Nosaukums (EN)<span class="text-red text-lg ml-1">*</span></label>
				<input class="form-input" id="title_en" type="text" placeholder="Jauna galerija..." name="title_en" value="{{ old('title_en') }}" required autofocus>
				@if($errors->has('title_en'))
					<p class="text-xs italic text-red font-regular mt-1">Norādi galerijas EN nosaukumu</p>
				@endif
			</div>

			<div class="mt-12">
				<button class="bg-green hover:bg-green-dark text-white px-6 py-3 rounded-sm border border-green hover:border-green-dark font-semibold text-sm uppercase tracking-xl">Pievienot galeriju</button>
			</div>
      	</form>
      </div>
    </div>
  </div>
@endsection