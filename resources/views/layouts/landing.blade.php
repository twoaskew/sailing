<!doctype html>
<html lang="{{ app()->getLocale() }}" class="h-full">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="description" content="{{ __('meta.description') }}">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        @if(App::environment('production'))
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134558235-1"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-134558235-1');
            </script>
        @endif

        {{-- favicons --}}
        <link rel="icon" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png') }}">
        <link rel="icon" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png') }}">
        <link rel="icon" sizes="96x96" href="{{ asset('images/favicons/favicon-96x96.png') }}">
        <link rel="icon" sizes="192x192" href="{{ asset('images/favicons/android-icon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/favicons/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/favicons/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicons/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicons/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicons/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicons/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicons/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicons/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-icon-180x180.png') }}">

         {{-- meta tags for facebook --}}
        <meta property="og:url" content="{{ __('meta.fb-url') }}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Gulf of Riga Regatta">
        <meta property="og:image" content="https://gulfofrigaregatta.eu/images/fb-share-image-2023.png">
        <meta property="og:description" content="{{ __('meta.fb-description') }}">

        <title>{{ __('regatta.title') }} 2023</title>
        <link rel="stylesheet" href="{{ mix('css/index.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap&subset=latin-ext" rel="stylesheet">
        @yield('head-js')
        {{-- facebook pixel --}}
        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '606411136508527'); // business pixel
          fbq('init', '1265951956890508'); // davis pixel
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=1265951956890508&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body class="font-sans h-full flex flex-col">
        <div id="app">
            @yield('content')
            @include('partials.footer')
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
        @yield('footer-js')
    </body>
</html>
