@extends('layouts.master')

@section('content')
  <section class="bg-30 pb-24 flex-expand">
    <div class="w-9/10 max-w-md mx-auto">
      <h2 class="text-center uppercase text-lg text-60 font-normal mb-16 pt-12 md:pt-24">{{ __('contacts.contacts') }}</h2>
      <div class="mb-8 border-t border-50 border-solid">
        <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('contacts.organisers-subtitle') }}</h3>
        <p class="text-90 leading-normal mb-8">{{ __('contacts.organisers-description') }}</p>
      
        {{-- valters --}}
        <div class="mb-6 flex">
          <img src="/images/valters-romans-img.png" alt="Valters Romans" class="mr-6 self-start w-24 md:w-auto">
          <div>
            <h4 class="text-100 text-2xl font-light mb-2">Valters Romans</h4>
            <p><a href="tel:+37129235659" class="text-sm text-70 no-underline mb-1">+371 29235659</a></p>
            <p><a href="mailto:valters@laivulietas.lv" class="text-sm text-70 no-underline">valters@laivulietas.lv</a></p>
          </div>
        </div>
        {{-- kristine --}}
        <div class="mb-6 flex">
          <img src="/images/kristine-kanska-new-img.png" alt="Kristīne Kanska" class="mr-6 self-start w-24 md:w-auto">
          <div>
            <h4 class="text-100 text-2xl font-light mb-2">Kristīne Kanska</h4>
            <p><a href="tel:+37129247411" class="text-sm text-70 no-underline mb-1">+371 29247411</a></p>
            <p><a href="mailto:kanska1409@gmail.com" class="text-sm text-70 no-underline">kanska1409@gmail.com</a></p>
          </div>
        </div>
        {{-- ilona --}}
        <div class="mb-6 flex">
          <img src="/images/ilona.png" alt="Ilona Skorobogatova" class="mr-6 self-start w-24 md:w-auto">
          <div>
            <h4 class="text-100 text-2xl font-light mb-2">Ilona Skorobogatova</h4>
            <p class="text-sm text-70">{{ __('contacts.ilona-details') }}</p>
            <p><a href="tel:+37125922782" class="text-sm text-70 no-underline mb-1">+371 25922782</a></p>
            <p><a href="mailto:ilona.skorobogatova@gmail.com" class="text-sm text-70 no-underline">ilona.skorobogatova@gmail.com</a></p>
          </div>
        </div>
        {{-- silards --}}
        <div class="mb-6 flex">
          <img src="/images/silards-kamergrauzis-img.png" alt="Silards Kamergrauzis" class="mr-6 self-start w-24 md:w-auto">
          <div>
            <h4 class="text-100 text-2xl font-light mb-2">Silards Kamergrauzis</h4>
            <p><a href="tel:+37126434440" class="text-sm text-70 no-underline mb-1">+371 26434440</a></p>
            <p><a href="mailto:sk.firmamarisa@gmail.com" class="text-sm text-70 no-underline">sk.firmamarisa@gmail.com</a></p>
          </div>
        </div>
        {{-- piret --}}
        <div class="mb-6 flex">
          <img src="/images/piret-tank-img.png" alt="Piret Tank" class="mr-6 self-start w-24 md:w-auto">
          <div>
            <h4 class="text-100 text-2xl font-light mb-2">Piret Ausman</h4>
            <p><a href="tel:+37256500295" class="text-sm text-70 no-underline mb-1">+372 56500295</a></p>
            <p><a href="mailto:piret.tank@gmail.com" class="text-sm text-70 no-underline">piret.tank@gmail.com</a></p>
          </div>
        </div>
      </div>

      <div class="mb-8 border-t border-50 border-solid">
        <h3 class="text-2xl text-100 mt-10 mb-2">{{ __('contacts.details') }}</h3>
        {{-- <div class="mb-2">
          <a href="https://www.dropbox.com/sh/i3tkg0ccpende5b/AABzAT9my0bJEZAKTOvajjama?dl=0" target="_blank" rel="noopener" class="text-color-primary font-semibold no-underline">GoRR Logo</a>
        </div> --}}
        <p class="text-90 leading-normal mb-8">{{ __('contacts.payment-details') }}</p>
        <div class="flex">
          <img src="/images/rekviziti-logo.png" class="mr-6 self-start w-24 md:w-auto">
          <div>
            <div class="text-sm text-70 leading-normal">
              <h4 class="text-100 text-base font-bold mb-2 leading-none mb-2">Latvijas Jūras burāšanas asociācija</h4>{{ __('contacts.address') }}<br>
              {{ __('contacts.reg_no') }}<span class="ml-2">40008285681</span><br>
              IBAN<span class="ml-2">LV87HABA0551046481239</span><br>
              {{ __('contacts.bank') }}<span class="ml-2">Swedbank AS</span><br>
              SWIFT<span class="ml-2">HABALV22</span><br>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>
@endsection