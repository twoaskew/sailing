<?php

return [
	'title' => 'Gulf of Riga Regatta 2023',
	'subtitle_open' => 'Latvian Offshore Sailing Championship',
	'subtitle_livonia' => 'Livonia Sailing Cup'
];