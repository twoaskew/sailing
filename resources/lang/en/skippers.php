<?php
return [
	'entriesTitle' => 'Current entries',
	'entriesSubtitle' => 'Currently :skippers_count teams have registered for the regatta',
	'entriesSubtitleArchive' => ':skippers_count teams participated in the regatta',
	'yacht' => 'yacht',
	'yachtclub' => 'yachtclub',
	'group' => 'group',
	'loa' => 'loa',
	'makeType' => 'make, type',
	'year' => 'year',
	'skipper' => 'skipper',
	'ctaText' => 'Sign up your team',
	'ctaButton' => 'register',
	'recentlyOpen' => 'Registration for the regatta has been open recently.'
];
