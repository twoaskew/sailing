<?php

return [
	'info' => 'information',
	'contacts' => 'contacts',
	'archive' => 'archive/{year}',
	'news' => 'news',
	'news_show' => 'news/{slug}',
	'notices' => 'notices',
	'notices_show' => 'notices/{slug}',
	'skippers' => 'skippers',
	'registration' => 'registration',
	'registration-participants' => 'registration-for-participants',
	'login' => 'login',
	'users' => [
		'participant' => 'participant',
		'skipper' => 'skipper',
		'home' => 'my-application',
	],
	'skipper' => [
		'participants' => 'participants',
		'dashboard' => 'skipper/my-application',
	],
	'results' => 'results',
	'galleryShow' => 'galleries/{gallery}',
	'gallery' => 'gallery',
	'archivedGalleryShow' => 'archive/{year}/galleries/{gallery}',
	// 'archive' => 'archive',
];