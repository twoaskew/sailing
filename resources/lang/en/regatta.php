<?php

return [
	'title' => 'Gulf of Riga Regatta',
	'subtitle1' => 'Latvian Offshore Sailing Championship 2023',
	'subtitle2' => 'Livonia Cup Challenge',
	'countdown' => '2023 regatta from June 24 till June 30',
	'register' => 'Register',
	'find_a_team' => 'Find a team',
];