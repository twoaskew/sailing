<?php

return [
    'title' => 'Notices',
    'back' => 'All notices',
    'empty' => 'There are no notices at the moment.'
];