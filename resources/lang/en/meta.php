<?php

return [
		'description' => 'Gulf of Riga Regatta 2023 that is also Latvian Offshore Sailing Championship and Livonia Cup Challenge, takes place from June 24 to June 30 in the Gulf of Riga, going from Ventspils to Kuressaare with a stop at Roja.',
	'fb-description' => 'Gulf of Riga Regatta 2023 that is also Latvian Offshore Sailing Championship and Livonia Cup Challenge, takes place from June 24 to June 30 in the Gulf of Riga, going from Ventspils to Kuressaare with a stop at Roja.',
	'fb-url' => 'https://gulfofrigaregatta.eu/en',
];