<?php

return [
	'contacts' => 'Contacts',
	'organisers-subtitle' => 'Regatta organisers',
	'organisers-description' => 'If you have any questions don\'t hesitate to contact us.',
	'valters-details' => 'Board member of the Latvian Yachting Union',
	'kristine-details' => 'Race secretary',
	'silards-details' => 'Sponsorship and commercial inquiries',
	'ilona-details' => 'Public and media relations',
	'payment-details' => 'Payment information.',
	'reg_no' => 'Reg. No.',
	'bank' => 'Bank',
	'details' => 'Details',
	'address' => 'Grecinieku street 11A-5, Riga, LV-1050',
];