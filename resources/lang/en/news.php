<?php

return [
    'title' => 'News',
    'subtitle' => 'Here you will find the latest information about the regatta.',
    'back' => 'All news',
    'read_more' => 'Read more',
];