<?php
return [
    '404Text' => 'The page you are looking for is not available. Would you like to start again and visit <a href="/en" class="text-color-primary no-underline">home page</a>?',
    'email_exists' => 'Cannot find such e-mail',
    'emal_unique' => 'This e-mail address has already been registerd',
];