<?php

return [
	'home' => 'Home',
	'registration' => 'Registration',
	'join' => 'Looking for crew',
	'info' => 'Info',
	'participants' => 'Entry List',
	'new_participants' => 'Crew looking for boats',
	'news' => 'News',
	'results' => 'Results',
	'merchandise' => 'Clothing',
	'contacts' => 'Contacts',
	'login' => 'Login',
	'logout' => 'Logout',
	'my_profile' => 'My Profile',
	'my_team' => 'My Profile',
	'gallery' => 'Galleries',
	'archive' => 'Archive',
	'notices' => 'Notices',
];