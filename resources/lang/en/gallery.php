<?php

return [
    'title' => 'Galleries',
    'viewFullGalleryLink' => 'View full gallery',
    'allGalleriesLink' => 'All galleries',
];