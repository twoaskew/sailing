<?php

return [
	'title' => 'Gulf of Riga Regatta 2018',
	'news' => 'News',
	'results' => 'Results',
	'gallery' => 'Gallery',
	'archive' => 'Archive',
];