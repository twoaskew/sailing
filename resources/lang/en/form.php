<?php

return [
	'seeking_crew' => 'If you are looking for a crew to join, fill out <a href="https://ej.uz/u6bp" class="text-color-primary no-underline" target="_blank">"Crewseeking Application Form"</a>.',
	'required_fields' => 'required fields',
	'name' => 'Name, surname',
	'email' => 'E-mail',
	'phone' => 'Phone',
	'yacht' => [
		'name' => 'Name',
		'sail_number' => 'Sail number',
		'yacht_club' => 'Yacht club',
		'group' => 'Group',
		'loa' => 'LOA',
		'make_model' => 'Yacht type',
		'year' => 'Year',
		'image' => 'Image',
	],
	'nor_read' => 'I have read the <a href=":link" target="_blank" class="text-sm text-color-primary no-underline">notice of race</a>',
	'team_size' => 'Crew size',
	'extra_crew' => 'Extra crew',
	'profile_image' => 'Profile image',
	'description' => 'Sailing experience',
	'can_accept_people' => 'I am looking for crew members and would like to receive an email notification when a crew member is looking for a boat.',
	'save' => 'Save',
	'register' => 'Register',
	'errors' => [
		'name' => 'Please, tell us your name',
		'email' => 'Provide valied e-mail',
		'phone' => 'Please, tell us your phone number',
		'yacht' => [
			'name' => 'Please, tell us your yacht\'s name',
			'sail_number' => 'Please, tell us your yacht\'s sail number',
			'make_model' => 'Please, tell us your yacht\'s make and type',
			'year' => 'When was your yacht made?',
			'image' => 'Image size can\'t exceed 10MB',
		],
		'team_size' => 'Please, tell us how many crew members you have',
		'nor_read' => 'To sign up for the regatta, you have to review the notice of race',
		'description' => 'Please, tell us about yourself',
	],
	'placeholders' => [
		'name' => 'John Sailor',
		'email' => 'john@example.org',
		'phone' => '+371 2000000',
		'yacht' => [
			'name' => 'Black Lion',
			'yacht_club' => 'Falmouth',
			'make_model' => 'Benetti Yachts, X-41',
		],
		'description' => 'What is your sailing experience? Why do you want to participate in Gulf of Riga Regatta? Tell us about yourself.',
	]
];