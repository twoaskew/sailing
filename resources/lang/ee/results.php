<?php

return [
    'infoText' => 'Results will be available when regatta starts on June 26th.',
    'ctaText' => 'There is still time to sign up your team',
    'ctaButton' => 'Register', 
    'tempMessage' => 'At the moment results are available at ',
    'title' => 'Tulemused',
    'updated_at' => 'Updated at',
];