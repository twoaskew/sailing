<?php

return [
	'title' => 'Gulf of Riga Regatta',
	'subtitle1' => 'Läti Avamerepurjetamise meistrivõistlused 2023',
	'subtitle2' => 'Livonia Cup Challenge',
	'countdown' => '2023 regatt alates 24 juuni kuni 30 juuni',
	'register' => 'Registreeri',
	'find_a_team' => 'Find a team',
];