<?php

return [
	'title' => 'Gulf of Riga Regatta 2023',
	'subtitle_open' => 'Läti Avamerepurjetamise meistrivõistlused',
	'subtitle_livonia' => 'Livonia Sailing Cup'
];