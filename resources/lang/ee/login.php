<?php

return [
	'title' => 'My profile',
	'copy' => 'Sign in to access or edit your registration information.',
	'label' => 'E-mail',
	'placeholder' => 'john@example.org',
	'submit' => 'Sign in with email',
	'error' => 'This e-mail address was not recognized.',
	'mailSent' => 'Please check your inbox. We just sent a magic link to your email address. Click the link, and you will be signed in. ',
	'noEmail' => 'Didn\'t receive our email?',
	'linkHasExpired' => 'Unfortunately, your login link has expired.',
	'loginAgain' => 'Login again',
];