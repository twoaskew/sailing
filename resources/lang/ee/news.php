<?php

return [
    'title' => 'Uudised',
    'subtitle' => 'Siit leiad viimased uudised regati kohta.',
    'back' => 'Kõik uudised',
    'read_more' => 'Loe veel',
];