<?php
return [
	'information' => 'Informatsioon',
  'about' => 'About Gulf of Riga Regatta 2019',
	'description' => 'Gulf of Riga Regatta 2019, that is also Latvian Offshore Sailing Championship and Livonia Cup Challenge, will take place from June 25 to June 29 in the Gulf of Riga, going from Roja to Kuivizi with stops in Montu, Kuressaare and Ruhnu island. ',
	'entryFee' => 'Entry fee',
	'feeUnder30feet' => '30,00 EUR per each crew member and 40,00 EUR per boat if under 30 feet.',
	'feeOver30feet' => '30,00 EUR per each crew member and 60,00 EUR per boat if 30 feet or over.',
	'feeForOneStage' => 'Entry fee for one leg is 20,00 EUR per each crew member and 30,00 EUR per boat.',
	'paymentInfoTitle' => 'Payment information:',
	'paymentInfo' => 'Latvijas Zegeletaju savieniba<br>
          Reg. Nr. 50008003331<br>
          IBAN: LV67HABA000140J035673<br>
          A/S SWEDBANK<br>
          SWIFT: HABALV22<br>
          With note: "GoRR 2019", name of the yacht or sail number and number of crew members entering regatta.',
    'lysTitle' => 'Läti LYS mõõdukirjast',
    'lysWhy' => 'Jahi omanik  peab korraldama oma jahi täpsed mõõtmised ning taotlema Läti LYS mõõdukirja. Mõõdukirja hind 25.- eur. Täpsem info ja juhised mõõdukirja taotlemise kohta leiad <a href="https://latlys.wordpress.com/lat-lys-english/" target="_blank" class="text-color-primary font-semibold no-underline">LAT LYS kodulehelt</a> (inglise keeles).',
    'lysSend' => 'Completed measurement form must be sent to e-mail <a href="mailto:gikefabo@gmail.com" class="text-green font-semibold">gikefabo@gmail.com</a> or <a href="mailto:ilmaslax@gmail.com" class="text-green font-semibold">ilmaslax@gmail.com</a>',
    'lysPay' => 'Measurement fee 20 EUR shall be paid to Yacht Club Engure account:',
    'lysPaymentInfo' => 'Biedriba "Jahtklubs ENGURE"<br>
        Reg.No: 40008122558<br>
        Bank: Swedbank<br>
        IBAN: LV91HABA0551019653780<br>
        In the subject please state: "LAT LYS, name of the yacht"',
    'lysAdditionalInfo' => 'For additional information please contact <a href="mailto:gikefabo@gmail.com" class="text-green font-semibold">gikefabo@gmail.com</a> or LAT LYS handicap system <a href="https://latlys.wordpress.com/" target="_blank" class="text-green font-semibold">website</a>.',
    'documents' => 'Dokumendid',
    'noticeOfRace' => '<a href=":download_link" target="_blank" class="text-green font-semibold">Notice of Race</a>.pdf',
    'entryForm' => '<a href=":download_link" target="_blank" class="text-green font-semibold">Entry form</a>.docx',
    'inspectionList' => '<a href=":download_link" target="_blank" class="text-green font-semibold">Inspection list</a>.docx',
    'ORCSailList' => '<a href=":download_link" target="_blank" class="text-green font-semibold">ORC sail list</a>.docx',
    'LYSSailList' => '<a href=":download_link" target="_blank" class="text-green font-semibold">LYS sail list</a>.docx',
    'usefulLinks' => 'Useful links',
    // 'hysInsurance' => 'Yacht Insurance Hamburger Yachtversicherung Schomacker',
    'hysInsurance' => '<a href="https://www.schomacker.de/en/yacht-boat-insurance/online-inquiry.html?pid=a1182" target="_blank" class="text-green font-semibold">Yacht Insurance</a> Hamburger Yachtversicherung Schomacker',
    'aboutOrcTitle' => 'Lühidalt ORC mõõdukirjast',
    'aboutOrcText' => 'Kõikide ORC mõõdukirju puudutavate küsimustega võib pöörduda nii otse ORC poole <a href="https://www.orc.org/" class="text-color-primary font-semibold no-underline">https://www.orc.org/</a> või saata küsimus Eesti ORC esindajatele. Eesti ORC esindajate e-mail: <a href="mailto:tehniline@puri.ee" class="text-color-primary font-semibold no-underline">tehniline@puri.ee</a>. Korduva mõõdukirja taotlemine: Kui kõik andmed jäävad samaks, siis piisab muudatusteta mõõdukirja ostu vormistamisest Eesti Jahtklubide Liidu kodulehel <a href="http://www.puri.ee/" class="text-color-primary font-semibold no-underline">http://www.puri.ee/</a> olevas e-poes. Kui aga jahil on tehtud muudatusi, mis võivad muuta jahti kiiremaks, siis tuleb vormistada muudatustega mõõdukirja ost Eesti Jahtklubide Liidu kodulehel <a href="http://www.puri.ee/" class="text-color-primary font-semibold no-underline">http://www.puri.ee/</a> olevas e-poes ning anda avaldusega muudatustest teada e-posti addressile <a href="mailto:moodukiri@puri.ee" class="text-color-primary font-semibold no-underline">moodukiri@puri.ee</a>.',
    'aboutTexelTitle' => 'TEXEL mõõdukiri',
    'aboutTexelText' => 'Rohkem infot Läti Texel mõõdukirja kohta saab Uldis Kronblums käest Läti Katamaraanide Liidust, <a href="mailto:ldja@parks.lv" class="text-color-primary font-semibold no-underline">ldja@parks.lv</a>',
    'harborsTitle' => 'Sadamate sügavuskaardid',
    'rigaCityYachtClub' => 'Riga, City Yacht Club',
    'ruhnu' => 'Ruhnu',
    'salacgriva' => 'Salacgriva',
    'parnu' => 'Parnu',

];