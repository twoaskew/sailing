<?php

return [
    'title' => 'Galerii',
    'viewFullGalleryLink' => 'Vaata kogu galeriid',
    'allGalleriesLink' => 'Kõik albumid',
];