<?php

return [
	'standWithUkraine' => 'Toetame Ukrainat',
	'regattaHasEnded' => 'Regatt on lõppenud',
	'schedule' => 'Regati ajakava',
	'news' => 'Uudised',
	'organizers' => 'Korraldajad',
	'partners' => 'Partnerid ja sponsorid',
	'date' => '27. juuni - 2. juuli',
	'followLive' => 'Regatt on avatud',
	'results' => 'Tulemused',
	'stage1' => 'Roja',
	'stage2' => 'Montu',
	'stage3' => 'Kuressaare',
	'stage4' => 'Ruhnu',
	'stage5' => 'Kuivizi',
	'organizing_team' => 'Regati korralduspere',
	'friends_promoters' => 'Sõbrad ja promootorid',
	'calendar' => 'Regati ajakava',	
	'date1' => '24',
	'date2' => '25',
	'date3' => '26',
	'date4' => '27',
	'date5' => '28',
	'date6' => '29',
	'date7' => '30',
	'june' => 'juuni',
	'july' => 'juuli',
	'day1' => 'Day 1. Roja - Montu',
	'day2' => 'Day 2. Montu - Kuressaare',
	'day3' => 'Day 3. Kuressaare - Ruhnu',
	'day4' => 'Day 4. Ruhnu - Kuivizi',
	'day5' => 'Day 5. Kuivizi - Kuivizi',

	'day1-title' => 'Ventspils',
	'day1-body' => '
		<ul>
			<li>16:00 - 20:00 Check-in and boat inspections</li>
		</ul>
	',

	'day2-title' => 'Ventspils',
	'day2-body' => '
		<ul>
			<li>10:00 - 16:00 Check-in and boat inspections</li>
			<li>17:00 Boat parade on the Venta river</li>
			<li>19:00 Opening ceremony and welcome event</li>
		</ul>
	',

	'day3-title' => 'Ventspils',
	'day3-body' => '
		<ul>
			<li>LEG 1 - Coastal race, LEG 1 award ceremony</li>
		</ul>
	',

	'day4-title' => 'Ventspils',
	'day4-body' => '
		<ul>
			<li>LEG 2 - Coastal race Ventspils - Roja</li>
		</ul>
	',

	'day5-title' => 'Roja',
	'day5-body' => '
		<ul>
			<li>LEG 3 - Coastal race and windward / leeward races</li>
			<li>Social event for participants</li>
			<li>Award ceremony for LEG 2 and LEG 3</li>
		</ul>
	',

	'day6-title' => 'Roja',
	'day6-body' => '
		<ul>
			<li>LEG 4 - Roja - Kuressaare</li>
		</ul>
	',

	'day7-title' => 'Kuressaare',
	'day7-body' => '
		<ul>
			<li>LEG 5 - Coastal race around Abruka island</li>
			<li>Award ceremony for LEG 4 and LEG 5</li>
			<li>Regatta winner\'s award and closing ceremony</li>
		</ul>
	',

	'calendarFooter' => '*ajakava täpsustakse',

	'feature1' => 'Take a challenge and great racing experience in the Gulf of Riga',
	'feature2' => 'Be the first winner of Livonia Sailing Cup',
	'feature3' => 'Join the most environmental friendly sailing competition in the region',

	'allNewsButton' => 'All news',
	'grislisInfo' => 'City Yachtclub representative',
	'romansInfo' => 'Läti Jahtklubide Liidu juhatuse liige',
	'megnisInfo' => 'Roja Purjespordikooli esindaja',
	'riimInfo' => 'Saaremaa Merispordi Seltsi esindaja',
	'ausmanInfo' => 'Peavõistlusjuht',
	'kanskaInfo' => 'Peasekretär',
	'ilonaInfo' => 'Meediajuht',
	'silardsInfo' => 'Sponsorship and commercial inquiries',
	'salinaInfo' => 'Graafiline disain',
	'davisInfo' => 'Kodulehe arendaja',

	'covid19Notice' => 'Notice regarding COVID-19',
	'covid19Link' => 'https://gorr.lv/en/news/announcement-on-progress-on-the-gulf-of-riga-regatta-in-2020',
	'covid19LastUpdated' => 'Last updated on May 22th',

	'signUpHere' => 'register <span class="text-color-secondary">here</span>',
	'thankYouForParticipating' => 'Thank you for participation',
	'seeYouNextYear' => 'See you next year',
];