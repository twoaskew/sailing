<?php

return [
	'my_profile' => 'My profile',
	'profile_copy' => 'Here\'s a summary of your skipper\'s submission for the Gulf of Riga Regatta.',
	'skipper' => 'Skipper',
	'description' => 'This information will be used to contact skipper regarding the regatta.',
	'yacht' => 'Yacht',
	'yacht-description' => 'Information about the yacht will be used to determine the regatta\'s groups.',
	'team' => 'Crew',
	'team-description' => 'How many members will be in the crew? Number of crew members will be clarified on the day regatta starts.',
	'extra_crew' => 'Crew looking for boats',
	'extra_crew_copy' => 'These crew members are looking for boats, to participate in the Regatta. Feel free to cantact them or send invitations so they can join your crew.',
	'no_extra_crew' => 'Right now there are no free crew members, who are looking for boats. Be sure to check out this page later.',
	'invite' => 'Invite crew member',
	'invited' => 'You have already invited this person to join your crew',
	'revoke_invitation' => 'Revoke invitation',
	'invite_success' => 'Invitation sent!',
	'invite_revoked' => 'Invitation revoked!',
	'registration' => 'Registration',
	'registration_copy' => 'Please fill out this form if you would like to participate in the Gulf of Riga Regatta 2018.',
	'thanks' => [
		'title' => 'Thank you, we have received your application',
		'copy' => 'If you would like to change the information in your application you can do so, by signing in to your account. Simply click "Login" in the top navigation, which will lead you to the profile page, where you can edit your information.',
	],
	'update_success' => 'Your profile has been updated.',
	'notice_of_race' => 'Notice of Race 2020',
];