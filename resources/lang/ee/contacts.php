<?php

return [
	'contacts' => 'Kontakt',
	'organisers-subtitle' => 'Regati korraldajad',
	'organisers-description' => 'Kui Sul on küsimusi, ära pelga meiega ühendust võtta!',
	'valters-details' => 'Läti Jahtklubide Liidu juhatuse liige',
	'kristine-details' => 'Peasekretär',
	'silards-details' => 'Sponsorsuhete ja finantsjuht',
	'ilona-details' => 'Meediajuht',
	'payment-details' => 'Makseinfo',
	'reg_no' => 'Reg.kood',
	'bank' => 'Pank',
	'details' => 'Makseviisid',
	'address' => 'Grecinieku 11A-5, Riga, LV-1050',
];