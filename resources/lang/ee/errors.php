<?php
return [
    '404Text' => 'Otsitud lehekülge ei leitud. Soovid Sa tagasi minna <a href="/ee" class="text-color-primary no-underline">pealehele</a>?',
    'email_exists' => 'Cannot find such e-mail',
    'emal_unique' => 'This e-mail address has already been registerd',
];