<?php

return [
	'info' => 'info',
	'contacts' => 'kontakt',
	'archive' => 'arhiiv/{year}',
	'news' => 'uudised',
	'news_show' => 'uudised/{slug}',
	'notices' => 'teated',
	'notices_show' => 'teated/{slug}',
	'skippers' => 'kaptenid',
	'registration' => 'registreerimine',
	'registration-participants' => 'meeskonnaliikmete-registreerimine',
	'login' => 'login',
	'users' => [
		'participant' => 'meeskonnaliige',
		'skipper' => 'kapten',
		'home' => 'minu-avaldus',
	],
	'skipper' => [
		'participants' => 'meeskond',
		'dashboard' => 'kapten/minu-avaldus',
	],
	'results' => 'tulemused',
	'galleryShow' => 'galeriid/{gallery}',
	'gallery' => 'galerii',
	'archivedGalleryShow' => 'arhiiv/{year}/galeriid/{gallery}',
	// 'archive' => 'archive',
];