<?php

return [
	'home' => 'Avaleht',
	'registration' => 'Registreerimine',
	'join' => 'Looking for crew',
	'info' => 'Info',
	'participants' => 'Osalejad',
	'new_participants' => 'Crew looking for boats',
	'news' => 'Uudised',
	'results' => 'Tulemused',
	'merchandise' => 'Regatisärgid ja meened',
	'contacts' => 'Kontaktid',
	'login' => 'Logi sisse',
	'logout' => 'Logi välja',
	'my_profile' => 'My Profile',
	'my_team' => 'My Profile',
	'gallery' => 'Galerii',
	'archive' => 'Arhiiv',
	'notices' => 'Teated',
];