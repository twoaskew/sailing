<?php

return [
	'title' => 'Gulf of Riga Regatta 2018',
	'news' => 'Uudised',
	'results' => 'Tulemused',
	'gallery' => 'Galerii',
	'archive' => 'Arhiiv',
];