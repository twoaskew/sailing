<?php

return [
	'info' => 'informacija',
	'contacts' => 'kontaktai',
	'archive' => 'archyvas/{year}',
	'news' => 'naujienos',
	'news_show' => 'naujienos/{slug}',
	'notices' => 'komentarai',
	'notices_show' => 'komentarai/{slug}',
	'skippers' => 'kapitonai',
	'registration' => 'registracija',
	'registration-participants' => 'dalyviu-registracija',
	'login' => 'login',
	'users' => [
		'participant' => 'dalyviai',
		'skipper' => 'kapitonas',
		'home' => 'mano-profilis',
	],
	'skipper' => [
		'participants' => 'dalyviai',
		'dashboard' => 'kapitonas/mano-profilis',
	],
	'results' => 'rezultatai',
	'galleryShow' => 'galerijos/{gallery}',
	'gallery' => 'galerija',
	'archivedGalleryShow' => 'archyvas/{year}/galerijos/{gallery}',
	// 'archive' => 'archyvas',
];