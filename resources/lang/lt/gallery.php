<?php

return [
    'title' => 'Galerija',
    'viewFullGalleryLink' => 'Peržiūrėti galeriją',
    'allGalleriesLink' => 'Visi albumai',
];