<?php

return [
	'standWithUkraine' => 'Mes palaikome Ukrainą',
	'regattaHasEnded' => 'Regatos pabaiga',
	'schedule' => 'Regatos planas',
	'news' => 'Naujienos',
	'organizers' => 'Organizatoriai',
	'partners' => 'Partneriai ir rėmėjai',
	'date' => 'Birželio 27 - liepos 2',
	'followLive' => 'Regatos pradžia',
	'results' => 'Rezultatai',
	'stage1' => 'Roja',
	'stage2' => 'Montu',
	'stage3' => 'Kuressaare',
	'stage4' => 'Ruhnu',
	'stage5' => 'Kuivizi',
	'organizing_team' => 'Komanda',
	'friends_promoters' => 'Draugai ir propaguotojai',
	'calendar' => 'Regatta Schedule',	
	'date1' => '24',
	'date2' => '25',
	'date3' => '26',
	'date4' => '27',
	'date5' => '28',
	'date6' => '29',
	'date7' => '30',
	'june' => 'birželio',
	'july' => 'liepos',
	'day1' => 'Day 1. Roja - Montu',
	'day2' => 'Day 2. Montu - Kuressaare',
	'day3' => 'Day 3. Kuressaare - Ruhnu',
	'day4' => 'Day 4. Ruhnu - Kuivizi',
	'day5' => 'Day 5. Kuivizi - Kuivizi',

	'day1-title' => 'Ventspils',
	'day1-body' => '
		<ul>
			<li>16:00 - 20:00 Check-in and boat inspections</li>
		</ul>
	',

	'day2-title' => 'Ventspils',
	'day2-body' => '
		<ul>
			<li>10:00 - 16:00 Check-in and boat inspections</li>
			<li>17:00 Boat parade on the Venta river</li>
			<li>19:00 Opening ceremony and welcome event</li>
		</ul>
	',

	'day3-title' => 'Ventspils',
	'day3-body' => '
		<ul>
			<li>LEG 1 - Coastal race, LEG 1 award ceremony</li>
		</ul>
	',

	'day4-title' => 'Ventspils',
	'day4-body' => '
		<ul>
			<li>LEG 2 - Coastal race Ventspils - Roja</li>
		</ul>
	',

	'day5-title' => 'Roja',
	'day5-body' => '
		<ul>
			<li>LEG 3 - Coastal race and windward / leeward races</li>
			<li>Social event for participants</li>
			<li>Award ceremony for LEG 2 and LEG 3</li>
		</ul>
	',

	'day6-title' => 'Roja',
	'day6-body' => '
		<ul>
			<li>LEG 4 - Roja - Kuressaare</li>
		</ul>
	',

	'day7-title' => 'Kuressaare',
	'day7-body' => '
		<ul>
			<li>LEG 5 - Coastal race around Abruka island</li>
			<li>Award ceremony for LEG 4 and LEG 5</li>
			<li>Regatta winner\'s award and closing ceremony</li>
		</ul>
	',

	'calendarFooter' => '*informacija bus patikslinta',

	'feature1' => 'Take a challenge and great racing experience in the Gulf of Riga',
	'feature2' => 'Be the first winner of Livonia Sailing Cup',
	'feature3' => 'Join the most environmental friendly sailing competition in the region',

	'allNewsButton' => 'All news',
	'grislisInfo' => 'City Yachtclub representative',
	'romansInfo' => 'Latvijos jachtų klubo asociacijos valdybos narys',
	'megnisInfo' => 'Roja buriavimo mokyklos atstovas',
	'riimInfo' => 'Saremos jūrų sporto draugijos atstovas',
	'ausmanInfo' => 'Trasa',
	'kanskaInfo' => 'Trasos sekretoriatas',
	'ilonaInfo' => 'Reklama',
	'silardsInfo' => 'Bendradarbiavimas',
	'salinaInfo' => 'Grafikos dizainas',
	'davisInfo' => 'Internetinio puslapio kūrėjas',

	'covid19Notice' => 'Notice regarding COVID-19',
	'covid19Link' => 'https://gorr.lv/en/news/announcement-on-progress-on-the-gulf-of-riga-regatta-in-2020',
	'covid19LastUpdated' => 'Last updated on May 22th',

	'signUpHere' => 'register <span class="text-color-secondary">here</span>',
	'thankYouForParticipating' => 'Thank you for participation',
	'seeYouNextYear' => 'See you next year',
];