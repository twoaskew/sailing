<?php

return [
	'title' => 'Gulf of Riga Regatta 2023',
	'subtitle_open' => 'Latvijos buriavimo jūroje čempionatas',
	'subtitle_livonia' => 'Livonijos buriavimo taurė'
];