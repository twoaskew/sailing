<?php

return [
	'submission' => 'My submission',
	'new_submission' => 'Find a boat',
	'new_submission_copy' => 'If you are a crew member and would like to participate in the Regatta, please fill out this form and all skippers will be notified, that you are looking for a crew member spot.',
	'submission_copy' => 'Here\'s a summary of your crew member submission for the Gulf of Riga Regatta 2018.',
	'contact_info' => 'Contact Information',
	'about' => 'About me',
	'about_you' => 'About you',
	'delete_submission' => 'Delete submission',
	'onBoard' => 'You have joined the crew of <span class="font-bold text-black capitalize">:yacht</span>!',
	'invitation' => 'Team <span class="font-bold text-black capitalize">:yacht</span> wants you to join their crew!',
	'accept' => 'Accept',
	'decline' => 'Decline',
	'accept_success' => 'Invitation accepted!',
	'thanks' => [
		'title' => 'Thank you, we have received your application!',
		'copy1' => 'If you would like to edit your profile information, click "Login".',
		'copy2' => 'Skippers will be notified, that you are looking for a boat to join for the Regatta. Once somebody invites you, you will receive an email notification.',
	]
];