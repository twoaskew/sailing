<?php

return [
	'home' => 'Pradžia',
	'registration' => 'Registracija',
	'join' => 'Looking for crew',
	'info' => 'Info',
	'participants' => 'Dalyviai',
	'new_participants' => 'Crew looking for boats',
	'news' => 'Naujienos',
	'results' => 'Rezultatai',
	'merchandise' => 'Regatos atributika',
	'contacts' => 'Kontaktai',
	'login' => 'Prisijungti',
	'logout' => 'Atsijungti',
	'my_profile' => 'My Profile',
	'my_team' => 'My Profile',
	'gallery' => 'Galerija',
	'archive' => 'Archyvai',
	'notices' => 'Komentarai',
];