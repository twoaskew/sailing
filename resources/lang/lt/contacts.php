<?php

return [
	'contacts' => 'Kontaktai',
	'organisers-subtitle' => 'Regatos organizatoriai',
	'organisers-description' => 'Jei turite klausimų, prašome kreiptis į mus.',
	'valters-details' => 'Latvijos jachtų klubo asociacijos narys',
	'kristine-details' => 'Sekretoriatas',
	'silards-details' => 'Bendradarbiavimas',
	'ilona-details' => 'Reklama',
	'payment-details' => 'Informacija apie mokėjimus',
	'reg_no' => 'Registracijos nr.',
	'bank' => 'Banks',
	'details' => 'Informacija',
	'address' => 'Grecinieku g. 11A-5, Riga, LV-1050',
];