<?php

return [
    'title' => 'Naujienos',
    'subtitle' => 'Čia rasite naujausią informaciją apie Regatą',
    'back' => 'Visos naujienos',
    'read_more' => 'Skaityti daugiau',
];