<?php

return [
	'title' => 'Gulf of Riga Regatta',
	'subtitle1' => 'Latvijos buriavimo jūroje čempionatas 2023',
	'subtitle2' => 'Livonijos taurė',
	'countdown' => '2023 regata nuo birželio 24d. iki 30d.',
	'register' => 'Registracija',
	'find_a_team' => 'Find a team',
];