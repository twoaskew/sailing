<?php

return [
    'title' => 'Komentarai',
    'back' => 'Visi komentarai',
    'empty' => 'Šiuo metu nėra komentarų'
];