<?php
return [
    '404Text' => 'Puslapis kurio ieškote, šiuo metu neprieinamas. Ar norite grįžti į <a href="/lt" class="text-color-primary no-underline">pagrindinį puslapį</a>?',
    'email_exists' => 'Cannot find such e-mail',
    'emal_unique' => 'This e-mail address has already been registerd',
];