<?php

return [
	'title' => 'Mana reģistrācijas informācija',
	'copy' => 'Lai apskatītu vai labotu informāciju, kuru norādīji pieteikumā, ievadi e-pastu, ar kuru reģistrējies regatei.',
	'label' => 'E-pasts',
	'placeholder' => 'valdis@example.org',
	'submit' => 'Ienākt',
	'error' => 'Šī e-pasta adrese nav reģistrēta dalībai regatē',
	'mailSent' => 'Saiti ar piekļuvi profilam nosūtījām uz Tavu norādīto e-pasta adresi. Lūdzu pārbaudi savu e-pastu un seko tajā norādītajām instrukcijām.',
	'noEmail' => 'Nesaņēmi e-pasta vēstuli?',
	'linkHasExpired' => 'Diemžēl Tavai ienākšanas saitei ir beidzies derīguma termiņš.',
	'loginAgain' => 'Ienākt vēlreiz',
];