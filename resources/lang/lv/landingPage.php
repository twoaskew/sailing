<?php

return [
	'standWithUkraine' => 'Mēs atbalstām Ukrainu',
	'regattaHasEnded' => 'Regate ir noslēgusies',
	'schedule' => 'Regates maršruts',
	'news' => 'Jaunumi',
	'organizers' => 'Regati organizē',
	'partners' => 'Burājam kopā ar',
	'date' => '28. jūnijs - 3. jūlijs',
	'followLive' => 'Seko regates norisei',
	'results' => 'Rezultāti',
	'stage1' => 'Roja',
	'stage2' => 'Montu',
	'stage3' => 'Kuresāre',
	'stage4' => 'Roņu sala',
	'stage5' => 'Kuiviži',
	'organizing_team' => 'Regates organizētāji',
	'friends_promoters' => 'Draugi & atbalstītāji',
	'calendar' => 'Regates kalendārs',
	'date1' => '24.',
	'date2' => '25.',
	'date3' => '26.',
	'date4' => '27.',
	'date5' => '28.',
	'date6' => '29.',
	'date7' => '30.',
	'june' => 'jūnijs',
	'july' => 'jūlijs',
	'day1' => '1. diena. Roja - Montu',
	'day2' => '2. diena. Montu - Kuresāre',
	'day3' => '3. diena. Kuresāre - Roņu sala',
	'day4' => '4. diena. Roņu sala - Kuiviži',
	'day5' => '5. diena. Kuiviži - Kuiviži',

	'day1-title' => 'Ventspils',
	'day1-body' => '
		<ul>
			<li>16:00 - 20:00 Ierašanās, pieteikšanās, aprīkojuma pārbaude</li>
		</ul>
	',

	'day2-title' => 'Ventspils',
	'day2-body' => '
		<ul>
			<li>10:00 - 16:00 Ierašanās, pieteikšanās, aprīkojuma pārbaude</li>
			<li>17:00 Laivu parāde Ventas upē</li>
			<li>19:00 Sacensību atklāšana</li>
		</ul>
	',

	'day3-title' => 'Ventspils',
	'day3-body' => '
		<ul>
			<li>Pirmā sacensību diena - piekrastes distance jūrā netālu no Ventspils ostas</li>
			<li>Vakarā pirmā posma uzvarētāju apbalvošana</li>
		</ul>
	',

	'day4-title' => 'Ventspils',
	'day4-body' => '
		<ul>
			<li>Otrā sacensību diena - jūras brauciens Ventspils - Roja</li>
			<li>Pēc finiša Rojā tradicionālā zupa</li>
		</ul>
	',

	'day5-title' => 'Roja',
	'day5-body' => '
		<ul>
			<li>Trešā sacensību diena - piekrastes distance Roja - Roja un olimpiskā pievēja / pavēja distance</li>
			<li>Vakarā saviesīgs pasākums dalībniekiem un otrā un trešā posma uzvarētāju apbalvošana</li>
		</ul>
	',

	'day6-title' => 'Roja',
	'day6-body' => '
		<ul>
			<li>Ceturtā sacensību diena - jūras brauciens Roja - Kuresāre</li>
		</ul>
	',

	'day7-title' => 'Kuresāre',
	'day7-body' => '
		<ul>
			<li>Noslēdzošā sacensību diena - piekrastes brauciens apkārt Abrukas salai</li>
			<li>Ceturtā un piektā posma uzvarētāju apbalvošana</li>
			<li>Regates uzvarētāju apbalvošana un noslēgums</li>
		</ul>
	',

	'calendarFooter' => '* Informācija tiks precizēta',

	'feature1' => 'Pieņem izaicinājumu un izbaudi sacensības Rīgas Jūras līcī',
	'feature2' => 'Kļūsti par pirmo Livonijas kausa ieguvēju',
	'feature3' => 'Piedalies regatē, kuras dalībniekiem rūp vides aizsardzība',

	'allNewsButton' => 'Visi jaunumi',
	'grislisInfo' => 'Pilsētas jahtkluba pārstāvis',
	'romansInfo' => 'Latvijas Zēģelētāju savienības valdes loceklis',
	'megnisInfo' => 'Rojas Zēģelētāju skolas pārstāvis',
	'riimInfo' => 'Saaremaa Merispordi Selts pārstāvis',
	'ausmanInfo' => 'Galvenais tiesnesis',
	'kanskaInfo' => 'Sekretāre',
	'ilonaInfo' => 'Sabiedriskās un mediju attiecības',
	'silardsInfo' => 'Korporatīvie un finanšu jautājumi',
	'salinaInfo' => 'Grafiskais dizains',
	'davisInfo' => 'Web izstrāde',

	'covid19Notice' => 'Paziņojums saistībā ar COVID-19',
	'covid19Link' => 'https://gorr.lv/lv/jaunumi/pazinojums-par-gulf-of-riga-regatta-sacensibu-norisi-2020-gada',
	'covid19LastUpdated' => 'Atjaunināts 22. maijā',

	'signUpHere' => 'piesakies <span class="text-color-secondary">šeit</span>',
	'thankYouForParticipating' => 'Paldies par dalību regatē',
	'seeYouNextYear' => 'Tiekamies nākamgad',
];