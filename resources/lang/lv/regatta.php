<?php

return [
	'title' => 'Gulf of Riga Regatta',
	'subtitle1' => 'Latvijas atklātais jūras burāšanas čempionāts 2023',
	'subtitle2' => 'Livonijas kausa izcīņa',
	'countdown' => '2023. gada regate no 24. līdz 30. jūnijam',
	'register' => 'Pieteikties',
	'find_a_team' => 'Meklēju komandu',
];