<?php

return [
    'title' => 'Galerijas',
    'viewFullGalleryLink' => 'Apskatīt visu galeriju',
    'allGalleriesLink' => 'Visas galerijas',
];