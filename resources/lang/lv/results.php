<?php

return [
    'infoText' => 'Rezultāti būs pieejami, regatei sākoties 26. jūnijā.',
    'ctaText' => 'Vēl ir laiks regatei pieteikt arī savu komandu',
    'ctaButton' => 'Reģistrēties',
    'tempMessage' => 'Šobrīd rezultāti ir pieejami ',
    'title' => 'Rezultāti',
    'updated_at' => 'Pēdējo reizi atjaunoti',
];