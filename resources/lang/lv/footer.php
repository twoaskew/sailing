<?php

return [
	'title' => 'Gulf of Riga Regatta 2023',
	'subtitle_open' => 'Latvijas Atklātais jūras burāšanas čempionāts',
	'subtitle_livonia' => 'Livonijas kausa izcīņa',
];