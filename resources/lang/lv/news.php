<?php

return [
    'title' => 'Jaunumi',
    'subtitle' => 'Šeit jūs atradīsiet jaunāko informāciju par regates norisi.',
    'back' => 'Visi jaunumi',
    'read_more' => 'Lasīt vairāk',
];