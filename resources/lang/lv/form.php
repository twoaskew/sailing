<?php

return [
	'seeking_crew' => 'Ja Tu meklē komandu, kurai pievienoties, aizpildi <a href="https://ej.uz/u6bp" class="text-color-primary no-underline" target="_blank">"Meklēju komandu" pieteikuma formu</a>.',
	'required_fields' => 'aizpildāmi obligāti',
	'name' => 'Vārds, uzvārds',
	'email' => 'E-pasts',
	'phone' => 'Telefons',
	'yacht' => [
		'name' => 'Nosaukums',
		'sail_number' => 'Buras numurs',
		'yacht_club' => 'Jahtklubs',
		'group' => 'Grupa',
		'make_model' => 'Ražotājs, tips',
		'year' => 'Gads',
		'image' => 'Attēls',
	],
	'nor_read' => 'Esmu iepazinies ar <a href=":link" target="_blank" class="text-sm text-color-primary no-underline">sacensību nolikumu</a>',
	'team_size' => 'Dalībnieku skaits',
	'extra_crew' => 'Papildus dalībnieki',
	'profile_image' => 'Profila bilde',
	'description' => 'Burāšanas pieredze',
	'can_accept_people' => 'Varu uzņemt papildus dalībniekus, vēlos saņemt informāciju par tiem e-pastā.',
	'save' => 'Saglabāt',
	'register' => 'Pieteikties',
	'errors' => [
		'name' => 'Norādi vārdu, uzvārdu',
		'email' => 'Norādi derīgu e-pasta adresi',
		'phone' => 'Norādi telefona numuru',
		'yacht' => [
			'name' => 'Norādi jahtas nosaukumu',
			'sail_number' => 'Norādi buras numuru',
			'group' => 'Norādi jahtas grupu',
			'loa' => 'Norādi jahtas garumu',
			'make_model' => 'Norādi jahtas ražotāju un tipu',
			'year' => 'Norādi jahtas izlaiduma gadu',
			'image' => 'Attēla izmērs nedrīkst pārsniegt 10 MB',
		],
		'team_size' => 'Norādi komandas dalībnieku skaitu',
		'nor_read' => 'Lai pieteiktos, Tev ir jāiepazīstās ar sacensību nolikumu',
		'description' => 'Lūdzu pastāsti par sevi',
	],
	'placeholders' => [
		'name' => 'Jānis Bērziņš',
		'email' => 'janis@example.org',
		'phone' => '+371 2000000',
		'yacht' => [
			'name' => 'Selga',
			'yacht_club' => 'Lielupe',
			'make_model' => 'Benetti Yachts, X-41',
		],
		'description' => 'Vai esi kādreiz burājis? Kādēļ vēlies piedalīties regatē? Pastāsti par sevi.',
	]
];