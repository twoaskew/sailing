<?php
return [
    '404Text' => 'Lapa, kuru Tu meklē, neeksistē. Varbūt vēlies doties uz <a href="/lv" class="text-color-primary no-underline">sākumu</a> un meklēt no jauna?',
    'email_exists' => 'Norādītais e-pasts nav reģistrēts dalībai regatē',
    'emal_unique' => 'Pieteikums ar šādu e-pasta adresi ir jau saņemts',
];