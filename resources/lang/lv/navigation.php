<?php

return [
	'home' => 'Sākums',
	'registration' => 'Pieteikšanās',
	'join' => 'Meklēju komandu',
	'info' => 'Info',
	'participants' => 'Dalībnieki',
	'new_participants' => 'Papildus dalībnieki',
	'news' => 'Jaunumi',
	'results' => 'Rezultāti',
	'merchandise' => 'Apģērbs',
	'contacts' => 'Kontakti',
	'login' => 'Ienākt',
	'logout' => 'Iziet',
	'my_profile' => 'Mans Pieteikums',
	'my_team' => 'Mans Pieteikums',
	'gallery' => 'Galerijas',
	'archive' => 'Arhīvs',
	'notices' => 'Paziņojumi',
];