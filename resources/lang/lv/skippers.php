<?php
return [
	'entriesTitle' => 'Dalībnieki',
	'entriesSubtitle' => 'Šobrīd regatei ir pieteikušās :skippers_count komandas',
	'entriesSubtitleArchive' => 'Regatē piedalījās :skippers_count komandas',
	'yacht' => 'jahta',
	'yachtclub' => 'jahtklubs',
	'group' => 'grupa',
	'loa' => 'loa',
	'makeType' => 'ražotājs, tips',
	'year' => 'gads',
	'skipper' => 'kapteinis',
	'ctaText' => 'Piesaki regatei arī savu komandu',
	'ctaButton' => 'pieteikties',
	'recentlyOpen' => 'Reģistrācija regatei ir nesen sākta.'
];
