<?php

return [
	'info' => 'informacija',
	'contacts' => 'kontakti',
	'news' => 'jaunumi',
	'news_show' => 'jaunumi/{slug}',
	'notices' => 'pazinojumi',
	'notices_show' => 'pazinojumi/{slug}',
	'archive' => 'arhivs/{year}',
	'skippers' => 'dalibnieki',
	'registration' => 'registracija',
	'registration-participants' => 'registracija-dalibniekiem',
	'login' => 'ienakt',
	'users' => [
		'participant' => 'dalibnieks',
		'skipper' => 'kapteinis',
		'home' => 'mans-pieteikums',
	],
	'skipper' => [
		'participants' => 'dalibnieki',
		'dashboard' => 'kapteinis/mans-pieteikums',
	],
	'results' => 'rezultati',
	'gallery' => 'galerija',
	'galleryShow' => 'galerijas/{gallery}',
	'archivedGalleryShow' => 'arhivs/{year}/galerijas/{gallery}',
	// 'archive' => 'arhivs',
];