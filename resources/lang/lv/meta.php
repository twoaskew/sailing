<?php

return [
	'description' => 'Gulf of Riga Regatta 2023, kas vienlaicīgi ir arī Latvijas atklātais jūras burāšanas čempionāts un Livonijas kausa izcīņa, notiek no 24. jūnija līdz 30. jūnijam Rīgas jūras līci, posmā Ventspils - Roja - Kuresāre.',
	'fb-description' => 'Gulf of Riga Regatta 2023, kas vienlaicīgi ir arī Latvijas atklātais jūras burāšanas čempionāts un Livonijas kausa izcīņa, notiek no 24. jūnija līdz 30. jūnijam Rīgas jūras līci, posmā Ventspils - Roja - Kuresāre.',
	'fb-url' => 'https://gulfofrigaregatta.eu/lv',
];