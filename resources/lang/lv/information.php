<?php
return [
	'information' => 'Informācija',
  'about' => 'Par Rīgas līča regati 2019',
	'description' => '2019. gada Rīgas līča regate, kas vienlaicīgi ir arī Latvijas jūras burāšanas čempionāts un Livonijas kausa izcīņa, notiek no 25. līdz 29. jūnijam Rīgas jūras līcī, posmā Roja - Montu - Kuresāre - Roņu sala - Kuiviži.',
	'entryFee' => 'Dalības maksa',
	'feeUnder30feet' => '30,00 EUR par katru komandas locekli un 40,00 EUR par jahtu, ja tās garums ir mazāks par 30 pēdām.',
	'feeOver30feet' => '30,00 EUR par katru komandas locekli un 60,00 EUR par jahtu, ja tās garums ir 30 pēdas un vairāk.',
	'feeForOneStage' => 'Dalības maksa vienam posmam ir 20,00 EUR par katru komandas locekli un 30,00 EUR par jahtu.',
	'paymentInfoTitle' => 'Rekvizīti maksājumam:',
	'paymentInfo' => 'Latvijas Zēģelētāju savienība<br>
          Reģ. Nr. 50008003331<br>
          IBAN: LV67HABA000140J035673<br>
          A/S SWEDBANK<br>
          SWIFT: HABALV22<br>
          Ar norādi: "GoRR 2019", jahtas nosaukums vai buras numurs un pieteikto cilvēku skaits.',
    'lysTitle' => 'Par LYS sertifikātu',
    'lysWhy' => 'Lai jahta varētu iegūt LAT LYS sertifikātu, ir nepieciešams veikt jahtas apmērīšanu. Apmērīšanu var veikt pats jahtas īpašnieks, īpašnieka pieaicināts speciālists, vai arī pieaicinot kādu no LAT LYS mērniekiem. Apmērīšanas forma ir atrodama <a href="https://latlys.wordpress.com/" target="_blank" class="text-color-primary font-semibold no-underline">LAT LYS mājas lapā</a>.',
    'lysSend' => 'Aizpildītu mērīšanas formu nepieciešams nosūtīt uz e-pasta adresi <a href="mailto:gikefabo@gmail.com" class="text-green font-semibold">gikefabo@gmail.com</a> vai <a href="mailto:ilmaslax@gmail.com" class="text-green font-semibold">ilmaslax@gmail.com</a>',
    'lysPay' => 'Maksu par mērgrāmatu, 20 EUR, nepieciešams samaksāt uz Jahtkluba Engure kontu:',
    'lysPaymentInfo' => 'Biedrība "Jahtklubs ENGURE"<br>
        Reģ.Nr: 40008122558<br>
        Banka: Swedbank<br>
        Konts: LV91HABA0551019653780<br>
        Maksājuma uzdevumā norādīt: "LAT LYS, jahtas vārds"',
    'lysAdditionalInfo' => 'Papildus informācijai sazināties rakstot <a href="mailto:gikefabo@gmail.com" class="text-green font-semibold">gikefabo@gmail.com</a> vai LAT LYS handikapa sistēmas <a href="https://latlys.wordpress.com/" target="_blank" class="text-green font-semibold">mājas lapā</a>.',
    'documents' => 'Dokumenti',
    'noticeOfRace' => '<a href=":download_link" target="_blank" class="text-green font-semibold">Regates nolikums</a>.pdf',
    'entryForm' => '<a href=":download_link" target="_blank" class="text-green font-semibold">Pieteikums</a>.docx',
    'inspectionList' => '<a href=":download_link" target="_blank" class="text-green font-semibold">Inspection list</a>.docx',
    'ORCSailList' => '<a href=":download_link" target="_blank" class="text-green font-semibold">ORC sail list</a>.docx',
    'LYSSailList' => '<a href=":download_link" target="_blank" class="text-green font-semibold">LYS sail list</a>.docx',
    'usefulLinks' => 'Noderīgas saites',
    // 'hysInsurance' => 'Jahtu apdrošināšana Hamburger Yachtversicherung Schomacker',
    'hysInsurance' => '<a href="https://www.schomacker.de/en/yacht-boat-insurance/online-inquiry.html?pid=a1182" target="_blank" class="text-green font-semibold">Jahtu apdrošināšana</a> Hamburger Yachtversicherung Schomacker',
    'aboutOrcTitle' => 'Par ORC sertifikātu',
    'aboutOrcText' => 'ORC sertifikātu izsniegšanu no 2022. gada janvāra Latvijā koordinēs Latvijas Jūras burāšanas asociācija sadarbībā ar atbildīgo ORC inspektoru Matteo Zuppini (Spānija).<br>
    <a href="mailto:ljba.offshore.sailing@gmail.com" class="text-color-primary font-semibold no-underline">ljba.offshore.sailing@gmail.com</a>',
    'aboutTexelTitle' => 'Par TEXEL sertifikātu',
    'aboutTexelText' => 'Informācija par Texel sertifikātu daudzkorpusu jahtām pieejama pie Ulda Kronblūma Latvijas Daudzkorpusu jahtu asociācijā pa e-pastu <a href="mailto:ldja@parks.lv" class="text-color-primary font-semibold no-underline">ldja@parks.lv</a>',
    'harborsTitle' => 'Ostu dziļumu kartes',
    'rigaCityYachtClub' => 'Rīga, pilsētas jahtklubs',
    'ruhnu' => 'Roņu sala',
    'salacgriva' => 'Salacgrīva',
    'parnu' => 'Pērnava',

];