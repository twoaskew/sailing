<?php

return [
	'my_profile' => 'Mana pieteikuma informācija',
	'profile_copy' => 'Zemāk ir apkopota informācija, kuru iesniedzi, piesakot jahtu dalībai regatē.',
	'skipper' => 'Kapteinis',
	'description' => 'Kontaktinformācija tiks izmantota, lai sazinātos ar jahtas kapteini par regates norisi.',
	'yacht' => 'Jahta',
	'yacht-description' => 'Informācija par jahtu tiks izmantota, sastādot regates grupas.',
	'team' => 'Komanda',
	'team-description' => 'Cik dalībnieku ir plānots komandā? Dalībnieku skaits tiks vēlreiz precizēts regates starta dienā.',
	'extra_crew' => 'Papildus dalībnieki',
	'extra_crew_copy' => 'Šie cilvēki meklē sev komandu dalībai regatē. Tu vari ar viņiem sazināties un aicināt pievienoties savai komandai.',
	'no_extra_crew' => 'Šobrīd nav brīvu dalībnieku, kuri meklē vietu kādā no regates komandām. Noteikti pārbaudi šo lapu vēlāk, jo esam pārliecināti, ka tādi cilvēki būs.',
	'invite' => 'Aicināt pievienoties komandai',
	'invited' => 'Šis dalībnieks ir uzaicināts pievienoties Tavai komandai',
	'revoke_invitation' => 'Atsaukt ielūgumu',
	'invite_success' => 'Ielūgums veiksmīgi nosūtīts!',
	'invite_revoked' => 'Ielūgums atsaukts!',
	'registration' => 'Pieteikšanās regatei',
	'registration_copy' => 'Ja vēlies pieteikt jahtu dalībai regatē, lūdzu aizpildi šo reģistrācijas formu.',
	'thanks' => [
		'title' => 'Paldies, Tavu pieteikumu esam saņēmuši',
		'copy' => 'Ja vēlies izmainīt informāciju, kuru norādīji reģistrējoties, spied pogu "Ienākt" augšējā izvēlnē, būs pieejama Tava profila informācija, kā arī iespēja to rediģēt.',
	],
	'update_success' => 'Esam saglabājuši izmaiņas pieteikuma formā.',
	'notice_of_race' => 'Regates nolikums 2020',
];