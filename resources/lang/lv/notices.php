<?php

return [
    'title' => 'Paziņojumi',
    'back' => 'Visi paziņojumi',
    'empty' => 'Šobrīd vēl nav neviena paziņojuma.'
];