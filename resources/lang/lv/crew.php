<?php

return [
	'submission' => 'Mans pieteikums',
	'new_submission' => 'Pieteikums iespējai burāt',
	'new_submission_copy' => 'Ja vēlies piedalīties regatē, bet Tev nav komandas, atstāj savu kontaktinformāciju un Tev būs iespēja kļūt par kādas komandas dalībnieku, kas meklē papildinājumu.',
	'submission_copy' => 'Zemāk ir apkopota informācija no Tava pieteikuma. Vari to droši mainīt, ja gadījumā esi kļūdījies reģistrējoties.',
	'contact_info' => 'Kontaktinformācija',
	'about' => 'Par Mani',
	'about_you' => 'Par Tevi',
	'delete_submission' => 'Dzēst pieteikumu',
	'onBoard' => 'Tu esi pieteikts jahtas <span class="font-bold text-black capitalize">:yacht</span> komandā!',
	'invitation' => 'Jahtas <span class="font-bold text-black capitalize">:yacht</span> komanda aicina Tevi piedalīties!',
	'accept' => 'Apstiprināt',
	'decline' => 'Noraidīt',
	'accept_success' => 'Ielūgums apstiprināts!',
	'thanks' => [
		'title' => 'Paldies, Tavu pieteikumu esam saņēmuši.',
		'copy1' => 'Ja vēlies izmainīt reģistrācijas informāciju, spied pogu "Ienākt", kur varēsi to rediģēt.',
		'copy2' => 'Jahtu kapteiņi tiks informēti par Tavu vēlmi piedalīties sacensībās un Tu saņemsi e-pastu, kad kāds vēlēsies Tevi pievienot savai komandai.',
	]
];