<?php

return [
	'contacts' => 'Kontakti',
	'organisers-subtitle' => 'Regates organizētāji',
	'organisers-description' => 'Jautājumu un neskaidrību gadījumā droši sazinieties ar mums.',
	'valters-details' => 'Latvijas Zēģelētāju savienības valdes loceklis',
	'kristine-details' => 'Sekretāre',
	'silards-details' => 'Korporatīvie un finanšu jautājumi',
	'ilona-details' => 'Sabiedriskās un mediju attiecības',
	'payment-details' => 'Informācija maksājumu veikšanai.',
	'reg_no' => 'Reģ. Nr.',
	'bank' => 'Banka',
	'details' => 'Rekvizīti',
	'address' => 'Grēcinieku iela 11A-5, Rīga, LV-1050',
];