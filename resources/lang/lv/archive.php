<?php

return [
	'title' => 'Gulf of Riga Regatta 2018',
	'news' => 'Ziņas',
	'results' => 'Rezultāti',
	'gallery' => 'Galerija',
	'archive' => 'Arhīvs',
];